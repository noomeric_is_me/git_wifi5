package wimosalsafispeedtest.speedtestminilib;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import utils.AppUtils;


public class SpeedTestMini {
	private String host;
	private int port;
	private ProgressReportListener report = null;
	private static final long BUFFERSIZE = 256;
	private static final long REPORTINTERVAL = 1024;
	private static final long UPLOAD_SIZE = 1024*1024*2;
	//private Info info;

	public static void main(String args[])
	{
		SpeedTestMini inst = new SpeedTestMini(AppUtils.speedtest_server, 80);
		inst.setProgressReportListener(new ProgressReportListener() {

			@Override
			public void reportCurrentDownloadSpeed(long bits) {
				//System.out.println(bits);

			}

			@Override
			public void reportCurrentUploadSpeed(long bits) {
				// TODO Stub di metodo generato automaticamente

			}

			@Override
			public void reportCurrentDownloadProgress(long l) {
				//System.out.println(l);
			}

			@Override
			public void reportCurrentUploadPercentage(long l) {

			}
		});
		try {
			System.out.println(inst.doDownloadtest());
		} catch (IOException e) {
			// TODO Blocco catch generato automaticamente
			e.printStackTrace();
		}
	}

	public SpeedTestMini(String host, int port) {
		this.host = host;
		this.port = port;


	}

	public void setProgressReportListener(ProgressReportListener lis)
	{
		report = lis;
	}

	public long doUploadtest() throws UnknownHostException, IOException
	{
		Socket sock = new Socket(host,port);
		PrintWriter pw = new PrintWriter(sock.getOutputStream());
		//InputStreamReader istream = new InputStreamReader(sock.getInputStream());
		pw.println("POST /speedtest/upload.php HTTP/1.1");
		pw.println("Host: "+host);
		pw.println("Cache-Control: max-age=0");
		pw.println("Content-Length: "+UPLOAD_SIZE);
		pw.println("Connection: close");
		pw.println("");
		pw.flush();
		long bytestot = 1;
		long bytesint = 0;
		long byteswritten = 1;
		long start = System.currentTimeMillis();


		long intermediate = start;
		byte buffer[] = new byte[(int) BUFFERSIZE];
		for ( int i = 0; i < BUFFERSIZE; i++ )
			buffer[i] = (byte) (Math.random()*255);
		while ( bytestot < (1024*1024*2) )
		{
			sock.getOutputStream().write(buffer);
			byteswritten = buffer.length;
			bytesint += byteswritten;
			bytestot += byteswritten;
			if ( bytesint >= 1024 )
			{
				long took = System.currentTimeMillis() - start;
				long bits = (long)((double)(bytestot*8)/((double)took/1000.0));

				if ( report != null )
					report.reportCurrentUploadSpeed(bits);
					report.reportCurrentUploadPercentage(bytestot * 100 / (1024*1024*2));
				bytesint = 0;
				intermediate = System.currentTimeMillis();
			}
		}

		long took = System.nanoTime() - start;
		long bits = (long)((double)(bytestot*8)/((double)took/1000.0));

		sock.close();
		return bits;
	}

	public long doDownloadtest() throws UnknownHostException, IOException
	{
		Socket sock = new Socket(host,port);
		PrintWriter pw = new PrintWriter(sock.getOutputStream());
		//InputStreamReader iStream = new InputStreamReader(sock.getInputStream());
		pw.println("GET /speedtest/random2000x2000.jpg HTTP/1.1");
		pw.println("Host: " + host);
		pw.println("Cache-Control: max-age=0");
		pw.println("Connection: close");
		pw.println("");
		pw.flush();
		//pw.flush();
		//pw.flush();
		//pw.flush();
		long bytestot = 1;
		long bytesint = 0;
		long bytesread = 1;
		sock.getInputStream().read();
		long start = System.currentTimeMillis();
		boolean startBits = false;
		long avg = 0;
		long intermediate = start;
		int bufferSize = 1024*1024;
		byte buffer[] = new byte[bufferSize];
		int cycle = 1;
        while ( ( bytesread = sock.getInputStream().read(buffer) ) != -1)
		{
			//long avg;
			bytesint += bytesread;
			bytestot += bytesread;
			if ( bytesint >= REPORTINTERVAL*32 )
			{
				long took = System.currentTimeMillis() - start;
				long bits = (long)((double)(bytestot*8)/((double)took/1000.0));
				if(!startBits) {
					avg = bits;
				}else {
						avg = avg + bits;
				}
				if ( report != null )
					//report.reportCurrentDownloadSpeed(bits);
					report.reportCurrentDownloadSpeed(bits);
					report.reportCurrentDownloadProgress(bytestot*100/7907740);
                    cycle++;
				bytesint = 0;
				intermediate = System.currentTimeMillis();
				startBits = true;
			}
		}

		long took = System.currentTimeMillis() - start;
		long bits = (long)((double)(bytestot*8)/((double)took/1000.0));

		sock.close();
		return bits;
	}


}
