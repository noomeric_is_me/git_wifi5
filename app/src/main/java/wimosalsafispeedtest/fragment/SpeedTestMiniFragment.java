package wimosalsafispeedtest.fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.doubleclick.PublisherAdRequest;
import com.google.android.gms.ads.doubleclick.PublisherAdView;
import com.jao.freewifi.wifimanager.R;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.crashlytics.android.answers.ShareEvent;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.ads.AdView;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.ExecutionException;

import co.mobiwise.materialintro.shape.Focus;
import co.mobiwise.materialintro.shape.FocusGravity;
import co.mobiwise.materialintro.shape.ShapeType;
import co.mobiwise.materialintro.view.MaterialIntroView;
import wimosalsafifreewifi.application.AppController;
import wimosalsafifreewifi.fragment.HotspotFragment;
import wimosalsafispeedtest.internet.CircularProgressBar;
import wimosalsafispeedtest.internet.CreateDatabaseAsynctask;
import wimosalsafispeedtest.internet.SpeedTestSQLite;
import wimosalsafispeedtest.internet.SpeedTestSQLiteData;
import wimosalsafispeedtest.speedtestminilib.ProgressReportListener;
import wimosalsafispeedtest.speedtestminilib.SpeedTestMini;
import wimosalsafispeedtest.util.FeedXmlUtil;
import utils.AppUtils;
import yalantis.com.sidemenu.interfaces.ScreenShotable;

public class SpeedTestMiniFragment extends Fragment implements ScreenShotable {

    private Button buttonTest, buttonFinished,buttonShare;
    private LinearLayout mLogoapp;
    private TextView download;
    private TextView upload;
    private TextView ping;

    private ScrollView scrollView;
    private ConnectivityManager connManager;
    private String tmp, dateDb = "", timeDb = "", downloadDb, uploadDb, pingDb;
    private LocationManager mLocationManager;
    private float LOCATION_REFRESH_DISTANCE = 0;
    private long LOCATION_REFRESH_TIME = 0;
    private ProgressDialog dialog;
    private String best;
    private ArrayList<ArrayList<Float>> nearestlist;
    private ArrayList<Object> serverXML;
    private int DISTANCE = 0;
    private int INDEX = 1;
    private int counter = 0;
    private String date = "";
    private AlphaAnimation anim;
    private ImageView reset;
    private CircularProgressBar circular;
    private TextView textviewStatus,textviewUnit;
    private LinearLayout display;
    private RelativeLayout  speedTestFragment;
    private ArrayList<String> datelist = new ArrayList<String>();
    private SpeedTestSQLite db;
    private View mRootView;
    private ShareDialog mShareDialog;
    private SharedPreferences sharedPref;
    private PublisherAdView mPublisherAdView;
    private Bitmap bitmap;
//    private FloatingActionButton fab_speed_booster;

    public ScrollView getScrollView() {
        //scrollView = (ScrollView) mRootView.findViewById(R.id.scrollView1);
        return scrollView;
    }
    public static SpeedTestMiniFragment newInstance()
    {
        SpeedTestMiniFragment speedtestminifragment = new SpeedTestMiniFragment();
        return speedtestminifragment;
    }

    private class SpeedTestDownloadTask extends AsyncTask<String, Long, Long> {
        @Override
        protected void onPreExecute() {
            download.setTextColor(AppController.getInstance().getAppContext().getResources().getColor(R.color.orange));
            super.onPreExecute();
        }

        @Override
        protected Long doInBackground(String... params) {

            publishProgress(0L, 0L);
            SpeedTestMini mini = new SpeedTestMini(params[0], Integer.parseInt(params[1]));
            mini.setProgressReportListener(new ProgressReportListener() {
                Long[] data = new Long[3];
                long dlbits_last = 0;
                long ulbits_last = 0;
                long dl_Percentage = 0;

                @Override
                public void reportCurrentDownloadSpeed(long bits) {
                    data[0] = bits;
                    data[1] = dl_Percentage;
                    SpeedTestDownloadTask.this.publishProgress(data);
                    dlbits_last = bits;
                }

                @Override
                public void reportCurrentUploadSpeed(long bits) {
                }

                @Override
                public void reportCurrentDownloadProgress(long percentage) {
                    data[0] = dlbits_last;
                    data[1] = percentage;
                    SpeedTestDownloadTask.this.publishProgress(data);
                    dl_Percentage = percentage;
                }

                @Override
                public void reportCurrentUploadPercentage(long l) {

                }

            });

            try {
                mini.doDownloadtest();
                return 0L;
            } catch (IOException e) {
                Crashlytics.logException(e);
                e.printStackTrace();
            }

            return 0L;
        }

        @Override
        protected void onProgressUpdate(Long... values) {
            String suffix = " "+AppController.getInstance().getAppContext().getResources().getString(R.string.bits_sec);
            double sout = values[0];
            long percentage = values[1];
            if (sout > 1000000) {
                suffix = " "+AppController.getInstance().getAppContext().getResources().getString(R.string.mbps);
                sout /= 1000000.0;
            } else if (sout > 1000) {
                suffix = " "+AppController.getInstance().getAppContext().getResources().getString(R.string.kbps);
                sout /= 1000.0;
            }

            download.setText(AppController.getInstance().getAppContext().getResources().getString(R.string.download)+"\n" + String.format("%.2f", sout) + suffix + "\n");
            circular.setProgress((int) percentage);

//            Log.d("downloadDb",circular.getProgress() + "");

//            circular.setTitle(String.format("%.2f", sout) + suffix);
//            circular.setSubTitle(AppController.getInstance().getAppContext().getResources().getString(R.string.download));
            downloadDb=(String.format("%.2f", sout)+suffix);

            textviewStatus.setText(String.format("%.2f", sout));
            textviewUnit.setText(suffix);
        }

        @Override
        protected void onPostExecute(Long result) {
            new SpeedTestUploadTask().execute(host, "80");
            download.setTextColor(AppController.getInstance().getAppContext().getResources().getColor(R.color.NativeGreen));

        }
    }

    private class SpeedTestUploadTask extends AsyncTask<String, Long, Long> {
        double ul_avg = 0;
        int cycle = 0;

        @Override
        protected void onPreExecute() {
            upload.setTextColor(AppController.getInstance().getAppContext().getResources().getColor(R.color.orange));
            super.onPreExecute();
        }

        @Override
        protected Long doInBackground(String... params) {

            publishProgress(0L, 0L);
            SpeedTestMini mini = new SpeedTestMini(params[0], Integer.parseInt(params[1]));
            mini.setProgressReportListener(new ProgressReportListener() {
                long ul_Percentage = 0;
                long ulbits_last = 0;

                @Override
                public void reportCurrentDownloadSpeed(long bits) {
                }

                @Override
                public void reportCurrentUploadSpeed(long bits) {
                    SpeedTestUploadTask.this.publishProgress(bits, ul_Percentage);
                    ulbits_last = bits;
                    ul_avg = ul_avg + bits;
                    cycle = cycle + 1;
                }

                @Override
                public void reportCurrentDownloadProgress(long l) {

                }

                @Override
                public void reportCurrentUploadPercentage(long percentage) {
                    SpeedTestUploadTask.this.publishProgress(ulbits_last, percentage);
                    ul_Percentage = percentage;
                }

            });

            try {

                //mini.doDownloadtest();
                mini.doUploadtest();

                return 0L;
            } catch (IOException e) {
                Crashlytics.logException(e);
                e.printStackTrace();
            }
            return 0L;
        }

        @Override
        protected void onProgressUpdate(Long... values) {
            String suffix = AppController.getInstance().getAppContext().getResources().getString(R.string.bits_sec);
            //double sout = values[0];
            double sout2 = values[0];
            long percentage = values[1];
            final double gval2 = sout2 / 1000000.0;
            //final double gval = sout / 1000000.0;
            if (sout2 > 1000000) {
                suffix = " "+AppController.getInstance().getAppContext().getResources().getString(R.string.mbps);
                //sout /= 1000000.0;
                sout2 /= 1000000.0;
            } else if (sout2 > 1000) {
                suffix = " "+AppController.getInstance().getAppContext().getResources().getString(R.string.kbps);
                //sout /= 1000.0;
                sout2 /= 1000.0;
            }
            //Log.d("Speed",+ sout + " " + sout2);
            //download.setText("Download\n" + String.format("%.2f", sout) + suffix + "\n");
            upload.setText(AppController.getInstance().getAppContext().getResources().getString(R.string.upload)+"\n" + String.format("%.2f", sout2) + suffix + "\n");
            circular.setProgress(100 - (int) percentage);

//            Log.d("uploadDb",circular.getProgress() + "");

//            circular.setTitle(String.format("%.2f", sout2) + suffix);
//            circular.setSubTitle(AppController.getInstance().getAppContext().getResources().getString(R.string.upload));
            uploadDb=(String.format("%.2f", sout2)+suffix);
            //gauge.setValue(gval2);

            textviewStatus.setText(String.format("%.2f", sout2));
            textviewUnit.setText(suffix);
        }

        @Override
        protected void onPostExecute(Long result) {

            upload.setTextColor(AppController.getInstance().getAppContext().getResources().getColor(R.color.NativeGreen));

            dateDb = new SimpleDateFormat("dd-MMM").format(new Date());
            timeDb = new SimpleDateFormat("HH:mm:ss").format(new Date());
            db.addGroup(new SpeedTestSQLiteData(dateDb, timeDb, downloadDb, uploadDb, pingDb));

            buttonTest.setText(AppController.getInstance().getAppContext().getResources().getString(R.string.start_test));
            buttonTest.setVisibility(View.GONE);
            buttonFinished.setVisibility(View.VISIBLE);
            buttonShare.setVisibility(View.GONE);
            mLogoapp.setVisibility(View.GONE);

            showIntroSpeedtestHistory(buttonShare);

            TextView textview_result = (TextView) mRootView.findViewById(R.id.textview_result);
            textview_result.setVisibility(View.VISIBLE);

            if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {

                //Show ads native
//                RelativeLayout adsNativeRootview = (RelativeLayout) mRootView.findViewById(R.id.adsNativeRootview);
//                adsNativeRootview.setVisibility(View.VISIBLE);

                //Gone Speed View
//                RelativeLayout speedTestFragment = (RelativeLayout) mRootView.findViewById(R.id.speedTestFragment);
//                speedTestFragment.setVisibility(View.GONE);

//                AdView mAdView = (AdView) mRootView.findViewById(R.id.admob_banner_view);
//                mAdView.setVisibility(View.GONE);
            } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {

                //Show ads native
                RelativeLayout adsNativeRootview = (RelativeLayout) mRootView.findViewById(R.id.adsNativeRootview);
                adsNativeRootview.setVisibility(View.VISIBLE);

                //Gone Speed View
                RelativeLayout speedTestFragment = (RelativeLayout) mRootView.findViewById(R.id.speedTestFragment);
                speedTestFragment.setVisibility(View.GONE);


                adViewfacebook.setVisibility(View.GONE);
                adViewfacebook.loadAd();
            }


//            //Add animation for circular
//            circular.startAnimation(anim);
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    circular.clearAnimation();
//                }
//            }, 3000);
//            circular.setEnabled(true);
        }
    }

    private class SpeedTestLatency extends AsyncTask<String, Void, Long> {

        @Override
        protected void onPreExecute() {
            display.setVisibility(View.VISIBLE);
            ping.setText(AppController.getInstance().getAppContext().getResources().getString(R.string.ping)+"\n"+AppController.getInstance().getAppContext().getResources().getString(R.string.waiting));
            ping.setTextColor(AppController.getInstance().getAppContext().getResources().getColor(R.color.orange));
            download.setText(AppController.getInstance().getAppContext().getResources().getString(R.string.download)+"\n"+AppController.getInstance().getAppContext().getResources().getString(R.string.waiting));
            upload.setText(AppController.getInstance().getAppContext().getResources().getString(R.string.upload)+"\n"+AppController.getInstance().getAppContext().getResources().getString(R.string.waiting));
//            buttonTest.startAnimation(anim);
            circular.setProgress(100);
            circular.setTitle(AppController.getInstance().getAppContext().getResources().getString(R.string.latency_test));
            circular.startAnimation(anim);
            super.onPreExecute();
        }

        @Override
        protected Long doInBackground(String... params) {
            long dt = 3000;
            long avg = 0;
            for (int i = 0; i < 10; i++) {
                long t1 = System.nanoTime();
                try {
                    Socket socket = new Socket(InetAddress.getByName(host), 80);
                    long t2 = System.nanoTime();
                    dt = (t2 - t1) / 1000000;
                    avg = avg + dt;
                    socket.close();
                } catch (IOException e) {
                    Crashlytics.logException(e);
                }
            }
            dt = avg / 10;
            return dt;
        }

        @Override
        protected void onPostExecute(Long time) {
            pingDb = (Long.toString(time)+" "+AppController.getInstance().getAppContext().getResources().getString(R.string.ms));
            ping.setText(AppController.getInstance().getAppContext().getResources().getString(R.string.ping)+"\n" + time + " " + AppController.getInstance().getAppContext().getResources().getString(R.string.ms));
            ping.setTextColor(AppController.getInstance().getAppContext().getResources().getColor(R.color.NativeGreen));
            circular.clearAnimation();
            super.onPostExecute(time);
            new SpeedTestDownloadTask().execute(host, "80");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        if (mRootView == null) {
            mRootView = inflater.inflate(R.layout.speed_test_mini, container,
                    false);

            serverXML = FeedXmlUtil.feed(getActivity());
            download = (TextView) mRootView.findViewById(R.id.textViewDownload);
            upload = (TextView) mRootView.findViewById(R.id.textViewUpload);
            ping = (TextView) mRootView.findViewById(R.id.textViewLatency);
            buttonTest = (Button) mRootView.findViewById(R.id.buttonTest);
            buttonFinished = (Button) mRootView.findViewById(R.id.buttonFinished);
            buttonShare = (Button) mRootView.findViewById(R.id.buttonShare);
            mLogoapp = (LinearLayout) mRootView.findViewById(R.id.logoapp);

            buttonFinished.setVisibility(View.GONE);
            buttonShare.setVisibility(View.GONE);
            mLogoapp.setVisibility(View.VISIBLE);

            display = (LinearLayout) mRootView.findViewById(R.id.measumentDisplay);
            speedTestFragment = (RelativeLayout) mRootView.findViewById(R.id.speedTestFragment);
            circular = (CircularProgressBar) mRootView.findViewById(R.id.circularprogressbar);
            circular.setProgress(100);

            textviewStatus = (TextView) mRootView.findViewById(R.id.textview_status);
            textviewUnit = (TextView) mRootView.findViewById(R.id.textview_unit);


            connManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            mLocationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            anim = new AlphaAnimation(0.0f, 1.0f);
            anim.setDuration(1000); //You can manage the blinking time with this parameter
            anim.setStartOffset(20);
            anim.setRepeatMode(Animation.REVERSE);
            anim.setRepeatCount(Animation.INFINITE);

            db = new SpeedTestSQLite(getActivity());

            buttonTest.setText(AppController.getInstance().getAppContext().getResources().getString(R.string.start_test));
            buttonTest.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

//                    Answers.getInstance().logContentView(new ContentViewEvent()
//                            .putContentName("Click Test Speed"));

//                    countSpeedTesting += 1; //count speed testing

                    circular.setVisibility(View.VISIBLE);
                    textviewStatus.setVisibility(View.VISIBLE);
                    textviewUnit.setVisibility(View.VISIBLE);
                    buttonTest.setVisibility(View.GONE);

                    host = AppUtils.speedtest_server;
                    new SpeedTestLatency().execute();
                }
            });
            buttonFinished.setText(AppController.getInstance().getAppContext().getResources().getString(R.string.test_again));
            buttonFinished.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    try {

                        buttonFinishedSelected();

//                    if(countSpeedTesting >= 3) {
//                        countSpeedTesting = 0;
                        //Show Full Ads
                        if(AppUtils.ads_interstitial_show_all) {

                            if (AppUtils.adsNetworkType != AppUtils.NONE_TYPE) {
                                AppUtils.getInstance().showAdsFullBanner(null);
                            }

                        }else {

                            if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
                                AppUtils.getInstance().showAdmobAdsFullBanner(null);
                            } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
                                AppUtils.getInstance().showFBAdsFullBanner(null);
                            }
                        }
//                    }
                    } catch (Exception ignored) {
                        Crashlytics.logException(ignored);
                    }

                }
            });

            buttonShare.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    try {
                        if (ShareDialog.canShow(ShareLinkContent.class)) {

                            Context context = AppController.getInstance().getAppContext();
                            String setContentUrl = "";
                            String setImageUrl = "";

                            try{
                                if(AppUtils.appConfig != null){

                                    setContentUrl = AppUtils.appConfig.getData().getSpeedTestSharing()
                                            .get_share_contentUrl();
                                    setImageUrl = AppUtils.appConfig.getData().getSpeedTestSharing()
                                            .get_share_setImageUrl();

                                } else{
                                    setContentUrl = context.getString(R.string.share_contentUrl);
                                    setImageUrl = context.getString(R.string.share_setImageUrl);
                                }
                            }catch (Exception e){
                                Crashlytics.logException(e);
                                setContentUrl = context.getString(R.string.share_contentUrl);
                                setImageUrl = context.getString(R.string.share_setImageUrl);
                            }


                            ShareLinkContent content = new ShareLinkContent.Builder()
                                    .setContentUrl(Uri.parse(setContentUrl))
                                    .setImageUrl(Uri.parse(setImageUrl))
                                    .setContentTitle("Best Free Speed Test Try it!\nDL: " + downloadDb
                                            + " | UL: " + uploadDb)
                                    .setContentDescription(context.getString(R.string.share_setContentDescription))
                                    .build();
                            mShareDialog.show(content);


                            Answers.getInstance().logShare(new ShareEvent()
                                    .putMethod("Facebook"));
                        }
                    } catch (Exception e) {
                        Crashlytics.logException(e);

                        Toast.makeText(getActivity(),"Share Error : Not Found Facebook Installed On Your Device.",Toast.LENGTH_SHORT).show();

                        Answers.getInstance().logShare(new ShareEvent()
                                .putMethod("Error"));
                    }

                }
            });

            buttonShowWifiSelected();

            mShareDialog = new ShareDialog(this);

//            //Click to speed booster
//            fab_speed_booster.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                    Answers.getInstance().logContentView(new ContentViewEvent()
//                            .putContentName("Speed Booster"));
//
//                    SharedPreferences.Editor editor = sharedPref.edit();
//                    editor.putBoolean(AppUtils.SHOW_INTRO_SPEEDTEST_SPEEDBOOSTER_TYPE_KEY, true);
//                    editor.apply();
//
//                    Intent intent = new Intent(getActivity(), OptimizeActivity.class);
//                    intent.putExtra(MainAppActivity.COLOR_MESSAGE, MainAppActivity.currentColor);
//                    startActivity(intent);
//                }
//            });


            //Init admob or facebook
            initAds();
        }


        return  mRootView;
    }


    private void buttonShowWifiSelected(){
        display.setVisibility(View.VISIBLE);
        speedTestFragment.setVisibility(View.VISIBLE);
        buttonTest.setVisibility(View.VISIBLE);
    }

    private void buttonFinishedSelected(){

        if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {

            //Gone ads native
//            RelativeLayout adsNativeRootview = (RelativeLayout) mRootView.findViewById(R.id.adsNativeRootview);
//            adsNativeRootview.setVisibility(View.GONE);


            //Show Speed View
//            RelativeLayout speedTestFragment = (RelativeLayout) mRootView.findViewById(R.id.speedTestFragment);
//            speedTestFragment.setVisibility(View.VISIBLE);

            //Gone text result
            TextView textview_result = (TextView) mRootView.findViewById(R.id.textview_result);
            textview_result.setVisibility(View.GONE);

//            AdView mAdView = (AdView) mRootView.findViewById(R.id.admob_banner_view);
//            mAdView.setVisibility(View.VISIBLE);

            mPublisherAdView = mRootView.findViewById(R.id.admob_banner_view);
            PublisherAdRequest adRequest = new PublisherAdRequest.Builder().build();
            mPublisherAdView.setVisibility(View.VISIBLE);
            mPublisherAdView.loadAd(adRequest);

        } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {

            //Gone ads native
            RelativeLayout adsNativeRootview = (RelativeLayout) mRootView.findViewById(R.id.adsNativeRootview);
            adsNativeRootview.setVisibility(View.GONE);

            //load new ads native
            LinearLayout adNativeViewContainer = (LinearLayout) mRootView.findViewById(R.id.facebook_native_ad_container);
            adNativeViewContainer.removeAllViews();
            AppUtils.getInstance().showFBAdsNativeInit(getActivity(),adNativeViewContainer);

            //Show Speed View
            RelativeLayout speedTestFragment = (RelativeLayout) mRootView.findViewById(R.id.speedTestFragment);
            speedTestFragment.setVisibility(View.VISIBLE);

            //Gone text result
            TextView textview_result = (TextView) mRootView.findViewById(R.id.textview_result);
            textview_result.setVisibility(View.GONE);

            adViewfacebook.setVisibility(View.VISIBLE);
        }


        circular.setVisibility(View.GONE);
        textviewStatus.setVisibility(View.GONE);
        textviewUnit.setVisibility(View.GONE);
        buttonTest.setVisibility(View.VISIBLE);

        buttonTest.setVisibility(View.VISIBLE);
        buttonFinished.setVisibility(View.GONE);
        buttonShare.setVisibility(View.GONE);
        mLogoapp.setVisibility(View.VISIBLE);

//        showIntroSpeedBooster(fab_speed_booster);
    }
    private com.facebook.ads.AdView adViewfacebook;
    private int countSpeedTesting;
    private void initAds(){

        if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
            //Show Admob Ads

            //Banner
//            AdView mAdView = (AdView) mRootView.findViewById(R.id.admob_banner_view);
//            mAdView.setVisibility(View.VISIBLE);
//            AppUtils.getInstance().showAdsBanner(mAdView);

            mPublisherAdView = mRootView.findViewById(R.id.admob_banner_view);
            PublisherAdRequest adRequest = new PublisherAdRequest.Builder().build();
            mPublisherAdView.setVisibility(View.VISIBLE);
            mPublisherAdView.loadAd(adRequest);

            //load large ads native bottom
//            NativeExpressAdView adViewNative = (NativeExpressAdView) mRootView.findViewById(R.id.admob_native_view);
//            AppUtils.getInstance().showNativeAdsBanner(adViewNative);


        } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
            //Show Facebook Ads
//            AdSettings.addTestDevice("20493d3d20e3114b4db3dbefdde28996");

            //Banner
            RelativeLayout adViewContainer = (RelativeLayout) mRootView.findViewById(R.id.facebook_banner_ad_container);
            adViewContainer.setVisibility(View.VISIBLE);
            adViewfacebook = AppUtils.getInstance().showFBAdsBanner(getActivity(),adViewContainer);

            //Native
            LinearLayout adNativeViewContainer = (LinearLayout) mRootView.findViewById(R.id.facebook_native_ad_container);
            AppUtils.getInstance().showFBAdsNativeInit(getActivity(),adNativeViewContainer);

        }
    }


    @Override
    public void onDestroy() {
        if(adViewfacebook != null){
            adViewfacebook.destroy();
        }
        super.onDestroy();
    }

    private void showDialog() {
        dialog = new ProgressDialog(getActivity());
        dialog.setTitle(AppController.getInstance().getAppContext().getResources().getString(R.string.loading));
        dialog.setMessage(AppController.getInstance().getAppContext().getResources().getString(R.string.searching_location));
        dialog.setCancelable(false);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.show();
    }

    private void wifi_change(Boolean mWifi) {
        WifiManager mWifiManager = (WifiManager) getActivity().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        mWifiManager.setWifiEnabled(mWifi);
    }

    private String host;
    private final LocationListener mLocationListener = new LocationListener() {

        @Override
        public void onLocationChanged(final Location location) {
            dialog.dismiss();
            CreateDatabaseAsynctask dbCreate = new CreateDatabaseAsynctask(getActivity());
            dbCreate.execute(location);
            try {
                nearestlist = dbCreate.get();
                //getServerData();
            } catch (InterruptedException e) {
                Crashlytics.logException(e);
                e.printStackTrace();
            } catch (ExecutionException e) {
                Crashlytics.logException(e);
                e.printStackTrace();
            } catch (Exception e){
                Crashlytics.logException(e);
            }
            mLocationManager.removeUpdates(mLocationListener);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            Log.d("Speed test", "Status Changed");
        }

        @Override
        public void onProviderEnabled(String provider) {
            Log.d("Speed test", "Provider Enabled");
        }

        @Override
        public void onProviderDisabled(String provider) {
            Log.d("Speed test", "Provider Disabled");
        }
    };

    private void showIntroSpeedtestHistory(View view){

        try{
            if(view == null) return;

            if(sharedPref == null) return;

            if(getActivity() == null) return;

            if(sharedPref.getBoolean(AppUtils.SHOW_INTRO_SPEEDTEST_SHARE_TYPE_KEY, false)) {
                return;
            }

            new MaterialIntroView.Builder(getActivity())
//                    .enableDotAnimation(true)
                    .setTargetPadding(40)
                    .enableIcon(false)
                    .setFocusGravity(FocusGravity.CENTER)
                    .setFocusType(Focus.MINIMUM)
                    .setDelayMillis(500)
                    .enableFadeAnimation(true)
                    .performClick(true)
                    .setInfoText("Hi There! You can now share speed internet to " +
                            "your friend, what are you waiting for Share it now !")
                    .setShape(ShapeType.CIRCLE)
                    .setTarget(view)
                    .setUsageId("intro_speed_share") //THIS SHOULD BE UNIQUE ID
                    .dismissOnTouch(true)
                    .show();


            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putBoolean(AppUtils.SHOW_INTRO_SPEEDTEST_SHARE_TYPE_KEY, true);
            editor.apply();
        }catch (Exception e){
            Crashlytics.logException(e);
        }
    }


    private void showIntroSpeedBooster(View view){

        try{
            if(view == null) return;

            if(sharedPref == null) return;

            if(getActivity() == null) return;

            if(sharedPref.getBoolean(AppUtils.SHOW_INTRO_SPEEDTEST_SPEEDBOOSTER_TYPE_KEY, false)) {
                return;
            }

            new MaterialIntroView.Builder(getActivity())
//                    .enableDotAnimation(true)
                    .setTargetPadding(40)
                    .enableIcon(false)
                    .setFocusGravity(FocusGravity.CENTER)
                    .setFocusType(Focus.MINIMUM)
                    .setDelayMillis(500)
                    .enableFadeAnimation(true)
                    .performClick(true)
                    .setInfoText("Hi There! You can now boost your internet speed, " +
                            "what are you waiting for boost it up now!")
                    .setShape(ShapeType.CIRCLE)
                    .setTarget(view)
                    .setUsageId("intro_speed_booster") //THIS SHOULD BE UNIQUE ID
                    .dismissOnTouch(true)
                    .show();


            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putBoolean(AppUtils.SHOW_INTRO_SPEEDTEST_SPEEDBOOSTER_TYPE_KEY, true);
            editor.apply();

        }catch (Exception e){
            Crashlytics.logException(e);
        }
    }

    @Override
    public void takeScreenShot() {
        Thread thread = new Thread() {
            @Override
            public void run() {
                Bitmap bitmap = Bitmap.createBitmap(mRootView.getWidth(),
                        mRootView.getHeight(), Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(bitmap);
                mRootView.draw(canvas);
                SpeedTestMiniFragment.this.bitmap = bitmap;
            }
        };

        thread.start();

    }

    @Override
    public Bitmap getBitmap() {
        return bitmap;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.mRootView = view.findViewById(R.id.container);
    }


}
