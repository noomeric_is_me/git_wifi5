package wimosalsafispeedtest.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.media.session.PlaybackStateCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.doubleclick.PublisherAdRequest;
import com.google.android.gms.ads.doubleclick.PublisherAdView;
import com.thefinestartist.Base;
import com.jao.freewifi.wifimanager.R;

import java.text.DecimalFormat;
import java.util.ArrayList;

import wimosalsafifreewifi.activity.ChartActivity;
import wimosalsafifreewifi.activity.SpeedTestMiniActivity;
import wimosalsafifreewifi.activity.WifiConnectActivity;
import wimosalsafifreewifi.application.AppController;
import wimosalsafifreewifi.main.MainAppActivity;
import wimosalsafifreewifi.main.MapsMarkerActivity;
import wimosalsafimainapp.scrollable.fragment.FragmentPagerFragment;
import wimosalsafispeedtest.activity.OptimizeActivity;
import utils.AppUtils;
import wimosalsafiwhousewifi.AdViewController;
import wimosalsafiwhousewifi.PingActivity;
import wimosalsafiwhousewifi.Router_password;
import wimosalsafiwhousewifi.WhoUseWifiActivity;
import wimosalsafiwhousewifi.WifiInfoActivity;
import wimosalsafiwhousewifi.network.Wireless;
import wimosalsafiwhousewifi.utils.DataService;
import wimosalsafiwhousewifi.utils.StoredData;
import wimosalsafiwifimap.activity.MapActivity;
import yalantis.com.sidemenu.interfaces.ScreenShotable;

import static com.thefinestartist.utils.content.ContextUtil.getApplicationContext;
import static com.thefinestartist.utils.content.ContextUtil.registerReceiver;
import static com.thefinestartist.utils.content.ContextUtil.startService;
import static com.thefinestartist.utils.content.ContextUtil.unregisterReceiver;

/**
 * Created by NTL on 8/12/2017 AD.
 */

public class MainFreeWiFiFragment extends FragmentPagerFragment implements ScreenShotable {

    public static final String TAG = MainFreeWiFiFragment.class.getSimpleName();

    private View mView;
    private Bitmap bitmap;
    private TextView text_number_wifi_in_range;
    private NestedScrollView mScrollView;
    private MainListener mMainListener;
    private PublisherAdView mPublisherAdView;

    private TextView dSpeed;
    private Thread dataUpdate;
    protected ArrayList<Float> mDownload;
    protected ArrayList<Float> mUpload;
    private BroadcastReceiver receiver = new C04651();
    CardView roter_setting_card;
    private TextView router_coonected_msg;
    Button router_info_btn;
    private TextView router_name;
    CardView router_password_card;
    ScrollView scrollView;
    private TextView uSpeed;
    Wireless wifi;
    DecimalFormat f132df = new DecimalFormat("#.##");
    private Handler vHandler = new Handler();
    private IntentFilter intentFilter = new IntentFilter();
    DrawerLayout leftDrawer;
    Toolbar toolbar;
    AdViewController controller;
    public interface MainListener{
        void clickHotspot();
    }

    public void setMainListener(MainListener mainListener){
        mMainListener = mainListener;
    }

    public static MainFreeWiFiFragment newInstance(){
        MainFreeWiFiFragment mainfreewififragment = new MainFreeWiFiFragment();
        return mainfreewififragment;
    }
    class C04651 extends BroadcastReceiver {
        C04651() {
        }

        public void onReceive(Context context, Intent intent) {
            NetworkInfo networkInfo = (NetworkInfo) intent.getParcelableExtra("networkInfo");
            if (networkInfo != null) {
                MainFreeWiFiFragment.this.getNetworkInfo(networkInfo);
            }
        }
    }
    class C04662 implements View.OnClickListener {
        C04662() {
        }

        public void onClick(View view) {
            MainFreeWiFiFragment.this.startActivity(new Intent("android.settings.WIFI_SETTINGS"));
        }
    }

    class C04683 implements Runnable {

        class C04671 implements Runnable {
            C04671() {
            }

            public void run() {
                MainFreeWiFiFragment.this.setSpeed();
            }
        }

        C04683() {
        }

        public void run() {
            while (!MainFreeWiFiFragment.this.dataUpdate.getName().equals("stopped")) {
                MainFreeWiFiFragment.this.vHandler.post(new C04671());
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if(mView == null){
            mView = inflater.inflate(R.layout.main_vmkoomfreewifi_fragment3, container, false);
            Base.initialize(getActivity());
            if (!DataService.service_status) {
                startService(new Intent(getActivity(), DataService.class));
            }


            this.router_name = (TextView) mView.findViewById(R.id.router_name);
            this.router_coonected_msg = (TextView) mView.findViewById(R.id.router_connected_msg);
            this.dSpeed = (TextView) mView.findViewById(R.id.text_download);
            this.uSpeed = (TextView) mView.findViewById(R.id.text_upload);
            this.dSpeed.setText(" ");
            this.uSpeed.setText(" ");
            this.mDownload = new ArrayList();
            this.mUpload = new ArrayList();
            liveData();
//            this.router_info_btn.setOnClickListener(this);
//            this.leftDrawer.setDrawerListener(new ActionBarDrawerToggle(this, this.leftDrawer, this.toolbar, 0, 0));
            this.intentFilter.addAction("android.net.wifi.STATE_CHANGE");

            Button router_info_btn = (Button) mView.findViewById(R.id.router_info_btn);
            router_info_btn.setOnClickListener(mClick);
//            mScrollView = mView.findViewById(R.id.scrollView);

            mScrollView = (NestedScrollView) mView.findViewById(R.id.scrollView);
//            MaterialViewPagerHelper.registerScrollView(getActivity(), mScrollView);

            text_number_wifi_in_range = (TextView) mView.findViewById(R.id.text_number_wifi_in_range);

            CardView txt_wifi_connect = (CardView) mView.findViewById(R.id.txt_wifi_connect);
            txt_wifi_connect.setOnClickListener(mClick);

            TextView txt_wifi_speed_boost = (TextView) mView.findViewById(R.id.txt_wifi_speed_boost);
            txt_wifi_speed_boost.setOnClickListener(mClick);

            CardView txt_wifi_chart = (CardView) mView.findViewById(R.id.txt_wifi_analyzer);
            txt_wifi_chart.setOnClickListener(mClick);

            CardView txt_wifi_map = (CardView) mView.findViewById(R.id.txt_wifi_map);
            txt_wifi_map.setOnClickListener(mClick);

            CardView txt_wifi_speed_test = (CardView) mView.findViewById(R.id.txt_wifi_speed_test);
            txt_wifi_speed_test.setOnClickListener(mClick);

            TextView txt_wifi_hotspot = (TextView) mView.findViewById(R.id.txt_wifi_hotspot);
            txt_wifi_hotspot.setOnClickListener(mClick);

            CardView txt_wifi_map_explorer = (CardView) mView.findViewById(R.id.txt_wifi_map_explorer);
            txt_wifi_map_explorer.setOnClickListener(mClick);

            initAds();

        }

        return mView;
    }

    @Override
    public void onStart() {
        super.onStart();
    }


    @Override
    public void onDestroy() {
        if(adViewfacebook != null){
            adViewfacebook.destroy();
        }
        super.onDestroy();
    }

    private com.facebook.ads.AdView adViewfacebook;
    private void initAds(){

        if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
            //Banner
//            AdView mAdView = (AdView) mView.findViewById(R.id.admob_banner_view);
//            mAdView.setVisibility(View.VISIBLE);
//            AppUtils.getInstance().showAdsBanner(mAdView);

            mPublisherAdView = mView.findViewById(R.id.admob_banner_view);
            PublisherAdRequest adRequest = new PublisherAdRequest.Builder().build();
            mPublisherAdView.setVisibility(View.VISIBLE);
            mPublisherAdView.loadAd(adRequest);

            mPublisherAdView = (PublisherAdView) mView.findViewById(R.id.fluid_view);
            PublisherAdRequest publisherAdRequest = new PublisherAdRequest.Builder().build();
            mPublisherAdView.loadAd(publisherAdRequest);

            mPublisherAdView = (PublisherAdView) mView.findViewById(R.id.fluid_view_1);
            PublisherAdRequest publisherAdRequest_1 = new PublisherAdRequest.Builder().build();
            mPublisherAdView.loadAd(publisherAdRequest_1);
            //load large ads native bottom
//            NativeExpressAdView adViewNative = (NativeExpressAdView) mView.findViewById(R.id.adViewNative);
//            adViewNative.setVisibility(View.VISIBLE);
//            AppUtils.getInstance().showNativeAdsBanner(adViewNative);

//            NativeExpressAdView adViewNative_1 = (NativeExpressAdView) mView.findViewById(R.id.adViewNative_1);
//            adViewNative_1.setVisibility(View.VISIBLE);
//            AppUtils.getInstance().showNativeAdsBanner(adViewNative_1);
        } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
            //Banner
            RelativeLayout adViewContainer = (RelativeLayout) mView.findViewById(R.id.facebook_banner_ad_container);
            adViewContainer.setVisibility(View.VISIBLE);
            adViewfacebook = AppUtils.getInstance().showFBAdsBanner(getActivity(),adViewContainer);

            //Native
            LinearLayout adNativeViewContainer = (LinearLayout) mView.findViewById(R.id.native_ad_container);
            adNativeViewContainer.setVisibility(View.VISIBLE);
            AppUtils.getInstance().showFBAdsNativeSmallInit(getActivity(),adNativeViewContainer);

            LinearLayout adNativeViewContainer_1 = (LinearLayout) mView.findViewById(R.id.native_ad_container_1);
            adNativeViewContainer_1.setVisibility(View.VISIBLE);
            AppUtils.getInstance().showFBAdsNativeInit(getActivity(),adNativeViewContainer_1);
        }
    }


    public void setWiFiNumberInRange(String numbers){
        if(text_number_wifi_in_range != null){
            text_number_wifi_in_range.setText(numbers);
        }
    }

    public void setWiFiHotspotMenuText(String menuText){
        if(mView != null){
            TextView txt_wifi_hotspot = (TextView) mView.findViewById(R.id.txt_wifi_hotspot);
            if(txt_wifi_hotspot != null){
                txt_wifi_hotspot.setText(menuText);
            }
        }
    }

    private View.OnClickListener mClick = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            final Intent intent;
            int id = view.getId();
            switch (id) {
                case R.id.router_info_btn:
                    intent = new Intent(getActivity(), WifiInfoActivity.class);
                    startActivity(intent);
                    return;
                case R.id.txt_wifi_connect:
                    try {

//                        Answers.getInstance().logContentView(new ContentViewEvent()
//                                .putContentName("Free-WiFi-Tab - Fine More Networks"));

//                        if (AppUtils.ads_interstitial_show_all) {
//
//                            if (AppUtils.adsNetworkType != AppUtils.NONE_TYPE) {
//                                AppUtils.getInstance().showAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        Intent intent = new Intent(getActivity(), WifiConnectActivity.class);
//                                        startActivity(intent);
//                                    }
//                                });
//                            } else {
//                                intent = new Intent(getActivity(), WifiConnectActivity.class);
//                                startActivity(intent);
//                            }
//
//                        } else {
//
//                            if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
//                                AppUtils.getInstance().showAdmobAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        Intent intent = new Intent(getActivity(), WifiConnectActivity.class);
//                                        startActivity(intent);
//                                    }
//                                });
//                            } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
//                                AppUtils.getInstance().showFBAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        Intent intent = new Intent(getActivity(), WifiConnectActivity.class);
//                                        startActivity(intent);
//                                    }
//                                });
//                            } else {
//                                intent = new Intent(getActivity(), WifiConnectActivity.class);
//                                startActivity(intent);
//                            }
//                        }

                        intent = new Intent(getActivity(), WhoUseWifiActivity.class);
                        startActivity(intent);

                    }
                    catch (Exception ignored) {
                        Crashlytics.logException(ignored);
                    }
                    break;
                case R.id.txt_wifi_speed_boost:
                    try {

                        Answers.getInstance().logContentView(new ContentViewEvent()
                                .putContentName("Free-WiFi-Tab - Wifi Speed Booster"));

//                        if (AppUtils.ads_interstitial_show_all) {
//
//                            if (AppUtils.adsNetworkType != AppUtils.NONE_TYPE) {
//                                AppUtils.getInstance().showAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        Intent intent = new Intent(getActivity(), OptimizeActivity.class);
//                                        intent.putExtra(MainAppActivity.COLOR_MESSAGE, MainAppActivity.currentColor);
//                                        startActivity(intent);
//                                    }
//                                });
//                            } else {
//                                intent = new Intent(getActivity(), OptimizeActivity.class);
//                                intent.putExtra(MainAppActivity.COLOR_MESSAGE, MainAppActivity.currentColor);
//                                startActivity(intent);
//                            }
//
//                        } else {
//
//                            if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
//                                AppUtils.getInstance().showAdmobAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        Intent intent = new Intent(getActivity(), OptimizeActivity.class);
//                                        intent.putExtra(MainAppActivity.COLOR_MESSAGE, MainAppActivity.currentColor);
//                                        startActivity(intent);
//                                    }
//                                });
//                            } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
//                                AppUtils.getInstance().showFBAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        Intent intent = new Intent(getActivity(), OptimizeActivity.class);
//                                        intent.putExtra(MainAppActivity.COLOR_MESSAGE, MainAppActivity.currentColor);
//                                        startActivity(intent);
//                                    }
//                                });
//                            } else {
//                                intent = new Intent(getActivity(), OptimizeActivity.class);
//                                intent.putExtra(MainAppActivity.COLOR_MESSAGE, MainAppActivity.currentColor);
//                                startActivity(intent);
//                            }
//                        }

                        intent = new Intent(getActivity(), OptimizeActivity.class);
                        intent.putExtra(MainAppActivity.COLOR_MESSAGE, MainAppActivity.currentColor);
                        startActivity(intent);

                    } catch (Exception ignored) {
                        Crashlytics.logException(ignored);
                    }
                    break;
                case R.id.txt_wifi_analyzer:
                    try {

//                        Answers.getInstance().logContentView(new ContentViewEvent()
//                                .putContentName("Free-WiFi-Tab - Wifi Analyzer"));

//                        if (AppUtils.ads_interstitial_show_all) {
//
//                            if (AppUtils.adsNetworkType != AppUtils.NONE_TYPE) {
//                                AppUtils.getInstance().showAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        Intent intent = new Intent(getActivity(), ChartActivity.class);
//                                        startActivity(intent);
//                                    }
//                                });
//                            } else {
//                                intent = new Intent(getActivity(), ChartActivity.class);
//                                startActivity(intent);
//                            }
//
//                        } else {
//
//                            if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
//                                AppUtils.getInstance().showAdmobAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        Intent intent = new Intent(getActivity(), ChartActivity.class);
//                                        startActivity(intent);
//                                    }
//                                });
//                            } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
//                                AppUtils.getInstance().showFBAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        Intent intent = new Intent(getActivity(), ChartActivity.class);
//                                        startActivity(intent);
//                                    }
//                                });
//                            } else {
//                                intent = new Intent(getActivity(), ChartActivity.class);
//                                startActivity(intent);
//                            }
//                        }

                        intent = new Intent(getActivity(), ChartActivity.class);
                        startActivity(intent);

                    } catch (Exception ignored) {
                        Crashlytics.logException(ignored);
                    }

                    break;
                case R.id.txt_wifi_map:
                    try {

//                        Answers.getInstance().logContentView(new ContentViewEvent()
//                                .putContentName("Free-WiFi-Tab - Network Map Location"));

                        final double lat;
                        final double lng;

                        if(AppUtils.getInstance().getAppNetworkInfo() == null || AppUtils.getInstance().getAppNetworkInfo().getNetworkInformation() == null) {
                            lat = 0;
                            lng = 0;
                        }else {
                            lat = Double.parseDouble(AppUtils.getInstance().getAppNetworkInfo().getNetworkInformation().getLat());
                            lng = Double.parseDouble(AppUtils.getInstance().getAppNetworkInfo().getNetworkInformation().getLon());
                        }

//                        if (AppUtils.ads_interstitial_show_all) {
//
//                            if (AppUtils.adsNetworkType != AppUtils.NONE_TYPE) {
//                                AppUtils.getInstance().showAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        Intent intent = new Intent(AppController.getInstance().getAppContext(), MapsMarkerActivity.class);
//                                        intent.putExtra(MapsMarkerActivity.LAT_MESSAGE, lat);
//                                        intent.putExtra(MapsMarkerActivity.LNG_MESSAGE, lng);
//                                        startActivity(intent);
//                                    }
//                                });
//                            } else {
//                                intent = new Intent(AppController.getInstance().getAppContext(), MapsMarkerActivity.class);
//                                intent.putExtra(MapsMarkerActivity.LAT_MESSAGE, lat);
//                                intent.putExtra(MapsMarkerActivity.LNG_MESSAGE, lng);
//                                startActivity(intent);
//                            }
//
//                        } else {
//
//                            if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
//                                AppUtils.getInstance().showAdmobAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        Intent intent = new Intent(AppController.getInstance().getAppContext(), MapsMarkerActivity.class);
//                                        intent.putExtra(MapsMarkerActivity.LAT_MESSAGE, lat);
//                                        intent.putExtra(MapsMarkerActivity.LNG_MESSAGE, lng);
//                                        startActivity(intent);
//                                    }
//                                });
//                            } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
//                                AppUtils.getInstance().showFBAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        Intent intent = new Intent(AppController.getInstance().getAppContext(), MapsMarkerActivity.class);
//                                        intent.putExtra(MapsMarkerActivity.LAT_MESSAGE, lat);
//                                        intent.putExtra(MapsMarkerActivity.LNG_MESSAGE, lng);
//                                        startActivity(intent);
//                                    }
//                                });
//                            } else {
//
//                                intent = new Intent(AppController.getInstance().getAppContext(), MapsMarkerActivity.class);
//                                intent.putExtra(MapsMarkerActivity.LAT_MESSAGE, lat);
//                                intent.putExtra(MapsMarkerActivity.LNG_MESSAGE, lng);
//                                startActivity(intent);
//                            }
//                        }

                        intent = new Intent(getActivity(),  MapsMarkerActivity.class);
                        intent.putExtra(MapsMarkerActivity.LAT_MESSAGE, lat);
                        intent.putExtra(MapsMarkerActivity.LNG_MESSAGE, lng);
                        startActivity(intent);

                    } catch (Exception ignored) {
                        Crashlytics.logException(ignored);
                    }
                    break;
                case R.id.txt_wifi_map_explorer:
                    try {

//                        Answers.getInstance().logContentView(new ContentViewEvent()
//                                .putContentName("Free-WiFi-Tab - Wi-Fi Map Explorer"));

//                        if (AppUtils.ads_interstitial_show_all) {
//
//                            if (AppUtils.adsNetworkType != AppUtils.NONE_TYPE) {
//                                AppUtils.getInstance().showAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        Intent intent = new Intent(AppController.getInstance().getAppContext(), MapActivity.class);
//                                        startActivity(intent);
//                                    }
//                                });
//                            } else {
//                                intent = new Intent(AppController.getInstance().getAppContext(), MapActivity.class);
//                                startActivity(intent);
//                            }
//
//                        } else {
//
//                            if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
//                                AppUtils.getInstance().showAdmobAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        Intent intent = new Intent(AppController.getInstance().getAppContext(), MapActivity.class);
//                                        startActivity(intent);
//                                    }
//                                });
//                            } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
//                                AppUtils.getInstance().showFBAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        Intent intent = new Intent(AppController.getInstance().getAppContext(), MapActivity.class);
//                                        startActivity(intent);
//                                    }
//                                });
//                            } else {
//                                intent = new Intent(AppController.getInstance().getAppContext(), MapActivity.class);
//                                startActivity(intent);
//                            }
//                        }

                        intent = new Intent(getActivity(),  MapActivity.class);
                        startActivity(intent);

                    } catch (Exception ignored) {
                        Crashlytics.logException(ignored);
                    }

                    break;
                case R.id.txt_wifi_speed_test:
                    try {

//                        Answers.getInstance().logContentView(new ContentViewEvent()
//                                .putContentName("Free-WiFi-Tab - Wifi Speed Test"));

//                        if (AppUtils.ads_interstitial_show_all) {
//
//                            if (AppUtils.adsNetworkType != AppUtils.NONE_TYPE) {
//                                AppUtils.getInstance().showAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        Intent intent = new Intent(getActivity(), SpeedTestMiniActivity.class);
//                                        startActivity(intent);
//                                    }
//                                });
//                            } else {
//                                intent = new Intent(getActivity(), SpeedTestMiniActivity.class);
//                                startActivity(intent);
//                            }
//
//                        } else {
//
//                            if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
//                                AppUtils.getInstance().showAdmobAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        Intent intent = new Intent(getActivity(), SpeedTestMiniActivity.class);
//                                        startActivity(intent);
//                                    }
//                                });
//                            } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
//                                AppUtils.getInstance().showFBAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        Intent intent = new Intent(getActivity(), SpeedTestMiniActivity.class);
//                                        startActivity(intent);
//                                    }
//                                });
//                            } else {
//                                intent = new Intent(getActivity(), SpeedTestMiniActivity.class);
//                                startActivity(intent);
//                            }
//                        }

                        intent = new Intent(getActivity(), Router_password.class);
                        startActivity(intent);

                    } catch (Exception ignored) {
                        Crashlytics.logException(ignored);
                    }
                    break;

                case R.id.txt_wifi_hotspot:

                    try {

                        Answers.getInstance().logContentView(new ContentViewEvent()
                                .putContentName("Free-WiFi-Tab - Free WiFi Hotspot"));

//                        if (AppUtils.ads_interstitial_show_all) {
//
//                            if (AppUtils.adsNetworkType != AppUtils.NONE_TYPE) {
//                                AppUtils.getInstance().showAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        if(mMainListener != null){
//                                            mMainListener.clickHotspot();
//                                        }
//
//                                    }
//                                });
//                            } else {
//                                if(mMainListener != null){
//                                    mMainListener.clickHotspot();
//                                }
//
//                            }
//
//                        } else {
//
//                            if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
//                                AppUtils.getInstance().showAdmobAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        if(mMainListener != null){
//                                            mMainListener.clickHotspot();
//                                        }
//
//                                    }
//                                });
//                            } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
//                                AppUtils.getInstance().showFBAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        if(mMainListener != null){
//                                            mMainListener.clickHotspot();
//                                        }
//
//                                    }
//                                });
//                            } else {
//                                if(mMainListener != null){
//                                    mMainListener.clickHotspot();
//                                }
//
//                            }
//                        }

                        if(mMainListener != null){
                            mMainListener.clickHotspot();
                        }

                    } catch (Exception ignored) {
                        Crashlytics.logException(ignored);
                    }
                    break;

            }
        }
    };

    @Override
    public boolean canScrollVertically(int direction) {
        return mScrollView != null && mScrollView.canScrollVertically(direction);
    }

    @Override
    public void onFlingOver(int y, long duration) {
        if (mScrollView != null) {
            mScrollView.smoothScrollBy(0, y);
        }
    }

    @Override
    public void takeScreenShot() {
        Thread thread = new Thread() {
            @Override
            public void run() {
                Bitmap bitmap = Bitmap.createBitmap(mView.getWidth(),
                        mView.getHeight(), Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(bitmap);
                mView.draw(canvas);
                MainFreeWiFiFragment.this.bitmap = bitmap;
            }
        };

        thread.start();

    }

    @Override
    public Bitmap getBitmap() {
        return bitmap;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.mView = view.findViewById(R.id.container);
    }
    private void getNetworkInfo(NetworkInfo networkInfo) {
        this.wifi = new Wireless(getApplicationContext());
        try {
            if (!this.wifi.isEnabled()) {
                this.router_name.setText(R.string.wifiDisabled);
                this.router_coonected_msg.setVisibility(8);
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (networkInfo.isConnected()) {
            try {
                CharSequence ssid = this.wifi.getSSID();
                this.router_coonected_msg.setVisibility(0);
                this.router_name.setText(ssid);
                return;
            } catch (Exception unused) {
                return;
            }
        }
        this.router_name.setText(R.string.noWifiConnection);
        this.router_coonected_msg.setVisibility(8);
    }
    public void liveData() {
        this.dataUpdate = new Thread(new C04683());
        this.dataUpdate.setName("started");
        this.dataUpdate.start();
    }

    public void setSpeed() {
        CharSequence charSequence = " ";
        CharSequence charSequence2 = " ";
        Long valueOf = Long.valueOf(StoredData.downloadSpeed);
        Long valueOf2 = Long.valueOf(StoredData.uploadSpeed);
        StringBuilder stringBuilder;
        if (valueOf.longValue() < PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID) {
            stringBuilder = new StringBuilder();
            stringBuilder.append(valueOf);
            stringBuilder.append("\nB/s");
            charSequence = stringBuilder.toString();
        } else if (valueOf.longValue() < PlaybackStateCompat.ACTION_SET_CAPTIONING_ENABLED) {
            stringBuilder = new StringBuilder();
            stringBuilder.append(this.f132df.format(valueOf.longValue() / PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID));
            stringBuilder.append("\nKB/s");
            charSequence = stringBuilder.toString();
        } else if (valueOf.longValue() >= PlaybackStateCompat.ACTION_SET_CAPTIONING_ENABLED) {
            stringBuilder = new StringBuilder();
            DecimalFormat decimalFormat = this.f132df;
            double longValue = (double) valueOf.longValue();
            Double.isNaN(longValue);
            stringBuilder.append(decimalFormat.format(longValue / 1048576.0d));
            stringBuilder.append("\nMB/s");
            charSequence = stringBuilder.toString();
        }
        StringBuilder stringBuilder2;
        if (valueOf2.longValue() < PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID) {
            stringBuilder2 = new StringBuilder();
            stringBuilder2.append(valueOf2);
            stringBuilder2.append("\nB/s");
            charSequence2 = stringBuilder2.toString();
        } else if (valueOf2.longValue() < PlaybackStateCompat.ACTION_SET_CAPTIONING_ENABLED) {
            stringBuilder2 = new StringBuilder();
            stringBuilder2.append(this.f132df.format(valueOf2.longValue() / PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID));
            stringBuilder2.append("\nKB/s");
            charSequence2 = stringBuilder2.toString();
        } else if (valueOf2.longValue() >= PlaybackStateCompat.ACTION_SET_CAPTIONING_ENABLED) {
            stringBuilder2 = new StringBuilder();
            DecimalFormat decimalFormat2 = this.f132df;
            double longValue2 = (double) valueOf2.longValue();
            Double.isNaN(longValue2);
            stringBuilder2.append(decimalFormat2.format(longValue2 / 1048576.0d));
            stringBuilder2.append("\nMB/s");
            charSequence2 = stringBuilder2.toString();
        }
        this.dSpeed.setText(charSequence);
        this.uSpeed.setText(charSequence2);
        this.dSpeed.setTextSize(18.0f);
        this.uSpeed.setTextSize(18.0f);
    }

    public void onPause() {
        super.onPause();
        unregisterReceiver(this.receiver);
        this.dataUpdate.setName("stopped");
    }

    public void onResume() {
        super.onResume();
        registerReceiver(this.receiver, this.intentFilter);
        DataService.notification_status = true;
        this.dataUpdate.setName("started");
        if (!this.dataUpdate.isAlive()) {
            liveData();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
}
