package wimosalsafispeedtest.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.net.ConnectivityManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.facebook.ads.NativeAdScrollView;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.doubleclick.PublisherAdRequest;
import com.google.android.gms.ads.doubleclick.PublisherAdView;
import com.jao.freewifi.wifimanager.R;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

import wimosalsafifreewifi.application.AppController;
import wimosalsafifreewifi.main.MainAppActivity;
import wimosalsafifreewifi.services.GsonRequest;
import wimosalsafifreewifi.services.ServiceAPI;
import wimosalsafifreewifi.services.model.External_IP;
import wimosalsafifreewifi.services.model.NetworkInformation;
import wimosalsafimainapp.scrollable.fragment.FragmentPagerFragment;
import wimosalsafispeedtest.activity.OptimizeActivity;
import utils.AppUtils;
import utils.Connectivity;
import yalantis.com.sidemenu.interfaces.ScreenShotable;

/**
 * Created by NTL on 8/12/2017 AD.
 */

public class SpeedDetailFragment extends FragmentPagerFragment implements ScreenShotable {

    public static final String TAG = SpeedDetailFragment.class.getSimpleName();
    private TextView network_name,wifi_name,linkspeed,external_ip_address,internal_ip_address,mac_address,security_bssid
            ,ip_city,ip_region,ip_country,ip_latitude,ip_longitude,ip_time_zone,ip_postal_code,ip_asn,ip_org;

    private LinearLayout linear_wifi_name,linear_wifi_security_bssid;

    private View mView;
    private Bitmap bitmap;
//    private SupportMapFragment mapFragment;
//    private GoogleMap mMap;
    private PublisherAdView mPublisherAdView;
    private NestedScrollView mScrollView;

    public static SpeedDetailFragment newInstance()
    {
        SpeedDetailFragment speeddetailfragment = new SpeedDetailFragment();
        return speeddetailfragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if(mView == null){
            mView = inflater.inflate(R.layout.fragment_speed_detail, container, false);

            mScrollView = mView.findViewById(R.id.scrollView);

            wifi_name = (TextView) mView.findViewById(R.id.wifi_name);
            network_name = (TextView) mView.findViewById(R.id.network_name);
            external_ip_address = (TextView) mView.findViewById(R.id.external_ip_address);
            internal_ip_address = (TextView) mView.findViewById(R.id.internal_ip_address);
            mac_address = (TextView) mView.findViewById(R.id.mac_address);
            security_bssid = (TextView) mView.findViewById(R.id.security_bssid);
            linkspeed = (TextView) mView.findViewById(R.id.linkspeed);

            ip_city = (TextView) mView.findViewById(R.id.ip_city);
            ip_region = (TextView) mView.findViewById(R.id.ip_region);
            ip_country = (TextView) mView.findViewById(R.id.ip_country);
            ip_latitude = (TextView) mView.findViewById(R.id.ip_latitude);
            ip_longitude = (TextView) mView.findViewById(R.id.ip_longitude);
            ip_time_zone = (TextView) mView.findViewById(R.id.ip_time_zone);
            ip_postal_code = (TextView) mView.findViewById(R.id.ip_postal_code);
            ip_asn = (TextView) mView.findViewById(R.id.ip_asn);
            ip_org = (TextView) mView.findViewById(R.id.ip_org);

            linear_wifi_name = (LinearLayout) mView.findViewById(R.id.linear_wifi_name);
            linear_wifi_security_bssid = (LinearLayout) mView.findViewById(R.id.linear_wifi_security_bssid);
            linear_wifi_name.setVisibility(View.GONE);
            linear_wifi_security_bssid.setVisibility(View.GONE);

            TextView txt_network_boost = (TextView) mView.findViewById(R.id.txt_network_boost);
            txt_network_boost.setOnClickListener(mClick);

            try{

                if(Connectivity.isConnectedWifi(AppController.getInstance().getAppContext())) {
                    //Wifi
                    linear_wifi_name.setVisibility(View.VISIBLE);
                    linear_wifi_security_bssid.setVisibility(View.VISIBLE);

                    WifiManager wifiMan = (WifiManager) AppController.getInstance().getAppContext().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                    WifiInfo wifiInfo = wifiMan.getConnectionInfo();

                    String ssid = wifiInfo.getSSID();
                    if (!TextUtils.isEmpty(ssid)) {
                        wifi_name.setText(String.format(wifiInfo.getSSID()).substring(1, String.format(wifiInfo.getSSID()).length() - 1));
                        network_name.setText(String.format(wifiInfo.getSSID()).substring(1, String.format(wifiInfo.getSSID()).length() - 1));
                    } else {
                        wifi_name.setText("N/A");
                    }

                    linkspeed.setText(wifiInfo.getLinkSpeed() + " " + WifiInfo.LINK_SPEED_UNITS);

                    int ipAddress = wifiInfo.getIpAddress();
                    internal_ip_address.setText(String.format("%d.%d.%d.%d", (ipAddress & 0xff), (ipAddress >> 8 & 0xff), (ipAddress >> 16 & 0xff), (ipAddress >> 24 & 0xff)));


                    String bssid = wifiInfo.getBSSID();
                    if (!TextUtils.isEmpty(bssid)) {
                        security_bssid.setText(wifiInfo.getBSSID());
                    } else {
                        security_bssid.setText("N/A");
                    }

                }else {


                    //Mobile


                    TelephonyManager manager = (TelephonyManager) AppController.getInstance().getAppContext().getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);
                    String carrierName = manager.getNetworkOperatorName();
                    int networkType = manager.getNetworkType();

//                    Log.d("getNetworkOperatorName",manager.getDeviceId());
//                    Log.d("getNetworkOperatorName",manager.getDeviceSoftwareVersion());
//                    Log.d("getNetworkOperatorName",manager.getLine1Number());
//                    Log.d("getNetworkOperatorName",manager.getNetworkOperator());
//                    Log.d("getNetworkOperatorName",manager.getNetworkOperatorName());
//                    Log.d("getNetworkOperatorName",manager.getNetworkCountryIso());
//                    Log.d("getNetworkOperatorName",manager.getSimOperator());
//                    Log.d("getNetworkOperatorName",manager.getSimOperatorName());
//                    Log.d("getNetworkOperatorName",manager.getSimSerialNumber());
//                    Log.d("getNetworkOperatorName",manager.getSubscriberId());

                    if (!TextUtils.isEmpty(carrierName)) {
                        network_name.setText(carrierName);
                    }

                    String mobile_ip_address = "";
                    try {
                        boolean isbreak = false;
                        for (Enumeration<NetworkInterface> en = NetworkInterface
                                .getNetworkInterfaces(); en.hasMoreElements();) {
                            NetworkInterface intf = en.nextElement();


                            for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
                                InetAddress inetAddress = enumIpAddr.nextElement();

                                if (!inetAddress.isLoopbackAddress()) {
                                    mobile_ip_address = inetAddress.getHostAddress();
                                    internal_ip_address.setText(mobile_ip_address);
                                    isbreak = true;
                                    break;
                                }
                            }

                            if(isbreak) {
                                break;
                            }
                        }

                        linkspeed.setText(Connectivity.isConnectionHowFast(ConnectivityManager.TYPE_MOBILE,manager.getNetworkType()));
                    } catch (Exception e) {
                        Crashlytics.logException(e);
                    }
                }

                //macAddress.setText(wifiInfo.getMacAddress());
                //Fixed bug for Android 6.0 Marshmallow and later
                mac_address.setText(getMacAddress());

            } catch (Exception e){
                Crashlytics.logException(e);
            }

            //Get Network information
//            getNetworkInformation();

            initAds();

        }

        return mView;
    }

//    @Override
//    public void onStart() {
//        super.onStart();
//    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //Get Network information
        getNetworkInformation();
    }

    @Override
    public void onDestroy() {
        if(adViewfacebook != null){
            adViewfacebook.destroy();
        }
        super.onDestroy();
    }

    private com.facebook.ads.AdView adViewfacebook;
    private NativeAdScrollView nativeAdScrollView;
    private void initAds(){

        if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
            //Banner
//            AdView mAdView = (AdView) mView.findViewById(R.id.admob_banner_view);
//            mAdView.setVisibility(View.VISIBLE);
//            AppUtils.getInstance().showAdsBanner(mAdView);

            mPublisherAdView = mView.findViewById(R.id.admob_banner_view);
            PublisherAdRequest adRequest = new PublisherAdRequest.Builder().build();
            mPublisherAdView.setVisibility(View.VISIBLE);
            mPublisherAdView.loadAd(adRequest);

//            mPublisherAdView = (PublisherAdView) mView.findViewById(R.id.fluid_view);
//            PublisherAdRequest publisherAdRequest = new PublisherAdRequest.Builder().build();
//            mPublisherAdView.loadAd(publisherAdRequest);
            mPublisherAdView = (PublisherAdView) mView.findViewById(R.id.fluid_view_1);
            PublisherAdRequest publisherAdRequest_1 = new PublisherAdRequest.Builder().build();
            mPublisherAdView.loadAd(publisherAdRequest_1);
            //load large ads native bottom
//            NativeExpressAdView adViewNative = (NativeExpressAdView) mView.findViewById(R.id.adViewNative);
//            adViewNative.setVisibility(View.VISIBLE);
//            AppUtils.getInstance().showNativeAdsBanner(adViewNative);

//            NativeExpressAdView adViewNative_1 = (NativeExpressAdView) mView.findViewById(R.id.adViewNative_1);
//            adViewNative_1.setVisibility(View.VISIBLE);
//            AppUtils.getInstance().showNativeAdsBanner(adViewNative_1);
        } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
            //Banner
            RelativeLayout adViewContainer = (RelativeLayout) mView.findViewById(R.id.facebook_banner_ad_container);
            adViewContainer.setVisibility(View.VISIBLE);
            adViewfacebook = AppUtils.getInstance().showFBAdsBanner(AppController.getInstance().getAppContext(),adViewContainer);

            //Native
            LinearLayout adNativeViewContainer = (LinearLayout) mView.findViewById(R.id.speed_detail_native_ad_container);
            adNativeViewContainer.setVisibility(View.VISIBLE);
            AppUtils.getInstance().showFBAdsNativeSmallInit(AppController.getInstance().getAppContext(),adNativeViewContainer);

            LinearLayout adNativeViewContainer_1 = (LinearLayout) mView.findViewById(R.id.speed_detail_native_ad_container_1);
            adNativeViewContainer_1.setVisibility(View.VISIBLE);
            AppUtils.getInstance().showFBAdsNativeInit(AppController.getInstance().getAppContext(),adNativeViewContainer_1);

        }
    }

    private void getNetworkInformation() {

        if(AppUtils.getInstance().getAppNetworkInfo() == null || AppUtils.getInstance().getAppNetworkInfo().getExternal_IP() == null) {
            getExternal_IP();
        }else {
            external_ip_address.setText(AppUtils.getInstance().getAppNetworkInfo().getExternal_IP().getIp());
        }


        if(AppUtils.getInstance().getAppNetworkInfo() == null || AppUtils.getInstance().getAppNetworkInfo().getNetworkInformation() == null) {
            getNetworkInfo();
        }else {
            ip_city.setText(AppUtils.getInstance().getAppNetworkInfo().getNetworkInformation().getCity());
            ip_region.setText(AppUtils.getInstance().getAppNetworkInfo().getNetworkInformation().getRegionName());
            ip_country.setText(AppUtils.getInstance().getAppNetworkInfo().getNetworkInformation().getCountry());
            ip_latitude.setText(AppUtils.getInstance().getAppNetworkInfo().getNetworkInformation().getLat());
            ip_longitude.setText(AppUtils.getInstance().getAppNetworkInfo().getNetworkInformation().getLon());
            ip_time_zone.setText(AppUtils.getInstance().getAppNetworkInfo().getNetworkInformation().getTimezone());
            ip_postal_code.setText(AppUtils.getInstance().getAppNetworkInfo().getNetworkInformation().getZip());
            ip_asn.setText(AppUtils.getInstance().getAppNetworkInfo().getNetworkInformation().getAs());
            ip_org.setText(AppUtils.getInstance().getAppNetworkInfo().getNetworkInformation().getOrg());
        }
    }


    private void getNetworkInfo(){
        String get_networkinfo_url = "http://ip-api.com/json/";
        //Volley
        GsonRequest reqNetworkInfo = ServiceAPI.getNetworkInfo(get_networkinfo_url,new Response.Listener<NetworkInformation>() {
            @Override
            public void onResponse(NetworkInformation response) {

                if(response != null){
                    AppUtils.getInstance().getAppNetworkInfo().setNetworkInformation(response);

                    ip_city.setText(response.getCity());
                    ip_region.setText(response.getRegionName());
                    ip_country.setText(response.getCountry());
                    ip_latitude.setText(response.getLat());
                    ip_longitude.setText(response.getLon());
                    ip_time_zone.setText(response.getTimezone());
                    ip_postal_code.setText(response.getZip());
                    ip_asn.setText(response.getAs());
                    ip_org.setText(response.getOrg());
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        reqNetworkInfo.setShouldCache(false);
        AppController.getInstance().addToRequestQueue(reqNetworkInfo, TAG);
    }

    private void getExternal_IP(){

        String get_external_ip_url = "https://api.ipify.org?format=json";
        //Volley
        GsonRequest reqExternalIp = ServiceAPI.getExternalIP(get_external_ip_url,new Response.Listener<External_IP>() {
            @Override
            public void onResponse(External_IP response) {

                if(response != null){
                    external_ip_address.setText(response.getIp());
                    AppUtils.getInstance().getAppNetworkInfo().setExternal_IP(response);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        reqExternalIp.setShouldCache(false);
        AppController.getInstance().addToRequestQueue(reqExternalIp, TAG);
    }

    public String getMacAddress() {
        try {
            List<NetworkInterface> networkInterfaceList = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface networkInterface : networkInterfaceList) {
                if (!networkInterface.getName().equalsIgnoreCase("wlan0")) {
                    continue;
                }

                byte[] macAddress = networkInterface.getHardwareAddress();
                if (macAddress == null) {
                    return "";
                }

                StringBuilder result = new StringBuilder();
                for (byte data : macAddress) {
                    result.append(Integer.toHexString(data & 0xFF)).append(":");
                }

                if (result.length() > 0) {
                    result.deleteCharAt(result.length() - 1);
                }
                return result.toString();
            }
        } catch (Exception ignored) {
        }
        return "02:00:00:00:00:00";
    }

    /** Get IP For mobile */
    public static String getMobileIP() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface
                    .getNetworkInterfaces(); en.hasMoreElements();) {
                NetworkInterface intf = (NetworkInterface) en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf
                        .getInetAddresses(); enumIpAddr.hasMoreElements();) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        String ipaddress = inetAddress .getHostAddress().toString();
                        return ipaddress;
                    }
                }
            }
        } catch (SocketException ex) {
            Crashlytics.logException(ex);
        }
        return null;
    }

    private void updateNetwork(){

    }

    private View.OnClickListener mClick = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            final Intent intent;
            int id = view.getId();
            switch (id) {
                case R.id.txt_network_boost:
                    try {

                        Answers.getInstance().logContentView(new ContentViewEvent()
                                .putContentName("WiFi-Info-Tab - Wifi Speed Booster"));

                        if (AppUtils.ads_interstitial_show_all) {

                            if (AppUtils.adsNetworkType != AppUtils.NONE_TYPE) {
                                AppUtils.getInstance().showAdsFullBanner(new AppUtils.BaseAdListener() {
                                    @Override
                                    public void onAdClosed() {
                                        Intent intent = new Intent(getActivity(), OptimizeActivity.class);
                                        intent.putExtra(MainAppActivity.COLOR_MESSAGE, MainAppActivity.currentColor);
                                        startActivity(intent);
                                    }
                                });
                            } else {
                                intent = new Intent(getActivity(), OptimizeActivity.class);
                                intent.putExtra(MainAppActivity.COLOR_MESSAGE, MainAppActivity.currentColor);
                                startActivity(intent);
                            }

                        } else {

                            if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
                                AppUtils.getInstance().showAdmobAdsFullBanner(new AppUtils.BaseAdListener() {
                                    @Override
                                    public void onAdClosed() {
                                        Intent intent = new Intent(getActivity(), OptimizeActivity.class);
                                        intent.putExtra(MainAppActivity.COLOR_MESSAGE, MainAppActivity.currentColor);
                                        startActivity(intent);
                                    }
                                });
                            } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
                                AppUtils.getInstance().showFBAdsFullBanner(new AppUtils.BaseAdListener() {
                                    @Override
                                    public void onAdClosed() {
                                        Intent intent = new Intent(getActivity(), OptimizeActivity.class);
                                        intent.putExtra(MainAppActivity.COLOR_MESSAGE, MainAppActivity.currentColor);
                                        startActivity(intent);
                                    }
                                });
                            } else {
                                intent = new Intent(getActivity(), OptimizeActivity.class);
                                intent.putExtra(MainAppActivity.COLOR_MESSAGE, MainAppActivity.currentColor);
                                startActivity(intent);
                            }
                        }

//                        intent = new Intent(getActivity(), OptimizeActivity.class);
//                        intent.putExtra(MainAppActivity.COLOR_MESSAGE, MainAppActivity.currentColor);
//                        startActivity(intent);
                    } catch (Exception ignored) {
                        Crashlytics.logException(ignored);
                    }
                    break;

            }
        }
    };

    @Override
    public boolean canScrollVertically(int direction) {
        return mScrollView != null && mScrollView.canScrollVertically(direction);
    }

    @Override
    public void onFlingOver(int y, long duration) {
        if (mScrollView != null) {
            mScrollView.smoothScrollBy(0, y);
        }
    }
    @Override
    public void takeScreenShot() {
        Thread thread = new Thread() {
            @Override
            public void run() {
                Bitmap bitmap = Bitmap.createBitmap(mView.getWidth(),
                        mView.getHeight(), Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(bitmap);
                mView.draw(canvas);
                SpeedDetailFragment.this.bitmap = bitmap;
            }
        };

        thread.start();

    }

    @Override
    public Bitmap getBitmap() {
        return bitmap;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.mView = view.findViewById(R.id.container);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
