package wimosalsafispeedtest.internet;

import android.app.ProgressDialog;
import android.content.Context;
import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;


import org.w3c.dom.Element;

import java.util.ArrayList;

import wimosalsafispeedtest.util.FeedXmlUtil;

/**
 * Created by Chang on 04/06/15.
 */
public class CreateDatabaseAsynctask extends AsyncTask < Location, String , ArrayList<ArrayList<Float>>> {

    Context mContext;
    private ProgressDialog progressDialog;
    private double latitude = 0.0;
    private double longitude = 0.0;
    private ArrayList<Float> distance;
    private ArrayList<ArrayList<Float>> nearest = new ArrayList<ArrayList<Float>>();
    private int DISTANCE = 0;
    private int INDEX = 1;
    private int PING = 2;
    //private Object mGoogleApiClient;

    public CreateDatabaseAsynctask(Context context){
        this.mContext = context;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = new ProgressDialog(mContext);
        progressDialog.setTitle("Loading");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
    }

    @Override
    protected ArrayList<ArrayList<Float>> doInBackground(Location... params) {
        Location location = params[0];
        latitude = location.getLatitude();
        longitude = location.getLongitude();
        ArrayList<Object> serverXML = FeedXmlUtil.feed(mContext);
        Log.d("Server index","" + location);
        distance = new ArrayList<>();
        int length = serverXML.size();
        Element e ;
        float[] mDistance = new float[length];
        for(int i = 0;i < length ; i++){
            e = (Element) serverXML.get(i);
            Double serverLatitude = Double.valueOf(e.getAttribute("lat"));
            Double serverLongitude = Double.valueOf(e.getAttribute("lon"));
            Location.distanceBetween(latitude, longitude, serverLatitude, serverLongitude, mDistance);
            Log.d("Far from this", mDistance[0] / 1000 + "KM");
            distance.add((float) (mDistance[0] / 1000));
            //getNearestLocation();
        }
        /**Code test arraylist is work? **/
        int j;
        for(j=0;j<distance.size();j++){
            Log.d( "distance" , String.valueOf(distance.get(j)));
            getNearestLocation(distance.get(j), j);
        }
        /*for (j = 0 ; j < nearest.size() ; j++){
            Log.d("Nearest data", nearest.get(j).get(0) + ":" + nearest.get(j).get(1));
        }*/
        Log.d("Nearest data",nearest.size() + " location");
        Log.d("Server index",latitude + " " + longitude);
        int top10 = nearest.size();
        for (j = 1; j <= 10 ; j++){
            Integer _position = top10 - j;
            Float _distance = nearest.get(_position).get(DISTANCE);
            Integer _index = Math.round(nearest.get(_position).get(INDEX));
            Element _element = (Element) serverXML.get(_index);
            String _sponsor = _element.getAttribute("sponsor");
            Log.d("Server index", j+ " " + _sponsor + " : " + _distance + " Km");
        }

        return nearest;
    }

    @Override
    protected void onPostExecute(ArrayList s) {
        super.onPostExecute(s);
        progressDialog.dismiss();
    }



    public ArrayList getNearestLocation(Float distance, int index){
        if (nearest.size()>0){
            if(nearest.get(nearest.size()-1).get(0) >= distance) {
                ArrayList<Float> data = new ArrayList<Float>();
                data.add(distance);
                data.add((float) index);
                nearest.add(data);
        }
        }else {
            ArrayList<Float> data = new ArrayList<Float>();
            data.add(distance);
            data.add((float) index);
            nearest.add(data);
        }
        return nearest;
    }

}
