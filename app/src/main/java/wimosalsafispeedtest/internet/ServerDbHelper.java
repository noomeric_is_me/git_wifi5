package wimosalsafispeedtest.internet;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import org.w3c.dom.Element;

import java.net.MalformedURLException;
import java.net.URL;

public class ServerDbHelper {

    public static final String DB_NAME  = "server.db";
    public static final String DB_TABLE = "server_list";
    public static final int DB_VERSION  = 1;

    public static final String ID_CLSTR = "_id";
    public static final String URL_CLSTR = "url";
    public static final String HOST_CLSTR = "host";
    public static final String LAT_CLSTR = "latitude";
    public static final String LON_CLSTR = "longitude";
    public static final String SPONSOR_CLSTR = "sponsor";
    public static final String COUNTRY_CLSTR = "country";
    public static final String CC_CLSTR = "cc";

    public static final int ID_CLINT = 0;
    public static final int URL_CLINT = 1;
    public static final int HOST_CLINT = 2;
    public static final int LAT_CLINT = 3;
    public static final int LON_CLINT = 4;
    public static final int SPONSOR_CLINT = 5;
    public static final int COUNTRY_CLINT = 6;
    public static final int CC_CLINT = 7;

    private SQLiteDatabase db;
    private final Context context;
    private serverDBOpenHelper dbHelper;

    public ServerDbHelper(Context context) {
        this.context = context;
        dbHelper = new serverDBOpenHelper(context, DB_NAME,
                null, DB_VERSION);
    }

    private static class serverDBOpenHelper extends SQLiteOpenHelper {

        private static final String DB_CREATE =
                "create table " + DB_TABLE +
                        " (" +
                        ID_CLSTR + " integer primary key autoincrement, " +
                        URL_CLSTR + " text not null, " +
                        HOST_CLSTR + " text not null, " +
                        LAT_CLSTR + " text not null, " +
                        LON_CLSTR + " text not null, " +
                        SPONSOR_CLSTR + " text not null, " +
                        COUNTRY_CLSTR + " text not null, " +
                        CC_CLSTR + " text not null " +
                        ");";

        public serverDBOpenHelper(Context context, String name,
                                  CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(DB_CREATE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.w("TaskDBAdapter", "Upgrading from version " +
                    oldVersion + " to " +
                    newVersion + ", which will destroy all old data");
            db.execSQL("DROP TABLE IF EXISTS " + DB_TABLE);
            onCreate(db);
        }
    }

    public void close() {
        db.close();
    }

    public void open() throws SQLiteException {
        try {
            db = dbHelper.getWritableDatabase();
            Log.w("OPEN DB: ", "WRITEABLE DB CREATED");
        }
        catch ( SQLiteException ex ) {
            Log.w("OPEN DB: ", "READABLE DB CREATED");
            db = dbHelper.getReadableDatabase();
        }
    }

    public long insertServer(Object server) {
        //String url = null;
        Element e = (Element) server;
        URL hostParse = null;
        String url = e.getAttributes().getNamedItem("url").getNodeValue();
        try {
            hostParse = new URL(url);
        } catch (MalformedURLException e1) {
            e1.printStackTrace();
        }
        String host = hostParse.getHost();
        String lat = e.getAttributes().getNamedItem("lat").getNodeValue();
        String lon = e.getAttributes().getNamedItem("lon").getNodeValue();
        String sponsor = e.getAttributes().getNamedItem("sponsor").getNodeValue();
        String country = e.getAttributes().getNamedItem("country").getNodeValue();
        String cc = e.getAttributes().getNamedItem("cc").getNodeValue();
        open();
        ContentValues newServer = new ContentValues();
        newServer.put(URL_CLSTR, url);
        newServer.put(HOST_CLSTR, host);
        newServer.put(LAT_CLSTR, lat);
        newServer.put(LON_CLSTR, lon);
        newServer.put(SPONSOR_CLSTR, sponsor);
        newServer.put(COUNTRY_CLSTR, country);
        newServer.put(CC_CLSTR, cc);
        long insertedRowIndex = db.insert(DB_TABLE, null, newServer);
        Log.w("INSERT TASK", "Inserted " + insertedRowIndex
                + server.toString());
        close();
        return insertedRowIndex;
    }

    public Cursor getAllCursor() {
        return db.query(DB_TABLE,
                new String[] {
                        ID_CLSTR,
                        URL_CLSTR,
                        HOST_CLSTR,
                        LAT_CLSTR,
                        LON_CLSTR,
                        SPONSOR_CLSTR,
                        COUNTRY_CLSTR,
                        CC_CLSTR
                },
                null, null, null, null, null ,null );
    }


    public String retrieveByCountry(String ln) {

        Cursor mathCursor =
                db.query(DB_TABLE,
                        new String[] {
                                ID_CLSTR,
                                URL_CLSTR,
                                HOST_CLSTR,
                                LAT_CLSTR,
                                LON_CLSTR,
                                SPONSOR_CLSTR,
                                COUNTRY_CLSTR,
                                CC_CLSTR
                        },
                        COUNTRY_CLSTR + "=" + ln, null, null, null, null);

        if ( mathCursor.moveToFirst() ) {
            String host = mathCursor.getString(HOST_CLINT);
            return host;
        }
        else
            return "";
    }


}