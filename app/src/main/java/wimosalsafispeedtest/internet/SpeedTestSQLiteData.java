package wimosalsafispeedtest.internet;

public class SpeedTestSQLiteData {

    String date, time, download, upload, ping;
    int id;

    public SpeedTestSQLiteData() {
        super();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDownload() {
        return download;
    }

    public void setDownload(String download) {
        this.download = download;
    }

    public String getUpload() {
        return upload;
    }

    public void setUpload(String upload) {
        this.upload = upload;
    }

    public String getPing() {
        return ping;
    }

    public void setPing(String ping) {
        this.ping = ping;
    }

    public SpeedTestSQLiteData(String date, String time, String download, String upload, String ping) {
        super();
        this.date = date;
        this.time = time;
        this.download = download;
        this.upload = upload;
        this.ping = ping;

    }


}
