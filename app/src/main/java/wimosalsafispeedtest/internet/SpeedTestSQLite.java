package wimosalsafispeedtest.internet;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class SpeedTestSQLite extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "speedtestsqlitedb";

    // Contacts table name
    private static final String TABLE_CONTACTS = "sqlitedb";

    // Contacts Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_DATE = "date";
    private static final String KEY_TIME = "time";
    private static final String KEY_DOWNLOAD = "download";
    private static final String KEY_UPLOAD = "upload";
    private static final String KEY_PING = "ping";


    public SpeedTestSQLite(Context group) {
        super(group, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_CONTACTS + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_DATE
                + " TEXT, " + KEY_TIME + " TEXT," + KEY_DOWNLOAD + " TEXT, "
                + KEY_UPLOAD + " TEXT," + KEY_PING + " TEXT" + ")";

        db.execSQL(CREATE_CONTACTS_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACTS);

        // Create tables again
        onCreate(db);
    }

    public List<String> getAllno() {
        List<String> grouplist = new ArrayList<String>();
        // Select All Query
        String selectQuery = "SELECT id FROM " + TABLE_CONTACTS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {

                grouplist.add(cursor.getString(0));

            } while (cursor.moveToNext());
        }
        cursor.close();

        // return contact list
        return grouplist;
    }

    public void addGroup(SpeedTestSQLiteData item) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(KEY_DATE, item.getDate());
        values.put(KEY_TIME, item.getTime());
        values.put(KEY_DOWNLOAD, item.getDownload());
        values.put(KEY_UPLOAD, item.getUpload());
        values.put(KEY_PING, item.getPing());

        // Inserting Row
        db.insert(TABLE_CONTACTS, null, values);
        db.close(); // Closing database connection
    }

    public int updateContact(SpeedTestSQLiteData item, String updatedtitle) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(KEY_DATE, item.getDate());
        values.put(KEY_TIME, item.getTime());
        values.put(KEY_DOWNLOAD, item.getDownload());
        values.put(KEY_UPLOAD, item.getUpload());
        values.put(KEY_PING, item.getPing());

        return db.update(TABLE_CONTACTS, values, KEY_DATE + " = ?",
                new String[] { String.valueOf(item.getDate()) });

    }

    public void deleteitem(int id1) {

        // Select All Query

        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_CONTACTS, KEY_ID + "=" + id1, null);
        db.close();

    }

    public List<SpeedTestSQLiteData> getAllitems() {
        List<SpeedTestSQLiteData> allitemdata = new ArrayList<SpeedTestSQLiteData>();

        String selectQuery = "SELECT * FROM " + TABLE_CONTACTS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToLast()) {
            do {

                SpeedTestSQLiteData grp = new SpeedTestSQLiteData();
                grp.setId(Integer.parseInt(cursor.getString(0)));
                grp.setDate(cursor.getString(1));
                grp.setTime(cursor.getString(2));
                grp.setDownload(cursor.getString(3));
                grp.setUpload(cursor.getString(4));
                grp.setPing(cursor.getString(5));

                allitemdata.add(grp);
            } while (cursor.moveToPrevious());
        }
        cursor.close();

        return allitemdata;

    }

    public List<SpeedTestSQLiteData> getselectedTitle(String title) {
        List<SpeedTestSQLiteData> allitemdata = new ArrayList<SpeedTestSQLiteData>();

        String selectQuery = "SELECT * FROM " + TABLE_CONTACTS
                + " WHERE title = '" + title + "'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {

                SpeedTestSQLiteData grp = new SpeedTestSQLiteData();
                grp.setId(Integer.parseInt(cursor.getString(0)));
                grp.setDate(cursor.getString(1));
                grp.setTime(cursor.getString(2));
                grp.setDownload(cursor.getString(3));
                grp.setUpload(cursor.getString(4));
                grp.setPing(cursor.getString(5));

                allitemdata.add(grp);
            } while (cursor.moveToNext());
        }
        cursor.close();

        return allitemdata;

    }

    public int number() {

        String countQuery = "SELECT * FROM " + TABLE_CONTACTS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        return cursor.getCount();
    }
}