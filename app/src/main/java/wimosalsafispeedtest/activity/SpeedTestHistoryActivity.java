package wimosalsafispeedtest.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.ads.doubleclick.PublisherAdRequest;
import com.google.android.gms.ads.doubleclick.PublisherAdView;
import com.jao.freewifi.wifimanager.R;
import com.google.android.gms.ads.AdView;

import wimosalsafifreewifi.main.MainAppActivity;
import utils.AppUtils;

public class SpeedTestHistoryActivity extends AppCompatActivity {
    private PublisherAdView mPublisherAdView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.speedtest_history_activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
        }

        Intent intent = getIntent();
        int color_message = intent.getIntExtra(MainAppActivity.COLOR_MESSAGE,0);

        if(color_message != 0){
            toolbar.setBackgroundColor(color_message);
        }

        //Init admob or facebook
        initAds();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
//                finish();
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private com.facebook.ads.AdView adViewfacebook;
    private void initAds(){

        if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
            //Show Admob Ads

            //Banner
//            AdView mAdView = (AdView) findViewById(R.id.admob_banner_view);
//            mAdView.setVisibility(View.VISIBLE);
//            AppUtils.getInstance().showAdsBanner(mAdView);

            mPublisherAdView = findViewById(R.id.admob_banner_view);
            PublisherAdRequest adRequest = new PublisherAdRequest.Builder().build();
            mPublisherAdView.setVisibility(View.VISIBLE);
            mPublisherAdView.loadAd(adRequest);

        } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
            //Show Facebook Ads

            RelativeLayout adViewContainer = (RelativeLayout) findViewById(com.jao.freewifi.wifimanager.R.id.adViewContainer);
            adViewfacebook = AppUtils.getInstance().showFBAdsBanner(this,adViewContainer);
        }
    }

    @Override
    public void onDestroy() {
        if(adViewfacebook != null){
            adViewfacebook.destroy();
        }
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {

        try {
            if(AppUtils.ads_interstitial_show_all) {

                if (AppUtils.adsNetworkType != AppUtils.NONE_TYPE) {
                    AppUtils.getInstance().showAdsFullBanner(null);
                }

            }else {

                if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {

                    AppUtils.getInstance().showAdmobAdsFullBanner(null);

                } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {

                    AppUtils.getInstance().showFBAdsFullBanner(null);

                }
            }
        } catch (Exception ignored) {
            Crashlytics.logException(ignored);
        }

        super.onBackPressed();
    }
}
