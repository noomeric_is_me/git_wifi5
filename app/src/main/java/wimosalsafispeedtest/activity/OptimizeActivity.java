package wimosalsafispeedtest.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jao.freewifi.wifimanager.R;
import com.crashlytics.android.Crashlytics;
import com.facebook.ads.AdError;
import com.facebook.ads.NativeAd;
import com.facebook.ads.NativeAdScrollView;
import com.facebook.ads.NativeAdView;
import com.facebook.ads.NativeAdsManager;
import com.github.jlmd.animatedcircleloadingview.AnimatedCircleLoadingView;
import com.skyfishjy.library.RippleBackground;

import wimosalsafifreewifi.main.MainAppActivity;
import nl.dionsegijn.konfetti.KonfettiView;
import nl.dionsegijn.konfetti.models.Shape;
import nl.dionsegijn.konfetti.models.Size;
import utils.AppUtils;


public class OptimizeActivity extends AppCompatActivity {

    private AnimatedCircleLoadingView animatedCircleLoadingView;
//    private RelativeLayout RelativeLayout_button;
    private Handler mHandler;
    private NativeAdsManager manager;
    private NativeAdScrollView nativeAdScrollView;
    private boolean optimize_success;
    private int color_message;
    private AnimatedCircleLoadingView circle_loading_view;
    private RippleBackground rippleBackground;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_optimize);

        Intent intent = getIntent();
        color_message = intent.getIntExtra(MainAppActivity.COLOR_MESSAGE,0);

        mHandler = new Handler();

        if(color_message != 0){
            RelativeLayout main_optimize_layout = (RelativeLayout) findViewById(R.id.main_optimize_layout);
            main_optimize_layout.setBackgroundColor(color_message);
            circle_loading_view = (AnimatedCircleLoadingView) findViewById(R.id.circle_loading_view);
            circle_loading_view.setBackgroundColor(color_message);
        }


//        RelativeLayout_button = (RelativeLayout) findViewById(R.id.RelativeLayout_button);
//        RelativeLayout_button.setVisibility(View.GONE);

//        ImageView imageView=(ImageView)findViewById(R.id.icon_speedup);
//        imageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                rippleBackground.startRippleAnimation();
//            }
//        });

        Button button_done = (Button) findViewById(R.id.button_done);
        button_done.setVisibility(View.GONE);
        button_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();

//                try {
//                    mHandler.postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
//                                AppUtils.getinstnce().showAdmobAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        finish();
//                                    }
//                                });
//                            } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
//                                AppUtils.getinstnce().showFBAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        finish();
//                                    }
//                                });
//                            }
//                        }
//                    }, 200);
//                } catch (Exception ignored) {
//                    Crashlytics.logException(ignored);
//                }
            }
        });

        // Initialize a NativeAdsManager and request 5 ads
        manager = new NativeAdsManager(this, OptimizeActivity.this.getResources().getString(R.string.fb_id_ads_native), 1);
        manager.setListener(new NativeAdsManager.Listener() {
            @Override
            public void onAdsLoaded() {
                if(manager.isLoaded()){
                    nativeAdScrollView = new NativeAdScrollView(OptimizeActivity.this, manager,
                            NativeAdView.Type.HEIGHT_300);
                    LinearLayout hscrollContainer = (LinearLayout) findViewById(R.id.hscrollContainer);
                    hscrollContainer.addView(nativeAdScrollView);
                }
            }

            @Override
            public void onAdError(AdError adError) {
                // Ad error callback
            }
        });
        manager.loadAds(NativeAd.MediaCacheFlag.ALL);


        animatedCircleLoadingView = (AnimatedCircleLoadingView) findViewById(R.id.circle_loading_view);
        startLoading();
        startPercentMockThread();

        rippleBackground = (RippleBackground)findViewById(R.id.content);
        rippleBackground.startRippleAnimation();
    }

    @Override
    public void onBackPressed() {

        if(optimize_success){
            super.onBackPressed();
        }

    }

    private void startLoading() {
        animatedCircleLoadingView.startDeterminate();
    }

    private void startPercentMockThread() {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1200);
                    for (int i = 0; i <= 100; i++) {
                        Thread.sleep(50);
                        changePercent(i);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        new Thread(runnable).start();
    }

    private void changePercent(final int percent) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                animatedCircleLoadingView.setPercent(percent);
                if(percent >= 100){

//                    circle_loading_view.setVisibility(View.GONE);

//                    RelativeLayout_button.setVisibility(View.VISIBLE);
                    Button button_done = (Button) findViewById(R.id.button_done);
                    button_done.setVisibility(View.VISIBLE);

                    TextView txt_battery_time_remaining_hour = (TextView) findViewById(R.id.txt_battery_time_remaining_hour);
                    txt_battery_time_remaining_hour.setText("BOOST SPEED SUCCESSFULLY");

                    LinearLayout hscrollContainer = (LinearLayout) findViewById(R.id.hscrollContainer);
                    hscrollContainer.setVisibility(View.VISIBLE);

                    optimize_success = true;

                    rippleBackground.stopRippleAnimation();
                    rippleBackground.setVisibility(View.GONE);

                    final KonfettiView konfettiView = (KonfettiView)findViewById(R.id.viewKonfetti);

                    konfettiView.build()
                            .addColors(Color.YELLOW, Color.GREEN, Color.MAGENTA)
                            .setDirection(0.0, 359.0)
                            .setSpeed(1f, 5f)
                            .setFadeOutEnabled(true)
                            .setTimeToLive(2000L)
                            .addShapes(Shape.RECT, Shape.CIRCLE)
                            .addSizes(new Size(12, 5f))
                            .setPosition(-50f, konfettiView.getWidth() + 50f, -50f, -50f)
                            .stream(300, 5000L);
                }
            }
        });
    }


    public void resetLoading() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                animatedCircleLoadingView.resetLoading();
            }
        });
    }
}