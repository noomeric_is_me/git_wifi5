package wimosalsafispeedtest.util;

import com.crashlytics.android.Crashlytics;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;

import java.io.InputStream;
import java.util.ArrayList;

public class FeedJsonUtil {

    public static ArrayList<Object> feed(String url) {

        ArrayList<Object> _feedList = new ArrayList<Object>();

        HttpClient httpclient = new DefaultHttpClient();
        HttpGet httpget = new HttpGet(url);
        HttpResponse response;
        try {
            response = httpclient.execute(httpget);
            HttpEntity entity = response.getEntity();
            if (entity != null) {

                InputStream instream = entity.getContent();
                String result = StringUtil.convertStreamToString(instream);
                JSONArray ja = new JSONArray(result);
                for (int j = 0; j < ja.length(); j++) {
                    _feedList.add(ja.get(j));
                }
                // Closing the input stream will trigger connection release
                instream.close();
            }
        } catch (Exception e) {
            Crashlytics.logException(e);
            return null;
        }

        return _feedList;
    }

}
