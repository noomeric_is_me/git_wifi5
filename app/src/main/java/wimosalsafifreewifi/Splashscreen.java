package wimosalsafifreewifi;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;

import com.jao.freewifi.wifimanager.R;

import wimosalsafifreewifi.main.MainAppActivity;

public class Splashscreen extends Activity {
    final Handler h = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setupImageSplashScreen();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    private void setupImageSplashScreen() {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.splashscreen);

        //Load imageview_splash_screen
//        Glide.with(this)
//                .load(R.drawable.splashscreen)
//                .fitCenter()
//                .into(imageview_splash_screen);

        runDelayed(1000);
    }

    private void runDelayed(int delay){
        h.postDelayed(startMainApp, delay);
    }

    private Runnable startMainApp = new Runnable() {
        @Override
        public void run() {
            setupToStart();
        }
    };

    private void setupToStart() {
        Intent i = new Intent(getApplicationContext(), MainAppActivity.class);
        startActivity(i);
        Splashscreen.this.finish();
        Splashscreen.this.overridePendingTransition(0,0);
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }
}

