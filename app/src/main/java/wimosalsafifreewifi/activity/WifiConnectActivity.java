package wimosalsafifreewifi.activity;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.crashlytics.android.Crashlytics;
import com.jao.freewifi.wifimanager.R;

import wimosalsafifreewifi.application.AppController;
import utils.AppUtils;

/**
 * Created by NTL on 9/20/2017 AD.
 */

public class WifiConnectActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wifi_connect);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle("Free Wi-Fi Networks");
//            toolbar.setLogo(R.drawable.logo_white);
            toolbar.setContentInsetStartWithNavigation(0);
//            toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.white));
        }

        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
//            finish();
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        try {
            if(AppUtils.ads_interstitial_show_all) {

                if (AppUtils.adsNetworkType != AppUtils.NONE_TYPE) {
                    AppUtils.getInstance().showAdsFullBanner(null);
                }

            }else {

                if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {

                    AppUtils.getInstance().showAdmobAdsFullBanner(null);

                } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {

                    AppUtils.getInstance().showFBAdsFullBanner(null);

                }
            }
        } catch (Exception ignored) {
            Crashlytics.logException(ignored);
        }

        super.onBackPressed();
    }

}