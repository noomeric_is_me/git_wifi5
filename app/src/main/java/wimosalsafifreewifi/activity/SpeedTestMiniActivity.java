package wimosalsafifreewifi.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.jao.freewifi.wifimanager.R;

import wimosalsafifreewifi.main.MainAppActivity_v2;
import wimosalsafispeedtest.activity.OptimizeActivity;
import wimosalsafispeedtest.activity.SpeedTestHistoryActivity;
import utils.AppUtils;

/**
 * Created by NTL on 9/24/2017 AD.
 */

public class SpeedTestMiniActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_speed_test);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle("Speed Test");
//            getSupportActionBar().setTitle("Wi-Fi Analyzer");
//            toolbar.setLogo(R.drawable.logo_white);
            toolbar.setContentInsetStartWithNavigation(0);
//            toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.white));
        }

        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
        }

        TextView wifi_chart_boost_speed = (TextView) findViewById(com.jao.freewifi.wifimanager.R.id.wifi_chart_boost_speed);
        wifi_chart_boost_speed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {

                    if (AppUtils.ads_interstitial_show_all) {

                        if (AppUtils.adsNetworkType != AppUtils.NONE_TYPE) {
                            AppUtils.getInstance().showAdsFullBanner(new AppUtils.BaseAdListener() {
                                @Override
                                public void onAdClosed() {
                                    openNetworkBooster();
                                }
                            });
                        } else {
                            openNetworkBooster();
                        }

                    } else {

                        if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
                            AppUtils.getInstance().showAdmobAdsFullBanner(new AppUtils.BaseAdListener() {
                                @Override
                                public void onAdClosed() {
                                    openNetworkBooster();
                                }
                            });
                        } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
                            AppUtils.getInstance().showFBAdsFullBanner(new AppUtils.BaseAdListener() {
                                @Override
                                public void onAdClosed() {
                                    openNetworkBooster();
                                }
                            });
                        } else {
                            openNetworkBooster();
                        }
                    }
//                    Answers.getInstance().logContentView(new ContentViewEvent()
//                            .putContentName("Speed Activity - Wifi Speed Booster"));
//
//                    Intent intent = new Intent(SpeedTestMiniActivity.this, OptimizeActivity.class);
//                    intent.putExtra(MainAppActivity_v2.COLOR_MESSAGE, MainAppActivity_v2.currentColor);
//                    startActivity(intent);
                } catch (Exception ignored) {
                    Crashlytics.logException(ignored);
                }
            }
        });
    }

    private void openNetworkBooster(){
        Answers.getInstance().logContentView(new ContentViewEvent()
                .putContentName("Speed Activity - Wifi Speed Booster"));

        Intent intent = new Intent(SpeedTestMiniActivity.this, OptimizeActivity.class);
        intent.putExtra(MainAppActivity_v2.COLOR_MESSAGE, MainAppActivity_v2.currentColor);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
//        if (item.getItemId() == android.R.id.home) {
//            finish();
//        }
        Intent intent;

        switch (item.getItemId()) {
            case android.R.id.home:
//                finish();
                onBackPressed();
                return true;

            case R.id.speedtest_history:

                try {

                    Answers.getInstance().logContentView(new ContentViewEvent()
                            .putContentName("Speed Activity - Test History Results"));

//                    if(AppUtils.ads_interstitial_show_all) {
//
//                        if (AppUtils.adsNetworkType != AppUtils.NONE_TYPE) {
//                            AppUtils.getInstance().showAdsFullBanner(new AppUtils.BaseAdListener() {
//                                @Override
//                                public void onAdClosed() {
//
//                                    Intent intent = new Intent(SpeedTestMiniActivity.this, SpeedTestHistoryActivity.class);
//                                    intent.putExtra(MainAppActivity_v2.COLOR_MESSAGE, MainAppActivity_v2.currentColor);
//                                    startActivity(intent);
//                                }
//                            });
//                        } else {
//
//                            intent = new Intent(SpeedTestMiniActivity.this, SpeedTestHistoryActivity.class);
//                            intent.putExtra(MainAppActivity_v2.COLOR_MESSAGE, MainAppActivity_v2.currentColor);
//                            startActivity(intent);
//
//                        }
//
//                    }else {
//
//                        if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
//                            AppUtils.getInstance().showAdmobAdsFullBanner(new AppUtils.BaseAdListener() {
//                                @Override
//                                public void onAdClosed() {
//
//                                    Intent intent = new Intent(SpeedTestMiniActivity.this, SpeedTestHistoryActivity.class);
//                                    intent.putExtra(MainAppActivity_v2.COLOR_MESSAGE, MainAppActivity_v2.currentColor);
//                                    startActivity(intent);
//                                }
//                            });
//                        } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
//                            AppUtils.getInstance().showFBAdsFullBanner(new AppUtils.BaseAdListener() {
//                                @Override
//                                public void onAdClosed() {
//
//                                    Intent intent = new Intent(SpeedTestMiniActivity.this, SpeedTestHistoryActivity.class);
//                                    intent.putExtra(MainAppActivity_v2.COLOR_MESSAGE, MainAppActivity_v2.currentColor);
//                                    startActivity(intent);
//                                }
//                            });
//                        } else {
//
//                            intent = new Intent(SpeedTestMiniActivity.this, SpeedTestHistoryActivity.class);
//                            intent.putExtra(MainAppActivity_v2.COLOR_MESSAGE, MainAppActivity_v2.currentColor);
//                            startActivity(intent);
//                        }
//                    }

                    intent = new Intent(SpeedTestMiniActivity.this, SpeedTestHistoryActivity.class);
                    intent.putExtra(MainAppActivity_v2.COLOR_MESSAGE, MainAppActivity_v2.currentColor);
                    startActivity(intent);

                } catch (Exception ignored) {
                    Crashlytics.logException(ignored);
                }

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.speedtest_menu, menu);
        return true;
    }

    @Override
    public void onBackPressed() {

        try {
            if(AppUtils.ads_interstitial_show_all) {

                if (AppUtils.adsNetworkType != AppUtils.NONE_TYPE) {
                    AppUtils.getInstance().showAdsFullBanner(null);
                }

            }else {

                if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {

                    AppUtils.getInstance().showAdmobAdsFullBanner(null);

                } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {

                    AppUtils.getInstance().showFBAdsFullBanner(null);

                }
            }
        } catch (Exception ignored) {
            Crashlytics.logException(ignored);
        }

        super.onBackPressed();
    }

}