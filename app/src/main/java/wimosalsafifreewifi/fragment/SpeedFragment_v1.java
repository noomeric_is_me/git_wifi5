package wimosalsafifreewifi.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jao.freewifi.wifimanager.R;
import com.crashlytics.android.Crashlytics;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;

import wimosalsafispeedtest.fragment.SpeedTestMiniFragment;
import utils.AppUtils;

/**
 * Created by NTL on 7/22/2017 AD.
 */

public class SpeedFragment_v1 extends Fragment {

    private View mRootView;
    private Handler mHandler;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mHandler = new Handler();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (mRootView == null) {
            mRootView = inflater.inflate(R.layout.wificonnectfragment_v1, container,
                    false);

            FragmentPagerItemAdapter adapter = new FragmentPagerItemAdapter(
                    this.getActivity().getSupportFragmentManager(), FragmentPagerItems.with(getActivity())
                    .add("SPEEDTEST+", SpeedTestMiniFragment.class)
                    .create());

            ViewPager viewPager = (ViewPager) mRootView.findViewById(R.id.viewpager);
            viewPager.setAdapter(adapter);

            SmartTabLayout viewPagerTab = (SmartTabLayout) mRootView.findViewById(R.id.viewpagertab);
            viewPagerTab.setViewPager(viewPager);

            viewPagerTab.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {

                    try {
                        mHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
//                                if(AppUtils.ads_interstitial_show_all) {
//
//                                    if (AppUtils.adsNetworkType != AppUtils.NONE_TYPE) {
//                                        AppUtils.getInstance().showAdsFullBanner(null);
//                                    }
//
//                                }else {
//
//                                    if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
//                                        AppUtils.getInstance().showAdmobAdsFullBanner(null);
//                                    } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
//                                        AppUtils.getInstance().showFBAdsFullBanner(null);
//                                    }
//                                }

                                if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
                                    AppUtils.getInstance().showFBAdsFullBanner(null);
                                }
                            }
                        }, 200);
                    } catch (Exception ignored) {
                        Crashlytics.logException(ignored);
                    }

                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }

            });

        }

        return mRootView;
    }
}