package wimosalsafifreewifi.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.facebook.ads.InterstitialAd;
import com.facebook.ads.NativeAd;

import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.doubleclick.PublisherAdRequest;
import com.google.android.gms.ads.doubleclick.PublisherAdView;
import com.mady.wifi.api.wifiHotSpots;
import com.jao.freewifi.wifimanager.R;

import cn.pedant.SweetAlert.SweetAlertDialog;
import utils.AppUtils;
import wimosalsafifreewifi.application.AppController;
import wimosalsafimainapp.scrollable.fragment.FragmentPagerFragment;
import wimosalsafispeedtest.fragment.MainFreeWiFiFragment;
import yalantis.com.sidemenu.interfaces.ScreenShotable;

import static com.thefinestartist.utils.content.ContextUtil.getApplicationContext;
import static com.thefinestartist.utils.content.ContextUtil.getPackageName;

//import fast.cleaner.battery.saver.MainActivity;

/**
 * Created by intag pc on 2/12/2017.
 */

public class HotspotFragment extends FragmentPagerFragment implements ScreenShotable {
    private Button bthotspot_on,bthotspot_off;
    public static final String TAG = HotspotFragment.class.getSimpleName();
    public static final String AP_NAME_WIFI_HOTSPOT_TYPE_KEY = "ap_name_wifi_hotspot_type_key";
    public static final String AP_PASSWORD_WIFI_HOTSPOT_TYPE_KEY = "ap_password_wifi_hotspot_type_key";
    private NestedScrollView mScrollView;
    private Bitmap bitmap;

    public static HotspotFragment newInstance()
    {
        HotspotFragment hotspotfragment = new HotspotFragment();
        return hotspotfragment;
    }

    private wifiHotSpots mHotUtil;
    public static boolean mWifiHotspotEnable;
    private SharedPreferences sharedPref;
    private String vmkoomNameWifiHotspot, vmkoomPasswordWifiHotspot;
    private ImageView vmkoomImageToggle;
    private EditText vmkoomPasswordInput, vmkoomSSIDInput;
    private CheckBox vmkoomShowPasswordCheckbox;
    private TextView vmkoomSettingSave, vmkoomSettingInformation;
    private Switch vmkoomSecuritySwitchToggle;
    ImageView mainbrush,cache,temp,residue,system;
    TextView maintext,cachetext,temptext,residuetext,systemtext;
    public static ImageView mainbutton;
    private Handler mHandler;
    private CardView hotspotconfig;
    private PublisherAdView mPublisherAdView;
    private int i = -1;
    int checkvar=0;
    int alljunk;
    //Ad
    private AdView adView;
    private InterstitialAd interstitialAd;
    private NativeAd nativeAd;
    private LinearLayout nativeAdContainer;
    private LinearLayout adViewLinearLayout;
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;

    private View mRootView;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mRootView == null) {
            mRootView= inflater.inflate(R.layout.hotspot_main, container, false);
            mScrollView = mRootView.findViewById(R.id.scrollView);
            mHandler = new Handler();
            mHotUtil = new wifiHotSpots(getActivity());

//        ImageView iispeedtestii = (ImageView) mRootView.findViewById(R.id.speedtestii);
//        iispeedtestii.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View mRootView) {
//                final Intent intent;
//                intent = new Intent(getActivity(), IntroActivity.class);
//                startActivity(intent);
//            }
//        });
            hotspotconfig = this.mRootView.findViewById(R.id.confighotspot);
            bthotspot_on = (Button) mRootView.findViewById(R.id.HotspotON);
            bthotspot_on.setVisibility(View.GONE);
            bthotspot_on.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View mRootView) {
                    vmkoomShareWifiHotspot();
                }
            });

            bthotspot_off = (Button) mRootView.findViewById(R.id.HotspotOFF);
            bthotspot_off.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View mRootView) {
                    vmkoomShareWifiHotspot();
                }
            });
            vmkoomImageToggle = mRootView.findViewById(R.id.vmkoom_image_toggle);
            vmkoomImageToggle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View mRootView) {
                    vmkoomShareWifiHotspot();
                }
            });

            vmkoomConfigureHotspot();
            initAds();


        }
        return mRootView;
    }

    @Override
    public void onDestroy() {
        if(adViewfacebook != null){
            adViewfacebook.destroy();
        }
        super.onDestroy();
    }

    private com.facebook.ads.AdView adViewfacebook;
    //    private NativeAdsManager manager;
//    private NativeAdScrollView nativeAdScrollView;
    private void initAds(){

        if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
            //Banner
//            AdView mAdView = (AdView) mRootView.findViewById(R.id.admob_banner_view);
//            mAdView.setVisibility(View.VISIBLE);
//            AppUtils.getInstance().showAdsBanner(mAdView);

            mPublisherAdView = mRootView.findViewById(R.id.admob_banner_view);
            PublisherAdRequest adRequest = new PublisherAdRequest.Builder().build();
            mPublisherAdView.setVisibility(View.VISIBLE);
            mPublisherAdView.loadAd(adRequest);

            mPublisherAdView = (PublisherAdView) mRootView.findViewById(R.id.fluid_view);
            PublisherAdRequest publisherAdRequest = new PublisherAdRequest.Builder().build();
            mPublisherAdView.loadAd(publisherAdRequest);
            //load large ads native bottom
//            NativeExpressAdView adViewNative = (NativeExpressAdView) mView.findViewById(R.id.adViewNative);
//            adViewNative.setVisibility(View.VISIBLE);
//            AppUtils.getInstance().showNativeAdsBanner(adViewNative);

//            NativeExpressAdView adViewNative_1 = (NativeExpressAdView) mView.findViewById(R.id.adViewNative_1);
//            adViewNative_1.setVisibility(View.VISIBLE);
//            AppUtils.getInstance().showNativeAdsBanner(adViewNative_1);
        } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
            //Banner
            RelativeLayout adViewContainer = (RelativeLayout) mRootView.findViewById(R.id.facebook_banner_ad_container);
            adViewContainer.setVisibility(View.VISIBLE);
            adViewfacebook = AppUtils.getInstance().showFBAdsBanner(AppController.getInstance().getAppContext(),adViewContainer);

            //Native
            LinearLayout adNativeViewContainer = (LinearLayout) mRootView.findViewById(R.id.hotspot_native_ad_container);
            adNativeViewContainer.setVisibility(View.VISIBLE);
            AppUtils.getInstance().showFBAdsNativeSmallInit(AppController.getInstance().getAppContext(),adNativeViewContainer);
        }
    }

    public void showInterstitial() {
        try {
            if(AppUtils.ads_interstitial_show_all) {

                if (AppUtils.adsNetworkType != AppUtils.NONE_TYPE) {
                    AppUtils.getInstance().showAdsFullBanner(null);
                }

            }else {

                if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {

                    AppUtils.getInstance().showAdmobAdsFullBanner(null);

                } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {

                    AppUtils.getInstance().showFBAdsFullBanner(null);

                }
            }
        } catch (Exception ignored) {
            Crashlytics.logException(ignored);
        }
    }

    private void vmkoomConfigureHotspot() {
        sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        vmkoomNameWifiHotspot = sharedPref.getString(AP_NAME_WIFI_HOTSPOT_TYPE_KEY, "FreeWiFi");
        vmkoomPasswordWifiHotspot = sharedPref.getString(AP_PASSWORD_WIFI_HOTSPOT_TYPE_KEY, "11111111");

        vmkoomSettingInformation = this.mRootView.findViewById(R.id.vmkoom_setting_information);
        vmkoomSettingInformation.setVisibility(View.GONE);

        vmkoomSSIDInput = this.mRootView.findViewById(R.id.vmkoom_edit_SSID);
        vmkoomSSIDInput.setText(vmkoomNameWifiHotspot);

        vmkoomPasswordInput = this.mRootView.findViewById(R.id.vmkoom_edit_password);
        vmkoomPasswordInput.setText(vmkoomPasswordWifiHotspot);

        vmkoomSecuritySwitchToggle = this.mRootView.findViewById(R.id.vmkoom_security_switch_toggle);
        vmkoomSecuritySwitchToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                if (checked) {
                    vmkoomPasswordInput.setEnabled(true);
                    vmkoomPasswordInput.setTextColor(ContextCompat.getColor(getActivity(), R.color.black));
                    vmkoomShowPasswordCheckbox.setEnabled(true);
                    vmkoomShowPasswordCheckbox.setTextColor(ContextCompat.getColor(getActivity(), R.color.black));
                } else {
                    vmkoomPasswordInput.setEnabled(false);
                    vmkoomPasswordInput.setTextColor(ContextCompat.getColor(getActivity(), R.color.gray));
                    vmkoomShowPasswordCheckbox.setEnabled(false);
                    vmkoomShowPasswordCheckbox.setTextColor(ContextCompat.getColor(getActivity(), R.color.gray));
                }
            }
        });

        vmkoomSettingSave = this.mRootView.findViewById(R.id.vmkoom_setting_save);
        vmkoomSettingSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View mRootView) {

//                String tempNameWifiHotspot = sharedPref.getString(AP_NAME_WIFI_HOTSPOT_TYPE_KEY, "FreeWiFi");
//                String tempPasswordWifiHotspot = sharedPref.getString(AP_PASSWORD_WIFI_HOTSPOT_TYPE_KEY, "11111111");

                vmkoomNameWifiHotspot = vmkoomSSIDInput.getText().toString();
                vmkoomPasswordWifiHotspot = vmkoomPasswordInput.getText().toString();

//                if (tempNameWifiHotspot.equals(vmkoomNameWifiHotspot) && tempPasswordWifiHotspot.equals(vmkoomPasswordWifiHotspot)) {
//                    return;
//                }

                Toast.makeText(getActivity(), "Save!", Toast.LENGTH_LONG).show();

                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString(AP_NAME_WIFI_HOTSPOT_TYPE_KEY, vmkoomNameWifiHotspot);
                editor.apply();
                editor.putString(AP_PASSWORD_WIFI_HOTSPOT_TYPE_KEY, vmkoomPasswordWifiHotspot);
                editor.apply();

                if (mWifiHotspotEnable) {
                    mHotUtil.setAndStartHotSpotCheckAndroidVersion_v2(false, vmkoomNameWifiHotspot, vmkoomPasswordWifiHotspot);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            vmkoomStartWifiHotspot();
                        }
                    }, 2000);
                }
            }
        });

        vmkoomPasswordInput.addTextChangedListener(
                new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if (s.toString().trim().length() > 7 && vmkoomSSIDInput.length() > 0) {
                            vmkoomSettingSave.setEnabled(true);
                            vmkoomSettingSave.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorTextCanSave));
                        } else {
                            vmkoomSettingSave.setEnabled(false);
                            vmkoomSettingSave.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorTextCannotSave));
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                    }
                });

        vmkoomSSIDInput.addTextChangedListener(
                new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if (s.toString().trim().length() > 0 && vmkoomPasswordInput.length() > 7) {
                            vmkoomSettingSave.setEnabled(true);
                            vmkoomSettingSave.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorTextCanSave));
                        } else {
                            vmkoomSettingSave.setEnabled(false);
                            vmkoomSettingSave.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorTextCannotSave));
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                    }
                });

        // Toggling the show password CheckBox will mask or unmask the password input EditText
        vmkoomShowPasswordCheckbox = this.mRootView.findViewById(R.id.vmkoom_showPassword);
        vmkoomShowPasswordCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (!vmkoomShowPasswordCheckbox.isChecked()) {
                    vmkoomPasswordInput.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    vmkoomPasswordInput.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {
                    vmkoomPasswordInput.setInputType(InputType.TYPE_CLASS_TEXT);
                    vmkoomPasswordInput.setTransformationMethod(null);
                }
            }
        });
    }

    private static int requestCode_ACTION_MANAGE_WRITE_SETTINGS = 200;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == requestCode_ACTION_MANAGE_WRITE_SETTINGS) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (!Settings.System.canWrite(getApplicationContext())) {
                    vmkoomRetryOpenWifiHotspot();
                } else {
                    vmkoomStartWifiHotspot();
                }
            }
        }
    }

    private void vmkoomRetryOpenWifiHotspot() {
        final CoordinatorLayout coordinatorLayout = (CoordinatorLayout) mRootView.findViewById(R.id.scrollView);
        Snackbar snackbar = Snackbar
                .make(coordinatorLayout, "Unable to Open Free Hotspot", Snackbar.LENGTH_LONG)
                .setAction("RETRY", new View.OnClickListener() {
                    @Override
                    public void onClick(View mRootView) {
                        vmkoomStartWifiHotspot();
                    }
                });

        snackbar.show();
    }

    private void vmkoomShareWifiHotspot() {

        try {

            if (vmkoomSSIDInput.length() < 1) {
                Toast.makeText(getActivity(), "No network name",
                        Toast.LENGTH_LONG).show();
                return;
            }

            if (vmkoomSecuritySwitchToggle.isChecked()) {
                if (vmkoomPasswordInput.toString().trim().length() < 8) {
                    Toast.makeText(getActivity(), "Password (at least 8 character)",
                            Toast.LENGTH_LONG).show();
                    return;
                }
            }

            if (mWifiHotspotEnable) {
                vmkoomStopWiFiHotspot();
                return;
            }

            vmkoomStartWifiHotspot();
            starthotspotdialog();

        } catch (Exception ignored) {
            Crashlytics.logException(ignored);
        }
    }

    public void vmkoomStartWifiHotspot() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.System.canWrite(getActivity())) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS, Uri.parse("package:" + getPackageName()));
                startActivityForResult(intent, requestCode_ACTION_MANAGE_WRITE_SETTINGS);
                return;
            }
        }

        if (mHotUtil != null) {

            String password;
            if (vmkoomSecuritySwitchToggle.isChecked()) {
                password = vmkoomPasswordWifiHotspot;
                vmkoomSettingInformation.setText(
                        "Network name: " + vmkoomNameWifiHotspot + "\n" + "Password: " + vmkoomPasswordWifiHotspot);
            } else {
                password = "";
                vmkoomSettingInformation.setText(
                        "Network name: " + vmkoomNameWifiHotspot + "\n" + "Password: ");
            }

            vmkoomSettingInformation.setVisibility(View.VISIBLE);

            //Start Hotspot
            mHotUtil.setAndStartHotSpotCheckAndroidVersion_v2(true, vmkoomNameWifiHotspot, password);
            mWifiHotspotEnable = true;

            //Change Image
            vmkoomImageToggle.setImageResource(R.drawable.salsa_wifi_enable);
            bthotspot_off.setVisibility(View.GONE);
            bthotspot_on.setVisibility(View.GONE);

            hotspotconfig.setVisibility(View.GONE);
        }

    }

    private void vmkoomStopWiFiHotspot() {
        if (mHotUtil != null) {

            vmkoomSettingInformation.setVisibility(View.GONE);

            //Stop hotspot
            mHotUtil.setAndStartHotSpotCheckAndroidVersion_v2(false, vmkoomNameWifiHotspot, vmkoomPasswordWifiHotspot);

            //Change Image
            vmkoomImageToggle.setImageResource(R.drawable.salsa_wifi_disable);
            bthotspot_off.setVisibility(View.GONE);
            bthotspot_on.setVisibility(View.GONE);
            hotspotconfig.setVisibility(View.VISIBLE);
            mWifiHotspotEnable = false;


            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {

                    try {
                        stophotspotdialog2();


                    } catch (Exception ignored) {
                        Crashlytics.logException(ignored);
                    }

                }
            }, 200);

        }
    }

    private void starthotspotdialog(){
        new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText("Start Wifi Hotspot")
                .setContentText("Network name: " + vmkoomNameWifiHotspot + "\n" + "Password: " + vmkoomPasswordWifiHotspot)
                .setCustomImage(R.drawable.ic_free_wifi_hotspot)
                .setConfirmText("OK")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismissWithAnimation();
                    }
                })
                .show();
    }

    private void stophotspotdialog(){
        new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE)
                .setTitleText("Disable Hotspot ?")
                .setContentText("AP Name : FreeHotspot\nPassword : thankyou")
                .setCustomImage(R.drawable.ic_free_wifi_hotspot)
                .setConfirmText("Disable")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismissWithAnimation();
//                        showInterstitial();
                        if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
                            AppUtils.getInstance().showAdmobAdsFullBanner(null);
                        }
                    }
                })
                .setCancelText("No")
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {

                        sweetAlertDialog.dismissWithAnimation();

                    }
                })
                .show();
    }


    private void stophotspotdialog2(){
        final SweetAlertDialog pDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE)
                .setTitleText("Loading");
        pDialog.show();
        pDialog.setCancelable(false);
        new CountDownTimer(800 * 2, 800) {
            public void onTick(long millisUntilFinished) {
                // you can change the progress bar color by ProgressHelper every 800 millis
                i++;
                switch (i){
                    case 0:
                        pDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.blue_btn_bg_color));
                        break;
                    case 1:
                        pDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.material_deep_teal_50));
                        break;
                    case 2:
                        pDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.success_stroke_color));
                        break;
                    case 3:
                        pDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.material_deep_teal_20));
                        break;
                    case 4:
                        pDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.material_blue_grey_80));
                        break;
                    case 5:
                        pDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.warning_stroke_color));
                        break;
                    case 6:
                        pDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.success_stroke_color));
                        break;
                }
            }

            public void onFinish() {
                i = -1;

                pDialog.setTitleText("Disable Hotspot")
                        .setContentText("Personal Mobile Hotspot already Turn Off")
                        .setConfirmText("OK")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismissWithAnimation();
                                showInterstitial();
                            }
                        })
                        .setCustomImage(R.drawable.ic_free_wifi_hotspot)
                        .changeAlertType(SweetAlertDialog.CUSTOM_IMAGE_TYPE);

            }
        }.start();
    }
    @Override
    public boolean canScrollVertically(int direction) {
        return mScrollView != null && mScrollView.canScrollVertically(direction);
    }

    @Override
    public void onFlingOver(int y, long duration) {
        if (mScrollView != null) {
            mScrollView.smoothScrollBy(0, y);
        }
    }
    @Override
    public void takeScreenShot() {
        Thread thread = new Thread() {
            @Override
            public void run() {
                Bitmap bitmap = Bitmap.createBitmap(mRootView.getWidth(),
                        mRootView.getHeight(), Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(bitmap);
                mRootView.draw(canvas);
                HotspotFragment.this.bitmap = bitmap;
            }
        };

        thread.start();

    }

    @Override
    public Bitmap getBitmap() {
        return bitmap;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.mRootView = view.findViewById(R.id.container);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
