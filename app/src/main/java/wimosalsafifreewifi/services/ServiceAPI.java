package wimosalsafifreewifi.services;

import com.android.volley.Request;
import com.android.volley.Response;
import wimosalsafifreewifi.services.model.AppConfig;
import wimosalsafifreewifi.services.model.External_IP;
import wimosalsafifreewifi.services.model.NetworkInformation;

/**
 * Created by NTL on 1/22/2017 AD.
 */

public class ServiceAPI {

    public static GsonRequest<AppConfig> getAdsRequest(String adsconfigs_url, Response.Listener<AppConfig> listener
            , Response.ErrorListener errorListener){
//        String adsconfigs_url = ServiceAPI.APIdomain;

        return new GsonRequest<>(Request.Method.GET,
                adsconfigs_url,AppConfig.class,null, listener, errorListener);
    }

    public static GsonRequest<External_IP> getExternalIP(String url, Response.Listener<External_IP> listener
            , Response.ErrorListener errorListener){

        return new GsonRequest<>(Request.Method.GET,
                url,External_IP.class,null, listener, errorListener);
    }

    public static GsonRequest<NetworkInformation> getNetworkInfo(String url, Response.Listener<NetworkInformation> listener
            , Response.ErrorListener errorListener){

        return new GsonRequest<>(Request.Method.GET,
                url,NetworkInformation.class,null, listener, errorListener);
    }
}
