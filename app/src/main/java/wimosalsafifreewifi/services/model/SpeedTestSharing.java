package wimosalsafifreewifi.services.model;

/**
 * Created by NTL on 1/22/2017 AD.
 */

public class SpeedTestSharing {
    private String share_contentUrl;
    private String share_setImageUrl;

    public String get_share_contentUrl() {
        return share_contentUrl;
    }

    public void set_share_contentUrl(String share_contentUrl) {
        this.share_contentUrl = share_contentUrl;
    }

    public SpeedTestSharing with_share_contentUrl(String share_contentUrl) {
        this.share_contentUrl = share_contentUrl;
        return this;
    }

    public String get_share_setImageUrl() {
        return share_setImageUrl;
    }

    public void set_share_setImageUrl(String share_setImageUrl) {
        this.share_setImageUrl = share_setImageUrl;
    }

    public SpeedTestSharing with_share_setImageUrl(String share_setImageUrl) {
        this.share_setImageUrl = share_setImageUrl;
        return this;
    }

}
