package wimosalsafifreewifi.services.model;

/**
 * Created by NTL on 1/22/2017 AD.
 */

public class Ads {
    private String source;
    private String ads_banner_id;
    private String ads_interstitial_id;
    private String ads_native_id;

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public Ads withSource(String source) {
        this.source = source;
        return this;
    }

    public String getAds_banner_id() {
        return ads_banner_id;
    }

    public void setAds_banner_id(String ads_banner_id) {
        this.ads_banner_id = ads_banner_id;
    }

    public Ads withAdsBannerId(String adsBannerId) {
        this.ads_banner_id = adsBannerId;
        return this;
    }

    public String getAds_interstitial_id() {
        return ads_interstitial_id;
    }

    public void setAds_interstitial_id(String ads_interstitial_id) {
        this.ads_interstitial_id = ads_interstitial_id;
    }

    public Ads withAdsInterstitialId(String adsInterstitialId) {
        this.ads_interstitial_id = adsInterstitialId;
        return this;
    }


    public String getAds_native_id() {
        return ads_native_id;
    }
}
