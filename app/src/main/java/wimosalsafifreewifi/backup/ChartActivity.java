//package com.bestvmkoomfreewifi;
//
//import android.app.Activity;
//import android.content.BroadcastReceiver;
//import android.content.Context;
//import android.content.Intent;
//import android.content.IntentFilter;
//import android.graphics.Color;
//import android.graphics.Paint;
//import android.net.wifi.ScanResult;
//import android.net.wifi.WifiManager;
//import android.os.Bundle;
//import android.os.Handler;
//import android.util.Log;
//import android.widget.FrameLayout;
//import android.widget.RelativeLayout;
//
//import org.achartengine.ChartFactory;
//import org.achartengine.GraphicalView;
//import org.achartengine.chart.BarChart;
//import org.achartengine.model.XYMultipleSeriesDataset;
//import org.achartengine.model.XYSeries;
//import org.achartengine.renderer.XYMultipleSeriesRenderer;
//import org.achartengine.renderer.XYSeriesRenderer;
//
//import java.util.Collections;
//import java.util.Comparator;
//import java.util.List;
//
//import utils.AppUtils;
//
//public class ChartActivity extends Activity{
//    private String TAG = "debugging";
//
//    private WifiManager wifiManager;
//    private FrameLayout chartLayout;
//    private XYMultipleSeriesRenderer renderer;
//    private GraphicalView chartView;
//
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(com.jao.freewifi.wifimanager.R.layout.activity_chart);
//        chartLayout = (FrameLayout) findViewById(com.jao.freewifi.wifimanager.R.id.chart);
//        wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
//        if(!wifiManager.isWifiEnabled()){
//            wifiManager.setWifiEnabled(true);
//        }
//
//        //Init admob or facebook
//        initAds();
//    }
//
//    private com.facebook.ads.AdView adViewfacebook;
//    private void initAds(){
//        //Show Facebook Ads
//        RelativeLayout adViewContainer = (RelativeLayout) findViewById(com.jao.freewifi.wifimanager.R.id.adViewContainer);
//        adViewfacebook = AppUtils.getinstnce().showFBAdsBanner(this,adViewContainer);
//    }
//
//    @Override
//    protected void onDestroy() {
//        if(adViewfacebook != null){
//            adViewfacebook.destroy();
//        }
//        super.onDestroy();
//    }
//
//    @Override
//    public void onResume() {
//        Log.d(TAG, "onResume");
//        super.onResume();
//        registerReceiver(receiver, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
//        wifiManager.startScan();
//    }
//
//    @Override
//    public void onPause() {
//        Log.d(TAG, "onPause");
//        super.onPause();
//        unregisterReceiver(receiver);
//    }
//
//    public BroadcastReceiver receiver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            Log.d(TAG, "scanReceiver received!");
//
//            chartView = ChartFactory.getBarChartView(ChartActivity.this,
//                    getDataset(), renderer, BarChart.Type.STACKED);
//
//            if(chartView != null){
//                chartView.repaint();
//                chartLayout.removeAllViews();
//                chartLayout.addView(chartView);
//            }
//
//            Handler handler = new Handler();
//            handler.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    wifiManager.startScan();
//                }
//            }, 3000);
//        }
//    };
//
//    private XYMultipleSeriesDataset getDataset() {
//
//        List<ScanResult> results = wifiManager.getScanResults();
//        Collections.sort(results, new Comparator<ScanResult>() {
//            @Override
//            public int compare(ScanResult lhs, ScanResult rhs) {
//                return (lhs.level > rhs.level ? -1
//                        : (lhs.level == rhs.level ? 0 : 1));
//            }
//        });
//
//        renderer = getRenderer();
//        XYSeries series = new XYSeries("Wifi Signal");
//        XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
//
//        for (int i = 0;i < results.size();i++) {
//            series.add(i + 1, results.get(i).level + 100);
//            renderer.addXTextLabel(i + 1, results.get(i).SSID);
//        }
//
//        dataset.addSeries(series);
//
//        return dataset;
//    }
//
//    private XYMultipleSeriesRenderer getRenderer() {
//        XYMultipleSeriesRenderer mRenderer = new XYMultipleSeriesRenderer();
//
//        mRenderer.setChartTitle("WiFi Signal");
//        mRenderer.setYTitle("Signal Strength(%)");
//        mRenderer.setBackgroundColor(Color.WHITE);
//        mRenderer.setApplyBackgroundColor(true);
//        mRenderer.setAxesColor(Color.GRAY);
//        mRenderer.setLabelsColor(Color.GRAY);
//        mRenderer.setXLabelsColor(Color.GRAY);
//        mRenderer.setYLabelsColor(0, Color.GRAY);
//        mRenderer.setMarginsColor(Color.WHITE);
//        mRenderer.setAxisTitleTextSize(20);
//        mRenderer.setChartTitleTextSize(20);
//        mRenderer.setLabelsTextSize(22);
//        mRenderer.setXLabelsAngle(90);
//        mRenderer.setXLabelsAlign(Paint.Align.LEFT);
//        mRenderer.setMargins(new int[]{50, 50, 150, 20});
//        mRenderer.setXAxisMin(0);
//        mRenderer.setYAxisMax(100);
//        mRenderer.setYAxisMin(0);
//        mRenderer.setXLabels(0);
//        mRenderer.setYLabels(10);
//        mRenderer.setShowAxes(true);
//        mRenderer.setShowLegend(false);
//        mRenderer.setShowGridX(true);
//        mRenderer.setShowGridY(false);
//        mRenderer.setClickEnabled(false);
//        mRenderer.setExternalZoomEnabled(false);
//        mRenderer.setPanLimits(new double[]{0, 100, 0, 100});
//        mRenderer.setZoomEnabled(true, false);
//        mRenderer.setBarSpacing(0.200000001D);
//
//        XYSeriesRenderer r = new XYSeriesRenderer();
//        r.setDisplayChartValues(true);
//        r.setChartValuesTextSize(20);
//        r.setColor(getResources().getColor(com.jao.freewifi.wifimanager.R.color.blue));
//
//        mRenderer.addSeriesRenderer(r);
//
//        return mRenderer;
//    }
//}
//
