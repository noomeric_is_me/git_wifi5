/*
package com.bestvmkoomfreewifi;

import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TabHost;
import android.widget.TextView;

import com.jao.freewifi.wifimanager.BuildConfig;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.CrashlyticsCore;
import com.jao.freewifi.wifimanager.R;

import io.fabric.sdk.android.Fabric;
import utils.AppUtils;


public class MainActivity extends TabActivity implements TabHost.OnTabChangeListener {
    TabHost tabHost;
    private static String mTagListWifi = "Hotspot (Wi-Fi)";
    private static String mTagChartsWifi = "Graphs & Charts";

    */
/**
     * Called when the activity is first created.
     *//*

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Fabric.with(this, new Crashlytics());

        // Set up Crashlytics, disabled for debug builds
        Crashlytics crashlyticsKit = new Crashlytics.Builder()
                .core(new CrashlyticsCore.Builder().disabled(BuildConfig.DEBUG).build())
                .build();

        // Initialize Fabric with the debug-disabled crashlytics.
        Fabric.with(this, crashlyticsKit);


        setContentView(R.layout.activity_main);

        // create the TabHost that will contain the Tabs
        tabHost = (TabHost) findViewById(android.R.id.tabhost);

        setupTab(new TextView(this), mTagListWifi);
        setupTab(new TextView(this), mTagChartsWifi);
        tabHost.setOnTabChangedListener(this);
*/
/*
        TabHost.TabSpec tab1 = tabHost.newTabSpec("First Tab");
        TabHost.TabSpec tab2 = tabHost.newTabSpec("Second Tab");

        // Set the Tab name and Activity
        // that will be opened when particular Tab will be selected
        tab1.setIndicator("Wifi Connect");
        tab1.setContent(new Intent(this, WifiConnectActivity.class));

        tab2.setIndicator("Wifi Graph");
        tab2.setContent(new Intent(this, ChartActivity.class));

        // Add the tabs  to the TabHost to display.
        tabHost.addTab(tab1);
        tabHost.addTab(tab2);
*//*


        //Init admob or facebook
        initAds();
    }

    private com.facebook.ads.InterstitialAd mFBInterstitialAd;
    private void initAds(){
        mFBInterstitialAd = AppUtils.getinstnce().FBfullBannerInit(this);
    }

    @Override
    protected void onDestroy() {
        if (mFBInterstitialAd != null) {
            mFBInterstitialAd.destroy();
        }
        super.onDestroy();
    }

    private void setupTab(final View view, final String tag) {
        View tabview = createTabView(tabHost.getContext(), tag);
        TabHost.TabSpec setContent = null;

        switch (tag) {
            case "Hotspot (Wi-Fi)":
//                setContent = tabHost.newTabSpec(tag).setIndicator(tabview).setContent(new Intent(this, WifiConnectActivity.class));
                break;
            case "Graphs & Charts":
//                setContent = tabHost.newTabSpec(tag).setIndicator(tabview).setContent(new Intent(this, ChartActivity.class));
                break;
        }
        tabHost.addTab(setContent);
    }

    @Override
    public void onTabChanged(String tabId) {
        */
/* Your code to handle tab changes *//*

        switch (tabId) {
            case "Hotspot (Wi-Fi)":
                AppUtils.getInstance().showFBAdsFullBanner(new AppUtils.BaseAdListener() {
                    @Override
                    public void onAdClosed() {

                    }
                });
                break;
            case "Graphs & Charts":
                AppUtils.getInstance().showFBAdsFullBanner(new AppUtils.BaseAdListener() {
                    @Override
                    public void onAdClosed() {

                    }
                });
                break;
        }
    }

    private static View createTabView(final Context context, final String text) {
        View view = LayoutInflater.from(context).inflate(R.layout.tabs_bg, null);
        TextView tv = (TextView) view.findViewById(R.id.tabsText);
        tv.setText(text);
        return view;
    }
}*/
