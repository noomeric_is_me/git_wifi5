//package com.bestvmkoomfreewifi;
//
//import android.app.Activity;
//import android.content.ActivityNotFoundException;
//import android.content.BroadcastReceiver;
//import android.content.Context;
//import android.content.Intent;
//import android.content.IntentFilter;
//import android.net.NetworkInfo;
//import android.net.wifi.ScanResult;
//import android.net.wifi.WifiInfo;
//import android.net.wifi.WifiManager;
//import android.os.Bundle;
//import android.os.Handler;
//import android.support.design.widget.Snackbar;
//import android.text.Editable;
//import android.text.TextWatcher;
//import android.util.Log;
//import android.view.View;
//import android.widget.AdapterView;
//import android.widget.AutoCompleteTextView;
//import android.widget.Button;
//import android.widget.ImageView;
//import android.widget.ListView;
//import android.widget.RelativeLayout;
//import android.widget.Toast;
//import android.widget.ToggleButton;
//
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.Comparator;
//import java.util.List;
//
//import utils.AppUtils;
//
//public class WifiConnectActivity extends Activity implements AdapterView.OnItemClickListener, View.OnClickListener {
//    public static final String TAG = "debugging";
//
//    public static int WIFI_STATE_CONNECT;
//
//    private ToggleButton toggleWifi;
//    private ImageView btnSort, stateView;
//
//    private WifiManager wifiManager;
//    private WifiInfo wifiInfo;
//    private ListWifiAdapter listWifiAdapter;
//    private List<ScanResult> listWifiData = new ArrayList<>();
//    private List<ScanResult> listSearch = new ArrayList<>();
//    private int sort = 1;
//    private Handler guiThread;
//    private Runnable updateTask;
//    private AutoCompleteTextView txt_search;
//    private ListView listWifi;
//
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(com.jao.freewifi.wifimanager.R.layout.activity_wifi);
//
//        listWifi = (ListView) findViewById(com.jao.freewifi.wifimanager.R.id.list_wifi);
//        toggleWifi = (ToggleButton) findViewById(com.jao.freewifi.wifimanager.R.id.wifi_toggle_btn);
//        stateView = (ImageView) findViewById(com.jao.freewifi.wifimanager.R.id.state_view);
//
//        Button btnRefresh = (Button) findViewById(com.jao.freewifi.wifimanager.R.id.button_wifi_refresh);
//        btnSort = (ImageView) findViewById(com.jao.freewifi.wifimanager.R.id.sort_btn);
//
//        listWifiAdapter = new ListWifiAdapter(this);
//        wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
//        listWifi.setAdapter(listWifiAdapter);
//        listWifi.setOnItemClickListener(this);
//        setSwitchWifi();
//        setSwitchSort();
//        toggleWifi.setOnClickListener(this);
//        btnRefresh.setOnClickListener(WifiConnectActivity.this);
//        btnSort.setOnClickListener(WifiConnectActivity.this);
//
//        innitThread();
//        txt_search = (AutoCompleteTextView) findViewById(com.jao.freewifi.wifimanager.R.id.search_box);
//        txt_search.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                queueUpdate(500);
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//            }
//        });
//
//        //Init admob or facebook
//        initAds();
//    }
//
//    private com.facebook.ads.AdView adViewfacebook;
//    private void initAds(){
//        //Show Facebook Ads
//        RelativeLayout adViewContainer = (RelativeLayout) findViewById(com.jao.freewifi.wifimanager.R.id.adViewContainer);
//        adViewfacebook = AppUtils.getinstnce().showFBAdsBanner(this,adViewContainer);
//    }
//
//    @Override
//    protected void onDestroy() {
//        if(adViewfacebook != null){
//            adViewfacebook.destroy();
//        }
//        super.onDestroy();
//    }
//
//    private void queueUpdate(long delayMillisecond) {
//        guiThread.removeCallbacks(updateTask);
//        // update data if no change in textSearch after time config
//        // timer by = milliseconds
//        guiThread.postDelayed(updateTask, delayMillisecond);
//    }
//
//    private void innitThread() {
//        guiThread = new Handler();
//        updateTask = new Runnable() {
//            @Override
//            public void run() {
//
//                String word = txt_search.getText().toString().trim();
//                if (word.isEmpty()) {
//                    // if not change set listView first
//                    listWifiAdapter.add(listWifiData);
//                    listWifiAdapter.notifyDataSetChanged();
//                } else {
//                    // get data from webservice
//                    getDataByKeywords(word);
//                    // Show on list
//                    listSearch = null;
//                    // get data from webservice
//                    listSearch = getDataByKeywords(word);
//
//                    listWifiAdapter.add(listSearch);
//                    listWifiAdapter.notifyDataSetChanged();
//                }
//
//            }
//        };
//    }
//
//    public List<ScanResult> getDataByKeywords(String keyword) {
//        ArrayList<ScanResult> listFilter = new ArrayList<ScanResult>();
//        keyword = keyword.toUpperCase();
//        for (int i = 0; i < listWifiData.size(); i++) {
//            String contain = listWifiData.get(i).SSID.toUpperCase();
//            if (contain.contains(keyword)) {
//                if (listFilter != null) {
//                    listFilter.add(listWifiData.get(i));
//                }
//            }
//        }
//        return listFilter;
//    }
//
//    private void setSwitchWifi() {
//        if (wifiManager.isWifiEnabled()) {
//            toggleWifi.setChecked(true);
//            stateView.setImageResource(com.jao.freewifi.wifimanager.R.drawable.circle_on);
//
//        } else {
//            toggleWifi.setChecked(false);
//            stateView.setImageResource(com.jao.freewifi.wifimanager.R.drawable.circle_off);
//        }
//    }
//
//    private void setSwitchSort() {
//        switch (sort) {
//            case 1:
//                btnSort.setImageResource(com.jao.freewifi.wifimanager.R.drawable.sort_signal);
//                break;
//            case 2:
//                btnSort.setImageResource(com.jao.freewifi.wifimanager.R.drawable.sort_alpha);
//                break;
//        }
//    }
//
//    @Override
//    public void onResume() {
//        Log.d(TAG, "onResume");
//        super.onResume();
//        registerReceiver(receiver, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
//        registerReceiver(receiver, new IntentFilter(WifiManager.NETWORK_STATE_CHANGED_ACTION));
//        registerReceiver(receiver, new IntentFilter(WifiManager.WIFI_STATE_CHANGED_ACTION));
//        wifiManager.startScan();
//    }
//
//    @Override
//    public void onPause() {
//        Log.d(TAG, "onPause");
//        super.onPause();
//        unregisterReceiver(receiver);
//    }
//
//    @Override
//    public void onClick(View v) {
//        int id = v.getId();
//        switch (id) {
//            case com.jao.freewifi.wifimanager.R.id.wifi_toggle_btn:
//                if (toggleWifi.isChecked()) {
//                    if (!wifiManager.isWifiEnabled()) {
//                        wifiManager.setWifiEnabled(true);
//                    }
//                } else {
//                    if (wifiManager.isWifiEnabled()) {
//                        wifiManager.setWifiEnabled(false);
//                        listWifiData.clear();
//                        listWifiAdapter.notifyDataSetChanged();
//                    }
//                }
//                break;
//            case com.jao.freewifi.wifimanager.R.id.button_wifi_refresh:
//                AppUtils.getinstnce().showFBAdsFullBanner(new AppUtils.BaseAdListener() {
//                    @Override
//                    public void onAdClosed() {
//                        if(listWifiData != null
//                                && listWifiAdapter != null
//                                && wifiManager != null){
//
//                            listWifiData.clear();
//                            listWifiAdapter.notifyDataSetChanged();
//                            wifiManager.startScan();
//                        }
//                    }
//                });
//
////                if (intAd.isLoaded()) {
////                    intAd.show();
////                } else {
////                    listWifiData.clear();
////                    listWifiAdapter.notifyDataSetChanged();
////                    wifiManager.startScan();
////                }
////                intAd.setAdListener(new AdListener() {
////                    @Override
////                    public void onAdClosed() {
////                        openAd();
////                        listWifiData.clear();
////                        listWifiAdapter.notifyDataSetChanged();
////                        wifiManager.startScan();
////                    }
////                });
//
//                break;
//            case com.jao.freewifi.wifimanager.R.id.sort_btn:
//                switch (sort) {
//                    case 1:
//                        if(listWifiData != null
//                                && listWifiAdapter != null
//                                && wifiManager != null){
//
//                            listWifiData.clear();
//                            listWifiAdapter.notifyDataSetChanged();
//                            wifiManager.startScan();
//                            sort++;
//                        }
//                        break;
//                    default:
//                        if(listWifiData != null
//                                && listWifiAdapter != null
//                                && wifiManager != null){
//
//                            listWifiData.clear();
//                            listWifiAdapter.notifyDataSetChanged();
//                            wifiManager.startScan();
//                            sort = 1;
//                        }
//                        break;
//                }
//                setSwitchSort();
//                break;
//        }
//    }
//
//    @Override
//    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//        if(listWifiData == null || position >= listWifiData.size()){
//            return;
//        }
//        final ScanResult result = listWifiData.get(position);
//        launchWifiConnecter(WifiConnectActivity.this, result);
//    }
//
//    private BroadcastReceiver receiver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            String action = intent.getAction();
//            if (WifiManager.SCAN_RESULTS_AVAILABLE_ACTION.equals(action)) {
//                listWifiData = wifiManager.getScanResults();
//
//                if (sort == 1) {
//                /* sorting of wifi provider based on level */
//                    Collections.sort(listWifiData, new Comparator<ScanResult>() {
//                        @Override
//                        public int compare(ScanResult lhs, ScanResult rhs) {
//                            return (lhs.level > rhs.level ? -1
//                                    : (lhs.level == rhs.level ? 0 : 1));
//                        }
//                    });
//                } else if (sort == 2) {
//                /* sorting of wifi provider based on name */
//                    Collections.sort(listWifiData, new Comparator<ScanResult>() {
//                        @Override
//                        public int compare(ScanResult lhs, ScanResult rhs) {
//                            int res = String.CASE_INSENSITIVE_ORDER.compare(lhs.SSID, rhs.SSID);
//                            if (res == 0) {
//                                res = lhs.SSID.compareTo(rhs.SSID);
//                            }
//                            return res;
//                        }
//                    });
//                }
//
//                wifiInfo = wifiManager.getConnectionInfo();
//                if (wifiInfo.getBSSID() != null) {
//                    for (int i = 0; i < listWifiData.size(); i++) {
//                        if (wifiInfo.getBSSID().equals(listWifiData.get(i).BSSID)) {
//                            ScanResult itemToMove = listWifiData.get(i);
//                            listWifiData.remove(i);
//                            listWifiData.add(0, itemToMove);
//                        }
//                    }
//                }
//
//                guiThread.post(updateTask);
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        wifiManager.startScan();
//                    }
//                }, 5000);
//
//            }
//
//            if (WifiManager.WIFI_STATE_CHANGED_ACTION.equals(action)) {
//                int state = intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE, -1);
//                switch (state) {
//                    case WifiManager.WIFI_STATE_DISABLED:
//                        setSwitchWifi();
//                        break;
//                    case WifiManager.WIFI_STATE_ENABLED:
//                        setSwitchWifi();
//                        wifiManager.startScan();
//                        break;
//                }
//            }
//
//            if (WifiManager.NETWORK_STATE_CHANGED_ACTION.equals(action)) {
//
//                NetworkInfo nwInfo = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
//                if (NetworkInfo.State.CONNECTING.equals(nwInfo.getState())) {
//                    Log.d(TAG, "Wifi Connecting");
//                    WIFI_STATE_CONNECT = 1;
//                } else if (NetworkInfo.State.CONNECTED.equals(nwInfo.getState())) {
//                    Log.d(TAG, "Wifi Connected");
//                    WIFI_STATE_CONNECT = 2;
//                }
//                listWifiAdapter.notifyDataSetChanged();
//            }
//        }
//    };
//
//    private void launchWifiConnecter(final Context activity, final ScanResult hotspot) {
//        final Intent intent = new Intent("com.jao.freewifi.wifimanager.action.CONNECT_OR_EDIT");
//        intent.putExtra("com.jao.freewifi.wifimanager.extra.HOTSPOT", hotspot);
//        try {
//            activity.startActivity(intent);
//        } catch (ActivityNotFoundException e) {
//            // Wifi Connecter Library is not installed.
//
//            if(listWifi != null) {
//                Snackbar.make(listWifi, "There was an error please try again !", Snackbar.LENGTH_SHORT).show();
//            }else{
//                Toast.makeText(getApplicationContext(), "There was an error please try again !"
//                        , Toast.LENGTH_LONG).show();
//
////                Toast.makeText(getApplicationContext(), "Wifi Connecter is not installed."
////                    , Toast.LENGTH_LONG).show();
//            }
//        }
//    }
//}
//
