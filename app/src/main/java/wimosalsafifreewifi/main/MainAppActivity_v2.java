package wimosalsafifreewifi.main;

import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.location.LocationManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.codemybrainsout.ratingdialog.RatingDialog;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.crashlytics.android.answers.CustomEvent;
import com.crashlytics.android.core.CrashlyticsCore;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.applinks.AppLinkData;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.jao.freewifi.wifimanager.BuildConfig;
import com.jao.freewifi.wifimanager.R;
import com.mady.wifi.api.WifiStatus;
import com.mady.wifi.api.wifiHotSpots;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.rampo.updatechecker.UpdateChecker;
import com.skyfishjy.library.RippleBackground;
import com.thefinestartist.finestwebview.FinestWebView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import co.mobiwise.materialintro.shape.Focus;
import co.mobiwise.materialintro.shape.FocusGravity;
import co.mobiwise.materialintro.shape.ShapeType;
import co.mobiwise.materialintro.view.MaterialIntroView;
import devlight.io.library.ntb.NavigationTabBar;
import wimosalsafifreewifi.activity.ChartActivity;
import wimosalsafifreewifi.activity.SpeedTestMiniActivity;
import wimosalsafifreewifi.activity.WifiConnectActivity;
import wimosalsafifreewifi.application.AppController;
import wimosalsafifreewifi.fragment.ChartFragment;
import wimosalsafifreewifi.fragment.HotspotFragment;
import wimosalsafifreewifi.fragment.SpeedFragment_v1;
import wimosalsafifreewifi.fragment.WifiConnectFragment_Recycler_v1;
import wimosalsafifreewifi.fragment.WifiConnectFragment_v1;
import wimosalsafifreewifi.services.GsonRequest;
import wimosalsafifreewifi.services.ServiceAPI;
import wimosalsafifreewifi.services.model.AppConfig;
import wimosalsafifreewifi.services.model.External_IP;
import wimosalsafifreewifi.services.model.NetworkInformation;
import io.fabric.sdk.android.Fabric;
import wimosalsafimainapp.scrollable.MainAppHeaderView;
import wimosalsafimainapp.scrollable.fragment.FragmentPagerAdapter;
import wimosalsafimainapp.scrollable.fragment.FragmentPagerFragment;
import ru.noties.ccf.CCFAnimator;
import ru.noties.scrollable.CanScrollVerticallyDelegate;
import ru.noties.scrollable.OnFlingOverListener;
import ru.noties.scrollable.OnScrollChangedListener;
import ru.noties.scrollable.ScrollableLayout;
import wimosalsafispeedtest.activity.OptimizeActivity;
import wimosalsafispeedtest.activity.SpeedTestHistoryActivity;
import wimosalsafispeedtest.fragment.MainFreeWiFiFragment;
import wimosalsafispeedtest.fragment.SpeedDetailFragment;
import utils.AppUtils;
import wimosalsafispeedtest.fragment.SpeedTestHistoryFragment;
import wimosalsafispeedtest.fragment.SpeedTestMiniFragment;
import wimosalsafiwifimap.activity.MapActivity;
import wimosalsafiwifimap.adapter.ScanResultAdapter;
import wimosalsafiwifimap.fragment.SpeedMapFragment;
import wimosalsafiwifimap.model.service.FusedLocationService;
import wimosalsafiwifimap.ui.presenter.DataSetHandler;

import com.facebook.appevents.AppEventsLogger;

import javax.inject.Inject;

/**
 * Created by NTL on 8/20/2017 AD.
 */

public class MainAppActivity_v2 extends AppCompatActivity implements View.OnClickListener {

    public static final String TAG = MainAppActivity.class.getSimpleName();
    public static String COLOR_MESSAGE = "COLOR_MESSAGE";
    public static int currentColor;

    public static int WIFI_STATE_CONNECT;
    private static final int NAVDRAWER_LAUNCH_DELAY = 250;
    private DrawerLayout mDrawer;
    private NavigationView mNavigationView;
    private Handler mHandler;
    //    private ImageView stateView;
    private WifiManager wifiManager;
    private ToggleButton toggleWifi;
    private TextView mText_connect_wifi;
    private MainFreeWiFiFragment mMainFreeWiFiFragment;
    //    private SpeedMapFragment mSpeedMapFragment;
//    private AnimatedCircleLoadingView animatedCircleLoadingView;
    private RippleBackground rippleBackground;
    private CallbackManager callbackManager;
    private FirebaseAnalytics mFirebaseAnalytics;
    private SharedPreferences sharedPref;
    private boolean mRequestAdsConfig;
    private wifiHotSpots mHotUtil;
    private WifiStatus mWifiStatus;
    private FloatingActionButton mFabOpenWifiHotspot;
    public static boolean mWifiHotspotEnable;
    private int i = -1;
    private FragmentPagerAdapter adapter;
    private boolean canExitAppWithOutRateApp,isShowWifiHotspotIntro;
    private static int show_rating_dialog_countdown = 1;

    private interface CurrentFragment {
        @Nullable
        FragmentPagerFragment currentFragment();
    }

    @Inject
    DataSetHandler dataSetHandler;

    @Inject
    ScanResultAdapter scanResultAdapter;

    private BroadcastReceiver scanResultAvailableReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context c, Intent intent) {
            dataSetHandler.onWifiListReceive();

        }
    };

    private BroadcastReceiver fusedLocationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            intent.getParcelableExtra(FusedLocationService.LOCATION);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set up Crashlytics, disabled for debug builds
        Crashlytics crashlyticsKit = new Crashlytics.Builder()
                .core(new CrashlyticsCore.Builder().disabled(BuildConfig.DEBUG).build())
                .build();

        // Initialize Fabric with the debug-disabled crashlytics.
        Fabric.with(this, crashlyticsKit);

        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        AppLinkData.fetchDeferredAppLinkData(this,
                new AppLinkData.CompletionHandler() {
                    @Override
                    public void onDeferredAppLinkDataFetched(AppLinkData appLinkData) {
                        // Process app link data
                    }
                }
        );

        callbackManager = CallbackManager.Factory.create();

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        mHandler = new Handler();

        setContentView(R.layout.activity_main_v2);

        setTitle("");

        Toolbar toolbar = findViewById(R.id.toolbar);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle(this.getResources().getString(R.string.app_name));
            toolbar.setTitleTextColor(Color.WHITE);
            toolbar.setSubtitleTextColor(Color.WHITE);
//            toolbar.setLogo(R.drawable.logo_white);
            toolbar.setContentInsetStartWithNavigation(0);
        }

        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setHomeAsUpIndicator(R.drawable.ic_menu);
            ab.setDisplayHomeAsUpEnabled(true);
        }

        mMainFreeWiFiFragment = MainFreeWiFiFragment.newInstance();
        mMainFreeWiFiFragment.setMainListener(new MainFreeWiFiFragment.MainListener() {
            @Override
            public void clickHotspot() {
                openFreeWiFiHotspot();
            }
        });

        setContentView(R.layout.activity_horizontal_ntb);

        final ViewPager pager = (ViewPager) findViewById(R.id.vp_horizontal_ntb);
        pager.setAdapter(new android.support.v4.app.FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public int getCount() {
                return 5;
            }

            // Returns the fragment to display for that page
            @Override
            public Fragment getItem(int position) {
                final Intent intent;
                switch (position) {
                    case 0: // Fragment # 0 - This will show FirstFragment

                        return new MainFreeWiFiFragment();
                    case 1: // Fragment # 1 - This will show SecondFragment

                        return new HotspotFragment();
                    case 2: // Fragment # 2 - This will show ThirdFragment

                        return new SpeedTestMiniFragment();
                    case 3: // Fragment # 3 - This will show FourthFragment

                        return new SpeedDetailFragment();
                    case 4: // Fragment # 4 - This will show FifthFragment
//                        intent = new Intent(MainAppActivity_v2.this, OptimizeActivity.class);
//                        startActivity(intent);
                        return new SpeedMapFragment();
                    default:
                        return new MainFreeWiFiFragment();
                }
            }

            // Returns the page title for the top indicator
            @Override
            public CharSequence getPageTitle(int position) {
                return "Page" + position;
            }


        });

        final String[] colors = getResources().getStringArray(R.array.default_preview);

        final NavigationTabBar navigationTabBar = (NavigationTabBar) findViewById(R.id.ntb_horizontal);
        final ArrayList<NavigationTabBar.Model> models = new ArrayList<>();
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_first),
                        Color.parseColor(colors[0]))
//                        .selectedIcon(getResources().getDrawable(R.drawable.ic_sixth))
                        .title("Home")
                        .badgeTitle("NTB")
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_second),
                        Color.parseColor(colors[1]))
                        .selectedIcon(getResources().getDrawable(R.drawable.ic_second2))
                        .title("Hotspot")
                        .badgeTitle("with")
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_third),
                        Color.parseColor(colors[2]))
//                        .selectedIcon(getResources().getDrawable(R.drawable.ic_third))
                        .title("Speedtest")
                        .badgeTitle("state")
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_fourth),
                        Color.parseColor(colors[3]))
                        .selectedIcon(getResources().getDrawable(R.drawable.ic_fourth2))
                        .title("Info")
                        .badgeTitle("icon")
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_fifth),
                        Color.parseColor(colors[4]))
                        .selectedIcon(getResources().getDrawable(R.drawable.ic_fifth2))
                        .title("Wifimap")
                        .badgeTitle("777")
                        .build()
        );

        navigationTabBar.setModels(models);
        navigationTabBar.setViewPager(pager, 0);
        navigationTabBar.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(final int position, final float positionOffset, final int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(final int position) {
                navigationTabBar.getModels().get(position).hideBadge();
                try {
                    if (position == 0){

                    }else {
                        if (AppUtils.ads_interstitial_show_all) {

                            if (AppUtils.adsNetworkType != AppUtils.NONE_TYPE) {
                                AppUtils.getInstance().showAdsFullBanner(null);
                            }

                        } else {

                            if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {

                                AppUtils.getInstance().showAdmobAdsFullBanner(null);

                            } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {

                                AppUtils.getInstance().showFBAdsFullBanner(null);

                            }
                        }
                    }
                } catch (Exception ignored) {
                    Crashlytics.logException(ignored);
                }
            }

            @Override
            public void onPageScrollStateChanged(final int state) {

            }
        });

        navigationTabBar.postDelayed(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < navigationTabBar.getModels().size(); i++) {
                    final NavigationTabBar.Model model = navigationTabBar.getModels().get(i);
                    navigationTabBar.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            model.showBadge();
                        }
                    }, i * 100);
                }
            }
        }, 500);

//        scrollableLayout.setDraggableView(tabsLayout);
//
//        final MainAppActivity_v2.CurrentFragment currentFragment = new MainAppActivity_v2.CurrentFragmentImpl(viewPager, getSupportFragmentManager());
//
//        scrollableLayout.setCanScrollVerticallyDelegate(new CanScrollVerticallyDelegate() {
//            @Override
//            public boolean canScrollVertically(int direction) {
//                final FragmentPagerFragment fragment = currentFragment.currentFragment();
//                return fragment != null && fragment.canScrollVertically(direction);
//            }
//        });
//
//        scrollableLayout.setOnFlingOverListener(new OnFlingOverListener() {
//            @Override
//            public void onFlingOver(int y, long duration) {
//                final FragmentPagerFragment fragment = currentFragment.currentFragment();
//                if (fragment != null) {
//                    fragment.onFlingOver(y, duration);
//                }
//            }
//        });
//
//        scrollableLayout.addOnScrollChangedListener(new OnScrollChangedListener() {
//
//            final CCFAnimator mAnimator = CCFAnimator.rgb(header.getExpandedColor(), header.getCollapsedColor());
//
//            @Override
//            public void onScrollChanged(int y, int oldY, int maxY) {
//
////                Debug.i("y: %s, oldY: %s, maxY: %s", y, oldY, maxY);
//
//                final float tabsTranslationY;
//                if (y < maxY) {
//                    tabsTranslationY = .0F;
//                } else {
//                    tabsTranslationY = y - maxY;
//                }
//
//                tabsLayout.setTranslationY(tabsTranslationY);
//
//                // parallax effect for collapse/expand
//                final float ratio = (float) y / maxY;
//                header.setBackgroundColor(mAnimator.getColor(ratio));
////                header.getTextView().setAlpha(1.F - ratio);
////                header.getTextView().setTranslationY(y / 2);
//
//                header.getRootView().setAlpha(1.F - ratio);
//                header.getRootView().setTranslationY(y / 2);
//            }
//        });


        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mNavigationView = (NavigationView) findViewById(R.id.nav_view);
        if (mNavigationView != null) {
            setupDrawerContent(mNavigationView);
        }

//        stateView = (ImageView) findViewById(R.id.state_view);
//        toggleWifi = (ToggleButton) findViewById(R.id.wifi_toggle_btn);
//        wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
//        mText_connect_wifi = (TextView) findViewById(R.id.text_connect_wifi);
//        mText_connect_wifi.setGravity(Gravity.CENTER);
//        ImageView image_circle_outside_3 = (ImageView) findViewById(R.id.image_circle_outside_3);
//        Button btn_speed_test = (Button) findViewById(R.id.btn_speed_test);

//        toggleWifi.setOnClickListener(this);
//        mText_connect_wifi.setOnClickListener(this);
//        image_circle_outside_3.setOnClickListener(this);
//        btn_speed_test.setOnClickListener(this);


//        if (!wifiManager.isWifiEnabled()) {
//            wifiManager.setWifiEnabled(true);
//            mText_connect_wifi.setText("Free WiFi Scanning..");
//        }
//
//        setSwitchWifi();


        rippleBackground = (RippleBackground)findViewById(R.id.content);

        //Read last config
        sharedPref = this.getPreferences(Context.MODE_PRIVATE);
//        AppUtils.adsNetworkType = sharedPref.getInt(AppUtils.ADS_TYPE_KEY, AppUtils.FB_ADS_TYPE);
        AppUtils.adsNetworkType = sharedPref.getInt(AppUtils.ADS_TYPE_KEY, AppUtils.ADMOB_ADS_TYPE);//Edit Default Ads Admob
//        AppUtils.adsNetworkType = AppUtils.FB_ADS_TYPE;
//        AppUtils.adsNetworkType = AppUtils.ADMOB_ADS_TYPE;
//        AppUtils.adsNetworkType = AppUtils.NONE_TYPE;

        canExitAppWithOutRateApp = sharedPref.getBoolean(AppUtils.RATING_APP_TYPE_KEY, false);

        if(!canExitAppWithOutRateApp){
            int show_rate_dialog_count = sharedPref.getInt(AppUtils.RATING_APP_COUNT_TYPE_KEY,show_rating_dialog_countdown);

            if(show_rate_dialog_count > 0) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putInt(AppUtils.RATING_APP_COUNT_TYPE_KEY, show_rate_dialog_count - 1);
                editor.apply();

                canExitAppWithOutRateApp = true; //Force can exit app without rating
            }else{
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putInt(AppUtils.RATING_APP_COUNT_TYPE_KEY, show_rating_dialog_countdown);
                editor.apply();
            }
        }

        //Show Wifi Hotspot Intro
        isShowWifiHotspotIntro = sharedPref.getBoolean(AppUtils.SHOW_INTRO_WIFI_HOTSPOT_TYPE_KEY, false);

        //if first time run then show user guide
        if (isFirstTime()) {
            loadTutorial();
        }

        try {

            String package_name = MainAppActivity_v2.this.getPackageName();
            Answers.getInstance().logCustom(new CustomEvent("PackageName")
                    .putCustomAttribute("PackageName", package_name));

            Bundle params = new Bundle();
            params.putString("package_name", package_name);
            mFirebaseAnalytics.logEvent("PackageName", params);

        } catch (Exception ignored) {
            Crashlytics.logException(ignored);
        }

        //Init admob or facebook
        initAds();

        //Get Network information
        getNetworkInformation();

        //Free Hotspot
        mHotUtil = new wifiHotSpots(getApplicationContext());
        mWifiStatus = new WifiStatus(getApplicationContext());

//        mFabOpenWifiHotspot = (FloatingActionButton) findViewById(R.id.fab_open_wifi_hotspot);
//        mFabOpenWifiHotspot.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                disableWiFiHotspot();
//
//            }
//        });

        //Wifi Map
        initializeInjectors();
        final LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if(lm != null){
            if (!lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                buildAlertMessageNoGps();
            }
        }

        startService(new Intent(this, FusedLocationService.class));
        LocalBroadcastManager.getInstance(this).registerReceiver(fusedLocationReceiver,
                new IntentFilter(FusedLocationService.INTENT_LOCATION_CHANGED));

        if (dataSetHandler.checkToInitialize()) {
            Toast.makeText(this, R.string.notify_enable_scan, Toast.LENGTH_SHORT).show();
        }

        registerReceiver(scanResultAvailableReceiver,
                new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
    }

    private void initializeInjectors() {
        ButterKnife.bind(this);
        ((AppController) getApplication()).getComponent().inject(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        RequestAdsToDisplay();

        //Check version update
        checkVersionUpdate();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(rippleBackground != null){
            rippleBackground.stopRippleAnimation();
        }
    }

//    private List<FragmentPagerAdapter.Item> items(Context context) {
//        final Resources r = context.getResources();
//        final List<FragmentPagerAdapter.Item> items = new ArrayList<>(4);
//
//        items.add(new FragmentPagerAdapter.Item("Free-WiFi",
//                new FragmentPagerAdapter.Provider() {
//                    @Override
//                    public Fragment provide() {
////                        if(mMainFreeWiFiFragment != null){
////                            return mMainFreeWiFiFragment;
////                        }
////                        mMainFreeWiFiFragment = new MainFreeWiFiFragment();
//                        return mMainFreeWiFiFragment;
////                        return new MainFreeWiFiFragment();
//                    }
//                }
//        ));
//
//
//        items.add(new FragmentPagerAdapter.Item("WiFi-Info",
//                new FragmentPagerAdapter.Provider() {
//                    @Override
//                    public Fragment provide() {
//                        return new SpeedDetailFragment();
//                    }
//                }
//        ));
//
//        items.add(new FragmentPagerAdapter.Item("WiFi-Map",
//                new FragmentPagerAdapter.Provider() {
//                    @Override
//                    public Fragment provide() {
//                        return new SpeedMapFragment();
//                    }
//                }
//        ));
//
//
//        return items;
//    }

    private static class CurrentFragmentImpl implements MainAppActivity_v2.CurrentFragment {

        private final ViewPager mViewPager;
        private final FragmentManager mFragmentManager;
        private final FragmentPagerAdapter mAdapter;

        CurrentFragmentImpl(ViewPager pager, FragmentManager manager) {
            mViewPager = pager;
            mFragmentManager = manager;
            mAdapter = (FragmentPagerAdapter) pager.getAdapter();
        }

        @Override
        @Nullable
        public FragmentPagerFragment currentFragment() {
            final FragmentPagerFragment out;
            final int position = mViewPager.getCurrentItem();
            if (position < 0
                    || position >= mAdapter.getCount()) {
                out = null;
            } else {
                final String tag = makeFragmentName(mViewPager.getId(), mAdapter.getItemId(position));
                final Fragment fragment = mFragmentManager.findFragmentByTag(tag);
                if (fragment != null) {
                    out = (FragmentPagerFragment) fragment;
                } else {
                    // fragment is still not attached
                    out = null;
                }
            }
            return out;
        }

        // this is really a bad thing from Google. One cannot possible obtain normally
        // an instance of a fragment that is attached. Bad, really bad
        private static String makeFragmentName(int viewId, long id) {
            return "android:switcher:" + viewId + ":" + id;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawer.openDrawer(GravityCompat.START);
                return true;

            case R.id.action_wifi_gift:

                try {
                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
//                            if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
//                                AppUtils.getInstance().showAdmobAdsFullBanner(null);
//                            } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
//                                AppUtils.getInstance().showFBAdsFullBanner(null);
//                            }

                        }
                    }, 200);
                } catch (Exception ignored) {
                    Crashlytics.logException(ignored);
                }

                return true;
        }
        return super.onOptionsItemSelected(item);

    }

    private com.facebook.ads.InterstitialAd mFBInterstitialAd;
    private void initAds(){
//        AdSettings.addTestDevice("2ad1152013a23ee9cd88586b990e02bf");


        if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {

            //Interstitial
            AppUtils.getInstance().fullAdmobAdsBannerInit(this);


        } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {

            //Interstitial
            mFBInterstitialAd = AppUtils.getInstance().FBfullBannerInit(this);

        }

    }

    @Override
    protected void onDestroy() {
        if (mFBInterstitialAd != null) {
            mFBInterstitialAd.destroy();
        }
        super.onDestroy();

        mWifiHotspotEnable = false;

        getBaseContext().unregisterReceiver(scanResultAvailableReceiver);
        stopService(new Intent(this, FusedLocationService.class));
        LocalBroadcastManager.getInstance(this).unregisterReceiver(fusedLocationReceiver);
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            registerReceiver(receiver, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
            registerReceiver(receiver, new IntentFilter(WifiManager.NETWORK_STATE_CHANGED_ACTION));
            registerReceiver(receiver, new IntentFilter(WifiManager.WIFI_STATE_CHANGED_ACTION));
            wifiManager.startScan();
        } catch (Exception ignored) {
            Crashlytics.logException(ignored);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            unregisterReceiver(receiver);
        } catch (Exception ignored) {
            Crashlytics.logException(ignored);
        }
    }

    @Override
    public void onBackPressed() {
//        if (mDrawer.isDrawerOpen(mNavigationView)) {
//            mDrawer.closeDrawers();
//            return;
//        }

        if(!canExitAppWithOutRateApp) {
            rateApp();
            return;
        }

        exitApp();

//        super.onBackPressed();
    }

    private void rateApp(){

        final RatingDialog ratingDialog = new RatingDialog.Builder(this)
//                .session(3)
                .threshold(3)
//                .titlebarcolor(currentColor)
                .version(2)
//                .titlebarDrawable(R.drawable.gradient_background)
                .title("Enjoy Free WiFi ?" +
                        "\n\nRecommend this app to others by leaving us a review in the Play Store !" +
                        "\n\nPlease give us 5 star" +
                        "\n\nSelect star below to rate now !")
//                .titleTextColor(R.color.black)
                .positiveButtonText("")
//                .negativeButtonText("Never")
//                .positiveButtonTextColor(R.color.accent)
//                .negativeButtonTextColor(R.color.grey_500)
                .formTitle("Send Feedback")
                .formHint("Tell us where we can improve")
                .formSubmitText("Send")
                .formCancelText("Dismiss")
//                .ratingBarColor(R.color.black)
//                .positiveButtonBackgroundColor(R.drawable.button_selector_positive)
//                .negativeButtonBackgroundColor(R.drawable.button_selector_negative)
                .onThresholdCleared(new RatingDialog.Builder.RatingThresholdClearedListener() {
                    @Override
                    public void onThresholdCleared(RatingDialog ratingDialog, float rating, boolean thresholdCleared) {
                        //do something

                        showDialogThankyouAndGoStore();

//                        ratingDialog.openPlaystore(MainAppActivity_v2.this);
                        markRateAppComplete();

                        ratingDialog.dismiss();

                    }
                })
//                .onThresholdFailed(new RatingDialog.Builder.RatingThresholdFailedListener() {
//                    @Override
//                    public void onThresholdFailed(RatingDialog ratingDialog, float rating, boolean thresholdCleared) {
//                        //do something
//                        ratingDialog.dismiss();
//                    }
//                })
                .onRatingChanged(new RatingDialog.Builder.RatingDialogListener() {
                    @Override
                    public void onRatingSelected(float rating, boolean thresholdCleared) {
                        //do something
                        Answers.getInstance().logCustom(new CustomEvent("Rate")
                                .putCustomAttribute("Star Rating", rating + ""));

                    }
                })
                .onRatingBarFormSumbit(new RatingDialog.Builder.RatingDialogFormListener() {
                    @Override
                    public void onFormSubmitted(String feedback) {
                        Answers.getInstance().logCustom(new CustomEvent("Rate")
                                .putCustomAttribute("Feedback", feedback));

                        markRateAppComplete();
                        showDialogThankyou();
                    }
                })
                .onRatingDialogClick(new RatingDialog.Builder.RatingDialogOnClickListener() {
                    @Override
                    public void onPositiveButtonSelected() {

                    }

                    @Override
                    public void onNegativeButtonSelected() {

                    }

                    @Override
                    public void onDismissButtonSelected() {
                        canExitAppWithOutRateApp = true;
                    }
                })
                .build();

        ratingDialog.show();
    }

    private void markRateAppComplete(){
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(AppUtils.RATING_APP_TYPE_KEY, true);
        editor.apply();

        canExitAppWithOutRateApp = true;
    }

    private void showDialogThankyou(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainAppActivity_v2.this);
        alertDialogBuilder.setTitle("Thank you !");
        alertDialogBuilder.setMessage("Thank you for your support and give us a feedback we promise to improve as soon as possible.");
        alertDialogBuilder.setPositiveButton("Dismiss",  new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // on success

                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });

        alertDialogBuilder.show();
    }

    private void showDialogThankyouAndGoStore(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainAppActivity_v2.this);
        alertDialogBuilder.setTitle("Thank you ! " + new String(Character.toChars(0x1F601)));
        alertDialogBuilder.setMessage("Thanks for your feedback! It would be appreciated please take a little bit of time " +
                "to recommend this app to others by leaving us a review and give 5 star in the PlayStore !");
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton("OK",  new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // on success

                Uri marketUri = Uri.parse("market://details?id=" + MainAppActivity_v2.this.getPackageName());

                try {
                    MainAppActivity_v2.this.startActivity(new Intent("android.intent.action.VIEW", marketUri));
                } catch (ActivityNotFoundException var4) {
                    Toast.makeText(MainAppActivity_v2.this, "Couldn't find PlayStore on this device", Toast.LENGTH_SHORT).show();
                }

                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });

        alertDialogBuilder.show();
    }

    private void setSwitchWifi() {
        if (wifiManager.isWifiEnabled()) {
            toggleWifi.setChecked(true);
//            stateView.setImageResource(R.drawable.circle_on);

        } else {
            toggleWifi.setChecked(false);
//            stateView.setImageResource(R.drawable.circle_off);
        }
    }

    private List<ScanResult> listWifiData;
    private WifiInfo wifiInfo;
    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            try {
                String action = intent.getAction();

                if (WifiManager.SCAN_RESULTS_AVAILABLE_ACTION.equals(action)) {
                    listWifiData = wifiManager.getScanResults();

                    /* sorting of wifi provider based on level */
                    if (listWifiData != null) {
                        Collections.sort(listWifiData, new Comparator<ScanResult>() {
                            @Override
                            public int compare(ScanResult lhs, ScanResult rhs) {
                                return (lhs.level > rhs.level ? -1
                                        : (lhs.level == rhs.level ? 0 : 1));
                            }
                        });


                        if (mMainFreeWiFiFragment != null) {
                            mMainFreeWiFiFragment.setWiFiNumberInRange("" + listWifiData.size());
                        }

                    } else {
                        if (mMainFreeWiFiFragment != null) {
                            mMainFreeWiFiFragment.setWiFiNumberInRange("0");
                        }
                    }

                    wifiInfo = wifiManager.getConnectionInfo();
                    if (wifiInfo.getBSSID() != null) {
                        if (listWifiData != null) {
                            for (int i = 0; i < listWifiData.size(); i++) {
                                if (wifiInfo.getBSSID().equals(listWifiData.get(i).BSSID)) {
                                    ScanResult itemToMove = listWifiData.get(i);
                                    listWifiData.remove(i);
                                    listWifiData.add(0, itemToMove);
                                }
                            }
                        }
                    }

                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            wifiManager.startScan();
                        }
                    }, 5000);

                }

                if (WifiManager.WIFI_STATE_CHANGED_ACTION.equals(action)) {
                    int state = intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE, -1);
                    switch (state) {
                        case WifiManager.WIFI_STATE_DISABLED:
                            setSwitchWifi();
                            mText_connect_wifi.setText("WiFi Disable.");
                            if(rippleBackground != null){
                                rippleBackground.stopRippleAnimation();
                            }
                            break;
                        case WifiManager.WIFI_STATE_ENABLED:
                            setSwitchWifi();
                            wifiManager.startScan();
                            mText_connect_wifi.setText("Free WiFi Scanning..");
                            if(rippleBackground != null){
                                ImageView image_circle_outside_1 = (ImageView) findViewById(R.id.image_circle_outside_1);
                                ImageView image_circle_outside_2 = (ImageView) findViewById(R.id.image_circle_outside_2);
                                image_circle_outside_1.setVisibility(View.GONE);
                                image_circle_outside_2.setVisibility(View.GONE);
                                rippleBackground.startRippleAnimation();
                            }
                            break;
                    }
                }

                if (WifiManager.NETWORK_STATE_CHANGED_ACTION.equals(action)) {

                    NetworkInfo nwInfo = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
                    if (NetworkInfo.State.CONNECTING.equals(nwInfo.getState())) {
                        WIFI_STATE_CONNECT = 1;
                        Log.d("onReceive", "Wifi Connecting");
                    } else if (NetworkInfo.State.CONNECTED.equals(nwInfo.getState())) {
                        WIFI_STATE_CONNECT = 2;
                        Log.d("onReceive", "Wifi Connected");
                    } else {
                        WIFI_STATE_CONNECT = -1;
                        Log.d("onReceive", "Wifi Unknown");
                    }
                }


                if (wifiInfo.getBSSID() != null) {
                    for (int i = 0; i < listWifiData.size(); i++) {
                        if (wifiInfo.getBSSID().equals(listWifiData.get(i).BSSID)) {
                            if (WIFI_STATE_CONNECT == 1) {
                                mText_connect_wifi.setText("" + AppController.getInstance().getAppContext().getResources().getString(R.string.text_connecting));
                                break;
                            } else if (WIFI_STATE_CONNECT == 2) {
                                mText_connect_wifi.setText("" + listWifiData.get(i).SSID + "\n" + "Connected !");
                                if(rippleBackground != null){
                                    ImageView image_circle_outside_1 = (ImageView) findViewById(R.id.image_circle_outside_1);
                                    ImageView image_circle_outside_2 = (ImageView) findViewById(R.id.image_circle_outside_2);
                                    image_circle_outside_1.setVisibility(View.VISIBLE);
                                    image_circle_outside_2.setVisibility(View.VISIBLE);
                                    rippleBackground.stopRippleAnimation();
                                }
                                break;
                            } else {
                                mText_connect_wifi.setText("");
                                break;
                            }
                        }
                    }
                }
            } catch (Exception ignored) {
                Crashlytics.logException(ignored);
            }
        }
    };

    @Override
    public void onClick(View view) {
        final Intent intent;
        int id = view.getId();
        switch (id) {
            case R.id.wifi_toggle_btn:
                if (toggleWifi.isChecked()) {
                    if (!wifiManager.isWifiEnabled()) {
                        wifiManager.setWifiEnabled(true);
                        mText_connect_wifi.setText("Free WiFi Scanning..");
                    }
                } else {
                    if (wifiManager.isWifiEnabled()) {
                        wifiManager.setWifiEnabled(false);
                        if(listWifiData != null){
                            listWifiData.clear();
                        }
                        mText_connect_wifi.setText("WiFi Disable.");
                        if(rippleBackground != null){
                            rippleBackground.stopRippleAnimation();
                        }
                    }
                }
                break;
            case R.id.image_circle_outside_3:
                toggleWifi.performClick();
                break;
            case R.id.text_connect_wifi:
                if (!toggleWifi.isChecked()) {
                    toggleWifi.performClick();
                }
                break;
            case R.id.btn_speed_test:
                try {

                    Answers.getInstance().logContentView(new ContentViewEvent()
                            .putContentName("Header - Wifi Speed Test"));

//                    if (AppUtils.ads_interstitial_show_all) {
//
//                        if (AppUtils.adsNetworkType != AppUtils.NONE_TYPE) {
//                            AppUtils.getInstance().showAdsFullBanner(new AppUtils.BaseAdListener() {
//                                @Override
//                                public void onAdClosed() {
//                                    Intent intent = new Intent(MainAppActivity_v2.this, SpeedTestMiniActivity.class);
//                                    startActivity(intent);
//                                }
//                            });
//                        } else {
//                            intent = new Intent(MainAppActivity_v2.this, SpeedTestMiniActivity.class);
//                            startActivity(intent);
//                        }
//
//                    } else {
//
//                        if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
//                            AppUtils.getInstance().showAdmobAdsFullBanner(new AppUtils.BaseAdListener() {
//                                @Override
//                                public void onAdClosed() {
//                                    Intent intent = new Intent(MainAppActivity_v2.this, SpeedTestMiniActivity.class);
//                                    startActivity(intent);
//                                }
//                            });
//                        } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
//                            AppUtils.getInstance().showFBAdsFullBanner(new AppUtils.BaseAdListener() {
//                                @Override
//                                public void onAdClosed() {
//                                    Intent intent = new Intent(MainAppActivity_v2.this, SpeedTestMiniActivity.class);
//                                    startActivity(intent);
//                                }
//                            });
//                        } else {
//                            intent = new Intent(MainAppActivity_v2.this, SpeedTestMiniActivity.class);
//                            startActivity(intent);
//                        }
//                    }

                    intent = new Intent(MainAppActivity_v2.this, SpeedTestMiniActivity.class);
                    startActivity(intent);

                } catch (Exception ignored) {
                    Crashlytics.logException(ignored);
                }
                break;
        }
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(final MenuItem menuItem) {

                        mHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                goToNavDrawerItem(menuItem.getItemId());
                            }
                        }, NAVDRAWER_LAUNCH_DELAY);

//                        menuItem.setChecked(true);
                        mDrawer.closeDrawers();
                        return true;
                    }
                });
    }

    private void goToNavDrawerItem(int menuItem) {
        final Intent intent;
        String setContentUrl = "";
        String setImageUrl = "";

        switch (menuItem) {
            case android.R.id.home:
                mDrawer.openDrawer(GravityCompat.START);
                return;

            case R.id.nav_wifi_find:
                try {

                    Answers.getInstance().logContentView(new ContentViewEvent()
                            .putContentName("NAV - Find More Networks"));

//                    if (AppUtils.ads_interstitial_show_all) {
//
//                        if (AppUtils.adsNetworkType != AppUtils.NONE_TYPE) {
//                            AppUtils.getInstance().showAdsFullBanner(new AppUtils.BaseAdListener() {
//                                @Override
//                                public void onAdClosed() {
//                                    Intent intent = new Intent(MainAppActivity_v2.this, WifiConnectActivity.class);
//                                    startActivity(intent);
//                                }
//                            });
//                        } else {
//                            intent = new Intent(MainAppActivity_v2.this, WifiConnectActivity.class);
//                            startActivity(intent);
//                        }
//
//                    } else {
//
//                        if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
//                            AppUtils.getInstance().showAdmobAdsFullBanner(new AppUtils.BaseAdListener() {
//                                @Override
//                                public void onAdClosed() {
//                                    Intent intent = new Intent(MainAppActivity_v2.this, WifiConnectActivity.class);
//                                    startActivity(intent);
//                                }
//                            });
//                        } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
//                            AppUtils.getInstance().showFBAdsFullBanner(new AppUtils.BaseAdListener() {
//                                @Override
//                                public void onAdClosed() {
//                                    Intent intent = new Intent(MainAppActivity_v2.this, WifiConnectActivity.class);
//                                    startActivity(intent);
//                                }
//                            });
//                        } else {
//                            intent = new Intent(MainAppActivity_v2.this, WifiConnectActivity.class);
//                            startActivity(intent);
//                        }
//                    }

                    intent = new Intent(MainAppActivity_v2.this, WifiConnectActivity.class);
                    startActivity(intent);

                } catch (Exception ignored) {
                    Crashlytics.logException(ignored);
                }
                break;

            case R.id.nav_wifi_analyzer:
                try {

                    Answers.getInstance().logContentView(new ContentViewEvent()
                            .putContentName("NAV - Wifi Analyzer"));

//                    if (AppUtils.ads_interstitial_show_all) {
//
//                        if (AppUtils.adsNetworkType != AppUtils.NONE_TYPE) {
//                            AppUtils.getInstance().showAdsFullBanner(new AppUtils.BaseAdListener() {
//                                @Override
//                                public void onAdClosed() {
//                                    Intent intent = new Intent(MainAppActivity_v2.this, ChartActivity.class);
//                                    startActivity(intent);
//                                }
//                            });
//                        } else {
//                            intent = new Intent(MainAppActivity_v2.this, ChartActivity.class);
//                            startActivity(intent);
//                        }
//
//                    } else {
//
//                        if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
//                            AppUtils.getInstance().showAdmobAdsFullBanner(new AppUtils.BaseAdListener() {
//                                @Override
//                                public void onAdClosed() {
//                                    Intent intent = new Intent(MainAppActivity_v2.this, ChartActivity.class);
//                                    startActivity(intent);
//                                }
//                            });
//                        } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
//                            AppUtils.getInstance().showFBAdsFullBanner(new AppUtils.BaseAdListener() {
//                                @Override
//                                public void onAdClosed() {
//                                    Intent intent = new Intent(MainAppActivity_v2.this, ChartActivity.class);
//                                    startActivity(intent);
//                                }
//                            });
//                        } else {
//                            intent = new Intent(MainAppActivity_v2.this, ChartActivity.class);
//                            startActivity(intent);
//                        }
//                    }

                    intent = new Intent(MainAppActivity_v2.this, ChartActivity.class);
                    startActivity(intent);

                } catch (Exception ignored) {
                    Crashlytics.logException(ignored);
                }
                break;

            case R.id.nav_speed_booster:
                try {

                    Answers.getInstance().logContentView(new ContentViewEvent()
                            .putContentName("NAV - Wifi Speed Booster"));

//                    if (AppUtils.ads_interstitial_show_all) {
//
//                        if (AppUtils.adsNetworkType != AppUtils.NONE_TYPE) {
//                            AppUtils.getInstance().showAdsFullBanner(new AppUtils.BaseAdListener() {
//                                @Override
//                                public void onAdClosed() {
//                                    Intent intent = new Intent(MainAppActivity_v2.this, OptimizeActivity.class);
//                                    intent.putExtra(MainAppActivity_v2.COLOR_MESSAGE, currentColor);
//                                    startActivity(intent);
//                                }
//                            });
//                        } else {
//                            intent = new Intent(MainAppActivity_v2.this, OptimizeActivity.class);
//                            intent.putExtra(MainAppActivity_v2.COLOR_MESSAGE, currentColor);
//                            startActivity(intent);
//                        }
//
//                    } else {
//
//                        if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
//                            AppUtils.getInstance().showAdmobAdsFullBanner(new AppUtils.BaseAdListener() {
//                                @Override
//                                public void onAdClosed() {
//                                    Intent intent = new Intent(MainAppActivity_v2.this, OptimizeActivity.class);
//                                    intent.putExtra(MainAppActivity_v2.COLOR_MESSAGE, currentColor);
//                                    startActivity(intent);
//                                }
//                            });
//                        } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
//                            AppUtils.getInstance().showFBAdsFullBanner(new AppUtils.BaseAdListener() {
//                                @Override
//                                public void onAdClosed() {
//                                    Intent intent = new Intent(MainAppActivity_v2.this, OptimizeActivity.class);
//                                    intent.putExtra(MainAppActivity_v2.COLOR_MESSAGE, currentColor);
//                                    startActivity(intent);
//                                }
//                            });
//                        } else {
//                            intent = new Intent(MainAppActivity_v2.this, OptimizeActivity.class);
//                            intent.putExtra(MainAppActivity_v2.COLOR_MESSAGE, currentColor);
//                            startActivity(intent);
//                        }
//                    }

                    intent = new Intent(MainAppActivity_v2.this, OptimizeActivity.class);
                    intent.putExtra(MainAppActivity_v2.COLOR_MESSAGE, currentColor);
                    startActivity(intent);

                } catch (Exception ignored) {
                    Crashlytics.logException(ignored);
                }
                break;

            case R.id.nav_speed_test:
                try {

                    Answers.getInstance().logContentView(new ContentViewEvent()
                            .putContentName("NAV - Wifi Speed Test"));

//                    if (AppUtils.ads_interstitial_show_all) {
//
//                        if (AppUtils.adsNetworkType != AppUtils.NONE_TYPE) {
//                            AppUtils.getInstance().showAdsFullBanner(new AppUtils.BaseAdListener() {
//                                @Override
//                                public void onAdClosed() {
//                                    Intent intent = new Intent(MainAppActivity_v2.this, SpeedTestMiniActivity.class);
//                                    startActivity(intent);
//                                }
//                            });
//                        } else {
//                            intent = new Intent(MainAppActivity_v2.this, SpeedTestMiniActivity.class);
//                            startActivity(intent);
//                        }
//
//                    } else {
//
//                        if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
//                            AppUtils.getInstance().showAdmobAdsFullBanner(new AppUtils.BaseAdListener() {
//                                @Override
//                                public void onAdClosed() {
//                                    Intent intent = new Intent(MainAppActivity_v2.this, SpeedTestMiniActivity.class);
//                                    startActivity(intent);
//                                }
//                            });
//                        } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
//                            AppUtils.getInstance().showFBAdsFullBanner(new AppUtils.BaseAdListener() {
//                                @Override
//                                public void onAdClosed() {
//                                    Intent intent = new Intent(MainAppActivity_v2.this, SpeedTestMiniActivity.class);
//                                    startActivity(intent);
//                                }
//                            });
//                        } else {
//                            intent = new Intent(MainAppActivity_v2.this, SpeedTestMiniActivity.class);
//                            startActivity(intent);
//                        }
//                    }

                    intent = new Intent(MainAppActivity_v2.this, SpeedTestMiniActivity.class);
                    startActivity(intent);

                } catch (Exception ignored) {
                    Crashlytics.logException(ignored);
                }
                break;

            case R.id.nav_speed_test_result:
                try {

                    Answers.getInstance().logContentView(new ContentViewEvent()
                            .putContentName("NAV - Test History Results"));

//                    if(AppUtils.ads_interstitial_show_all) {
//
//                        if (AppUtils.adsNetworkType != AppUtils.NONE_TYPE) {
//                            AppUtils.getInstance().showAdsFullBanner(new AppUtils.BaseAdListener() {
//                                @Override
//                                public void onAdClosed() {
//
//                                    Intent intent = new Intent(MainAppActivity_v2.this, SpeedTestHistoryActivity.class);
//                                    intent.putExtra(MainAppActivity_v2.COLOR_MESSAGE, currentColor);
//                                    startActivity(intent);
//                                }
//                            });
//                        } else {
//
//                            intent = new Intent(MainAppActivity_v2.this, SpeedTestHistoryActivity.class);
//                            intent.putExtra(MainAppActivity_v2.COLOR_MESSAGE, currentColor);
//                            startActivity(intent);
//
//                        }
//
//                    }else {
//
//                        if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
//                            AppUtils.getInstance().showAdmobAdsFullBanner(new AppUtils.BaseAdListener() {
//                                @Override
//                                public void onAdClosed() {
//
//                                    Intent intent = new Intent(MainAppActivity_v2.this, SpeedTestHistoryActivity.class);
//                                    intent.putExtra(MainAppActivity_v2.COLOR_MESSAGE, currentColor);
//                                    startActivity(intent);
//                                }
//                            });
//                        } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
//                            AppUtils.getInstance().showFBAdsFullBanner(new AppUtils.BaseAdListener() {
//                                @Override
//                                public void onAdClosed() {
//
//                                    Intent intent = new Intent(MainAppActivity_v2.this, SpeedTestHistoryActivity.class);
//                                    intent.putExtra(MainAppActivity_v2.COLOR_MESSAGE, currentColor);
//                                    startActivity(intent);
//                                }
//                            });
//                        } else {
//
//                            intent = new Intent(MainAppActivity_v2.this, SpeedTestHistoryActivity.class);
//                            intent.putExtra(MainAppActivity_v2.COLOR_MESSAGE, currentColor);
//                            startActivity(intent);
//                        }
//                    }
//
                    intent = new Intent(MainAppActivity_v2.this, SpeedTestHistoryActivity.class);
                    intent.putExtra(MainAppActivity_v2.COLOR_MESSAGE, currentColor);
                    startActivity(intent);

                } catch (Exception ignored) {
                    Crashlytics.logException(ignored);
                }

                break;

            case R.id.nav_wifi_map:
                try {

                    Answers.getInstance().logContentView(new ContentViewEvent()
                            .putContentName("NAV - Network Map Location"));

                    final double lat;
                    final double lng;

                    if(AppUtils.getInstance().getAppNetworkInfo() == null || AppUtils.getInstance().getAppNetworkInfo().getNetworkInformation() == null) {
                        lat = 0;
                        lng = 0;
                    }else {
                        lat = Double.parseDouble(AppUtils.getInstance().getAppNetworkInfo().getNetworkInformation().getLat());
                        lng = Double.parseDouble(AppUtils.getInstance().getAppNetworkInfo().getNetworkInformation().getLon());
                    }

//                    if (AppUtils.ads_interstitial_show_all) {
//
//                        if (AppUtils.adsNetworkType != AppUtils.NONE_TYPE) {
//                            AppUtils.getInstance().showAdsFullBanner(new AppUtils.BaseAdListener() {
//                                @Override
//                                public void onAdClosed() {
//                                    Intent intent = new Intent(MainAppActivity_v2.this, MapsMarkerActivity.class);
//                                    intent.putExtra(MapsMarkerActivity.LAT_MESSAGE, lat);
//                                    intent.putExtra(MapsMarkerActivity.LNG_MESSAGE, lng);
//                                    startActivity(intent);
//                                }
//                            });
//                        } else {
//                            intent = new Intent(MainAppActivity_v2.this, MapsMarkerActivity.class);
//                            intent.putExtra(MapsMarkerActivity.LAT_MESSAGE, lat);
//                            intent.putExtra(MapsMarkerActivity.LNG_MESSAGE, lng);
//                            startActivity(intent);
//                        }
//
//                    } else {
//
//                        if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
//                            AppUtils.getInstance().showAdmobAdsFullBanner(new AppUtils.BaseAdListener() {
//                                @Override
//                                public void onAdClosed() {
//                                    Intent intent = new Intent(MainAppActivity_v2.this, MapsMarkerActivity.class);
//                                    intent.putExtra(MapsMarkerActivity.LAT_MESSAGE, lat);
//                                    intent.putExtra(MapsMarkerActivity.LNG_MESSAGE, lng);
//                                    startActivity(intent);
//                                }
//                            });
//                        } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
//                            AppUtils.getInstance().showFBAdsFullBanner(new AppUtils.BaseAdListener() {
//                                @Override
//                                public void onAdClosed() {
//                                    Intent intent = new Intent(MainAppActivity_v2.this, MapsMarkerActivity.class);
//                                    intent.putExtra(MapsMarkerActivity.LAT_MESSAGE, lat);
//                                    intent.putExtra(MapsMarkerActivity.LNG_MESSAGE, lng);
//                                    startActivity(intent);
//                                }
//                            });
//                        } else {
//
//                            intent = new Intent(MainAppActivity_v2.this, MapsMarkerActivity.class);
//                            intent.putExtra(MapsMarkerActivity.LAT_MESSAGE, lat);
//                            intent.putExtra(MapsMarkerActivity.LNG_MESSAGE, lng);
//                            startActivity(intent);
//                        }
//                    }

                    intent = new Intent(MainAppActivity_v2.this, MapsMarkerActivity.class);
                    intent.putExtra(MapsMarkerActivity.LAT_MESSAGE, lat);
                    intent.putExtra(MapsMarkerActivity.LNG_MESSAGE, lng);
                    startActivity(intent);

                } catch (Exception ignored) {
                    Crashlytics.logException(ignored);
                }
                break;

            case R.id.nav_wifi_map_explorer:
                try {

                    Answers.getInstance().logContentView(new ContentViewEvent()
                            .putContentName("NAV - Wi-Fi Map Explorer"));


//                    if (AppUtils.ads_interstitial_show_all) {
//
//                        if (AppUtils.adsNetworkType != AppUtils.NONE_TYPE) {
//                            AppUtils.getInstance().showAdsFullBanner(new AppUtils.BaseAdListener() {
//                                @Override
//                                public void onAdClosed() {
//                                    Intent intent = new Intent(AppController.getInstance().getAppContext(), MapActivity.class);
//                                    startActivity(intent);
//                                }
//                            });
//                        } else {
//                            intent = new Intent(AppController.getInstance().getAppContext(), MapActivity.class);
//                            startActivity(intent);
//                        }
//
//                    } else {
//
//                        if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
//                            AppUtils.getInstance().showAdmobAdsFullBanner(new AppUtils.BaseAdListener() {
//                                @Override
//                                public void onAdClosed() {
//                                    Intent intent = new Intent(AppController.getInstance().getAppContext(), MapActivity.class);
//                                    startActivity(intent);
//                                }
//                            });
//                        } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
//                            AppUtils.getInstance().showFBAdsFullBanner(new AppUtils.BaseAdListener() {
//                                @Override
//                                public void onAdClosed() {
//                                    Intent intent = new Intent(AppController.getInstance().getAppContext(), MapActivity.class);
//                                    startActivity(intent);
//                                }
//                            });
//                        } else {
//
//                            intent = new Intent(AppController.getInstance().getAppContext(), MapActivity.class);
//                            startActivity(intent);
//                        }
//                    }

                    intent = new Intent(AppController.getInstance().getAppContext(), MapActivity.class);
                    startActivity(intent);

                } catch (Exception ignored) {
                    Crashlytics.logException(ignored);
                }
                break;

            case R.id.nav_share:

                try{

                    Answers.getInstance().logContentView(new ContentViewEvent()
                            .putContentName("NAV - ShareAppToFacebook"));

                    if(AppUtils.appConfig != null){

                        setContentUrl = AppUtils.appConfig.getData().getSpeedTestSharing()
                                .get_share_contentUrl();
                        setImageUrl = AppUtils.appConfig.getData().getSpeedTestSharing()
                                .get_share_setImageUrl();

                    } else{
                        setContentUrl = AppController.getInstance().getAppContext().getString(R.string.share_contentUrl);
                        setImageUrl = AppController.getInstance().getAppContext().getString(R.string.share_setImageUrl);
                    }

                }catch (Exception e){
                    Crashlytics.logException(e);
                    setContentUrl = AppController.getInstance().getAppContext().getString(R.string.share_contentUrl);
                    setImageUrl = AppController.getInstance().getAppContext().getString(R.string.share_setImageUrl);
                }


                try{
                    ShareDialog shareDialog = new ShareDialog(MainAppActivity_v2.this);

                    if (ShareDialog.canShow(ShareLinkContent.class)) {

                        ShareLinkContent content = new ShareLinkContent.Builder()
                                .setContentUrl(Uri.parse(setContentUrl))
                                .setImageUrl(Uri.parse(setImageUrl))
                                .setContentTitle(MainAppActivity_v2.this.getString(R.string.share_setTitle))
                                .setContentDescription(MainAppActivity_v2.this.getString(R.string.share_setContentDescription))
                                .build();
                        shareDialog.show(content);
                    }
                }catch (Exception e){
                    Crashlytics.logException(e);
                    Toast.makeText(MainAppActivity_v2.this
                            , "something went wrong, we will fix this as soon as possible."
                            , Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.nav_rating:

                Answers.getInstance().logContentView(new ContentViewEvent()
                        .putContentName("NAV - RateOnPlayStore"));

                AppUtils.getInstance().openPlaystore(MainAppActivity_v2.this);
                break;

            case R.id.nav_feedback:

                Answers.getInstance().logContentView(new ContentViewEvent()
                        .putContentName("NAV - Feedback"));

                try{
                    final RatingDialog ratingDialog = new RatingDialog.Builder(MainAppActivity_v2.this)
                            .threshold(3)
                            .onRatingBarFormSumbit(new RatingDialog.Builder.RatingDialogFormListener() {
                                @Override
                                public void onFormSubmitted(String feedback) {

                                    Answers.getInstance().logCustom(new CustomEvent("About")
                                            .putCustomAttribute("About-Feedback", feedback));

                                    showDialogThankyou();
                                }
                            })
                            .build();
                    ratingDialog.show();
                    ratingDialog.openForm();

                }catch (Exception e){

                    Crashlytics.logException(e);
                    Toast.makeText(MainAppActivity_v2.this
                            , "something went wrong, we will fix this as soon as possible."
                            , Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.nav_visit_website:

                try{

                    Answers.getInstance().logContentView(new ContentViewEvent()
                            .putContentName("NAV - Website"));

                    if(AppUtils.appConfig != null){

                        setContentUrl = AppUtils.appConfig.getData().getSpeedTestSharing()
                                .get_share_contentUrl();
                        setImageUrl = AppUtils.appConfig.getData().getSpeedTestSharing()
                                .get_share_setImageUrl();

                    } else{
                        setContentUrl = AppController.getInstance().getAppContext().getString(R.string.share_contentUrl);
                        setImageUrl = AppController.getInstance().getAppContext().getString(R.string.share_setImageUrl);
                    }

                }catch (Exception e){
                    Crashlytics.logException(e);
                    setContentUrl = AppController.getInstance().getAppContext().getString(R.string.share_contentUrl);
                    setImageUrl = AppController.getInstance().getAppContext().getString(R.string.share_setImageUrl);
                }

                try{

                    new FinestWebView.Builder(MainAppActivity_v2.this)
                            .toolbarColor(currentColor)
                            .statusBarColor(currentColor)
                            .swipeRefreshColor(currentColor)
                            .disableIconMenu(true)
                            .showUrl(false)
                            .show(setContentUrl);
                }catch (Exception e){
                    Crashlytics.logException(e);
                    Toast.makeText(MainAppActivity_v2.this
                            , "something went wrong, we will fix this as soon as possible."
                            , Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.nav_privacy_policy:

                try{

                    Answers.getInstance().logContentView(new ContentViewEvent()
                            .putContentName("NAV - Privacy"));

                    String url_privacy_policy = MainAppActivity_v2.this.getResources().getString(R.string.url_privacy_policy);
                    new FinestWebView.Builder(MainAppActivity_v2.this)
                            .toolbarColor(currentColor)
                            .statusBarColor(currentColor)
                            .statusBarColor(currentColor)
                            .disableIconMenu(true)
                            .showUrl(false)
                            .show(url_privacy_policy);
                }catch (Exception e){
                    Crashlytics.logException(e);
                    Toast.makeText(MainAppActivity_v2.this
                            , "something went wrong, we will fix this as soon as possible."
                            , Toast.LENGTH_SHORT).show();
                }
                break;

//            case R.id.nav_aboutus:
//
//                try{
//
//                    Answers.getInstance().logContentView(new ContentViewEvent()
//                            .putContentName("NAV - About"));
//
//                    intent = new Intent(this, AboutActivity.class);
//                    intent.putExtra(MainAppActivity.COLOR_MESSAGE, currentColor);
//                    startActivity(intent);
//
//
//                }catch (Exception e){
//                    Crashlytics.logException(e);
//                }
//                break;

            case R.id.nav_free_wifi_hotspot:

                try{

                    Answers.getInstance().logContentView(new ContentViewEvent()
                            .putContentName("NAV - Free WiFi Hotspot"));

                    openFreeWiFiHotspot();


                }catch (Exception e){
                    Crashlytics.logException(e);
                }
                break;
        }

    }

    private void getNetworkInformation() {

        if(AppUtils.getInstance().getAppNetworkInfo() == null || AppUtils.getInstance().getAppNetworkInfo().getExternal_IP() == null) {
            getExternal_IP();
        }


        if(AppUtils.getInstance().getAppNetworkInfo() == null || AppUtils.getInstance().getAppNetworkInfo().getNetworkInformation() == null) {
            getNetworkInfo();
        }
    }


    private void getNetworkInfo(){
        String get_networkinfo_url = "http://ip-api.com/json/";
        //Volley
        GsonRequest reqNetworkInfo = ServiceAPI.getNetworkInfo(get_networkinfo_url,new Response.Listener<NetworkInformation>() {
            @Override
            public void onResponse(NetworkInformation response) {

                if(response != null){
                    AppUtils.getInstance().getAppNetworkInfo().setNetworkInformation(response);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        reqNetworkInfo.setShouldCache(false);
        AppController.getInstance().addToRequestQueue(reqNetworkInfo, TAG);
    }

    private void getExternal_IP(){
        String get_external_ip_url = "https://api.ipify.org?format=json";
        //Volley
        GsonRequest reqExternalIp = ServiceAPI.getExternalIP(get_external_ip_url,new Response.Listener<External_IP>() {
            @Override
            public void onResponse(External_IP response) {

                if(response != null){
                    AppUtils.getInstance().getAppNetworkInfo().setExternal_IP(response);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        reqExternalIp.setShouldCache(false);
        AppController.getInstance().addToRequestQueue(reqExternalIp, TAG);
    }

    //Check version update on play store
    private void checkVersionUpdate(){
        try {
            if (!MainAppActivity_v2.this.isFinishing()) {
                UpdateChecker checker = new UpdateChecker(MainAppActivity_v2.this); // If you are in a Activity or a FragmentActivity
                checker.setNoticeIcon(R.mipmap.ic_launcher);
//                checker.setNotice(Notice.NOTIFICATION);
                checker.setSuccessfulChecksRequired(3);
                checker.start();
            }
        } catch (Exception ignored) {
            Crashlytics.logException(ignored);
        }
    }

    private void RequestAdsToDisplay() {

        if(mRequestAdsConfig){
            return;
        }

        String adsconfigs_url = this.getResources().getString(R.string.ads_configs_url);
        //Volley
        GsonRequest req = ServiceAPI.getAdsRequest(adsconfigs_url,new Response.Listener<AppConfig>() {
            @Override
            public void onResponse(AppConfig response) {
                Log.d("NTL", "RequestAdsToDisplay = onResponse", null);

                try {

                    String packagename = MainAppActivity_v2.this.getPackageName();
                    if(!packagename.equalsIgnoreCase(response.getData().getPackagename())){
                        finish();
                    }

                    AppUtils.appConfig = response;
                    AppUtils.defaultads = response.getData().getDefaultads();
                    AppUtils.interstitial_count_maximum = response.getData().getAds_interstitial_count();
                    AppUtils.ads_interstitial_show_all = response.getData().isAds_interstitial_show_all();

                    if(AppUtils.defaultads.equalsIgnoreCase("admob")) {
                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putInt(AppUtils.ADS_TYPE_KEY, AppUtils.ADMOB_ADS_TYPE);
                        editor.apply();

                    }else if(AppUtils.defaultads.equalsIgnoreCase("facebook")) {
                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putInt(AppUtils.ADS_TYPE_KEY, AppUtils.FB_ADS_TYPE);
                        editor.apply();
                    }


                    if(AppUtils.ads_interstitial_show_all){
                        //Init all Ad InterstitialAd
                        AppUtils.getInstance().fullAdmobAdsBannerInit(MainAppActivity_v2.this);
                        //Interstitial
                        mFBInterstitialAd = AppUtils.getInstance().FBfullBannerInit(MainAppActivity_v2.this);
                    }

                    mRequestAdsConfig = true;

                } catch (Exception ignored) {
                    Crashlytics.logException(ignored);
                }


//                int size = response.getData().getAds().size();
//                for(int i=0; i < size; i++){
//                    String source = response.getData().getAds().get(i).getSource();
//                    if(!source.equals(AppUtils.defaultads)){
//                        continue;
//                    }
//                    AppUtils.source = source;
//                    AppUtils.ads_banner_id = response.getData().getAds().get(i).getAds_banner_id();
//                    AppUtils.ads_interstitial_id = response.getData().getAds().get(i).getAds_interstitial_id();
//                    AppUtils.ads_native_id = response.getData().getAds().get(i).getAds_native_id();
//                    break;
//                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("NTL", "RequestAdsToDisplay = onErrorResponse", null);
            }
        });

        req.setShouldCache(false);
        AppController.getInstance().addToRequestQueue(req, TAG);

    }

    private boolean isFirstTime() {
        final String ranBeforeKey = "RanBefore";
        SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        boolean ranBefore = preferences.getBoolean(ranBeforeKey, false);
        if (!ranBefore) {
            // first time
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean(ranBeforeKey, true);
            editor.apply();
        }
        return !ranBefore;
    }

    public void loadTutorial() {

        try {
            Intent intent = new Intent(MainAppActivity_v2.this, IntroActivity.class);
            startActivity(intent);
        } catch (Exception ignored) {
            Crashlytics.logException(ignored);
        }
    }

    private static int requestCode_ACTION_MANAGE_WRITE_SETTINGS = 200;
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == requestCode_ACTION_MANAGE_WRITE_SETTINGS){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (!Settings.System.canWrite(getApplicationContext())) {
                    retryOpenWifiHotspot();
                } else {
                    inviteFriend(mHotUtil);
                }
            }
        }
    }

    private void openFreeWiFiHotspot(){

//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                    if (!Settings.System.canWrite(MainAppActivity.this)) {
//                        MainAppActivityPermissionsDispatcher.inviteFriendWithPermissionCheck(MainAppActivity.this, mHotUtil);
//                        break;
//                    }
//                }
//
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                    if (!Settings.System.canWrite(getApplicationContext())) {
//                        intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS, Uri.parse("package:" + getPackageName()));
//                        startActivityForResult(intent, requestCode_ACTION_MANAGE_WRITE_SETTINGS);
//                        break;
//                    }
//                }

        try {

            if (!isShowWifiHotspotIntro) {
                //Show speed test history intro
                if(mFabOpenWifiHotspot != null){
                    showIntroWifiHotspot(mFabOpenWifiHotspot);
                }
            }

            if(mWifiHotspotEnable){
                disableWiFiHotspot();
                return;
            }

//            if(AppUtils.ads_interstitial_show_all) {
//
//                if (AppUtils.adsNetworkType != AppUtils.NONE_TYPE) {
//                    AppUtils.getInstance().showAdsFullBanner(new AppUtils.BaseAdListener() {
//                        @Override
//                        public void onAdClosed() {
//
//                            inviteFriend(mHotUtil);
//                        }
//                    });
//
//                } else {
//
//                    inviteFriend(mHotUtil);
//                }
//
//            }else {
//
//                if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
//                    AppUtils.getInstance().showAdmobAdsFullBanner(new AppUtils.BaseAdListener() {
//                        @Override
//                        public void onAdClosed() {
//
//                            inviteFriend(mHotUtil);
//                        }
//                    });
//                } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
//                    AppUtils.getInstance().showFBAdsFullBanner(new AppUtils.BaseAdListener() {
//                        @Override
//                        public void onAdClosed() {
//
//                            inviteFriend(mHotUtil);
//                        }
//                    });
//                } else {
//
//                    inviteFriend(mHotUtil);
//                }
//
//            }

            inviteFriend(mHotUtil);

        } catch (Exception ignored) {
            Crashlytics.logException(ignored);
        }

    }

    private void stopWiFiHotspot(){
        if(mHotUtil != null){

            //Stop hotspot
            mHotUtil.setAndStartHotSpotCheckAndroidVersion(false,"", "");
            mFabOpenWifiHotspot.setVisibility(View.GONE);
            mWifiHotspotEnable = false;

            final SweetAlertDialog pDialog = new SweetAlertDialog(MainAppActivity_v2.this, SweetAlertDialog.PROGRESS_TYPE)
                    .setTitleText("Loading");
            pDialog.show();
            pDialog.setCancelable(false);
            new CountDownTimer(800 * 2, 800) {
                public void onTick(long millisUntilFinished) {
                    // you can change the progress bar color by ProgressHelper every 800 millis
                    i++;
                    switch (i){
                        case 0:
                            pDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.blue_btn_bg_color));
                            break;
                        case 1:
                            pDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.material_deep_teal_50));
                            break;
                        case 2:
                            pDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.success_stroke_color));
                            break;
                        case 3:
                            pDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.material_deep_teal_20));
                            break;
                        case 4:
                            pDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.material_blue_grey_80));
                            break;
                        case 5:
                            pDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.warning_stroke_color));
                            break;
                        case 6:
                            pDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.success_stroke_color));
                            break;
                    }
                }

                public void onFinish() {
                    i = -1;
                    pDialog.setTitleText("Disable Success!")
                            .setConfirmText("OK")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {

                                    pDialog.dismiss();

                                    mHandler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {

                                            try {
                                                if(AppUtils.ads_interstitial_show_all) {

                                                    if (AppUtils.adsNetworkType != AppUtils.NONE_TYPE) {
                                                        AppUtils.getInstance().showAdsFullBanner(null);
                                                    }

                                                }else {

                                                    if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {

                                                        AppUtils.getInstance().showAdmobAdsFullBanner(null);

                                                    } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {

                                                        AppUtils.getInstance().showFBAdsFullBanner(null);

                                                    }
                                                }
                                            } catch (Exception ignored) {
                                                Crashlytics.logException(ignored);
                                            }

                                        }
                                    }, 200);

                                }
                            })
                            .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);

                    toggleWifi.performClick();

                    setMenuTextWifiHotsot(false);

                }
            }.start();

        }
    }

    private void disableWiFiHotspot(){
        new SweetAlertDialog(MainAppActivity_v2.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                .setTitleText("Disable Hotspot ?")
                .setContentText("AP Name : FreeHotspot\nPassword : thankyou")
                .setCustomImage(R.drawable.ic_free_wifi_hotspot)
                .setConfirmText("Disable")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {

                        sweetAlertDialog.dismissWithAnimation();
                        stopWiFiHotspot();

                    }
                })
                .setCancelText("Dismiss")
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismissWithAnimation();
                    }
                })
                .show();
    }

    private void retryOpenWifiHotspot(){
        final CoordinatorLayout coordinatorLayout = (CoordinatorLayout) findViewById(R.id
                .coordinatorLayout);
        Snackbar snackbar = Snackbar
                .make(coordinatorLayout, "Unable to Open Free Hotspot", Snackbar.LENGTH_LONG)
                .setAction("RETRY", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        inviteFriend(mHotUtil);
                    }
                });

        snackbar.show();
    }

    public void inviteFriend(wifiHotSpots hotutil)
    {

//        try {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.System.canWrite(getApplicationContext())) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS, Uri.parse("package:" + getPackageName()));
                startActivityForResult(intent, requestCode_ACTION_MANAGE_WRITE_SETTINGS);
                return;
            }
        }

        if(hotutil != null){

//            Toast.makeText(MainAppActivity.this,"Enable Free WiFi Hotspot",Toast.LENGTH_LONG).show();

            //Start Hotspot
            hotutil.setAndStartHotSpotCheckAndroidVersion(true, "FreeHotspot", "thankyou");
            mWifiHotspotEnable = true;

            final SweetAlertDialog pDialog = new SweetAlertDialog(MainAppActivity_v2.this, SweetAlertDialog.PROGRESS_TYPE)
                    .setTitleText("Loading");
            pDialog.show();
            pDialog.setCancelable(false);
            new CountDownTimer(800 * 2, 800) {
                public void onTick(long millisUntilFinished) {
                    // you can change the progress bar color by ProgressHelper every 800 millis
                    i++;
                    switch (i){
                        case 0:
                            pDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.blue_btn_bg_color));
                            break;
                        case 1:
                            pDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.material_deep_teal_50));
                            break;
                        case 2:
                            pDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.success_stroke_color));
                            break;
                        case 3:
                            pDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.material_deep_teal_20));
                            break;
                        case 4:
                            pDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.material_blue_grey_80));
                            break;
                        case 5:
                            pDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.warning_stroke_color));
                            break;
                        case 6:
                            pDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.success_stroke_color));
                            break;
                    }
                }

                public void onFinish() {
                    i = -1;

                    pDialog.setTitleText("Free Hotspot now active")
                            .setContentText("AP Name : FreeHotspot\nPassword : thankyou")
                            .setConfirmText("OK")
                            .setCustomImage(R.drawable.ic_free_wifi_hotspot)
                            .changeAlertType(SweetAlertDialog.CUSTOM_IMAGE_TYPE);

                    mFabOpenWifiHotspot.setVisibility(View.VISIBLE);

                    setMenuTextWifiHotsot(true);
                }
            }.start();

        }
//        } catch (Exception ignored) {
//            Crashlytics.logException(ignored);
//        }

    }

    private void setMenuTextWifiHotsot(boolean isHotspotActive){
        if(mNavigationView != null){
            if(isHotspotActive){
                mNavigationView.getMenu().findItem(R.id.nav_free_wifi_hotspot).setTitle("Disable Wi-Fi Hotspot");
                if(mMainFreeWiFiFragment != null){
                    mMainFreeWiFiFragment.setWiFiHotspotMenuText("Disable Wi-Fi Hotspot");
                }
            } else {
                mNavigationView.getMenu().findItem(R.id.nav_free_wifi_hotspot).setTitle("Enable Wi-Fi Hotspot");
                if(mMainFreeWiFiFragment != null){
                    mMainFreeWiFiFragment.setWiFiHotspotMenuText("Enable Wi-Fi Hotspot");
                }
            }
        }
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.gps_auth_message);
        builder.setCancelable(false);

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, final int id) {
//                startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                try {
                    MainAppActivity_v2.this.startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                } catch (Exception e) {
                    Crashlytics.logException(e);
                }
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, final int id) {
                dialog.cancel();
            }
        });

        builder.create().show();
    }

    private void showIntroWifiHotspot(View view){
        if(view == null) return;

        try {
            if(!isShowWifiHotspotIntro) {

                new MaterialIntroView.Builder(this)
//                    .enableDotAnimation(true)
                        .setTargetPadding(40)
                        .enableIcon(false)
                        .setFocusGravity(FocusGravity.CENTER)
                        .setFocusType(Focus.MINIMUM)
                        .setDelayMillis(500)
                        .enableFadeAnimation(true)
                        .performClick(true)
                        .setInfoText("Hooray ! You can now share internet to your friend and disable hotspot right here.")
                        .setShape(ShapeType.CIRCLE)
                        .setTarget(view)
                        .setUsageId("intro_wifi_hotspot") //THIS SHOULD BE UNIQUE ID
                        .dismissOnTouch(true)
                        .show();

                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(AppUtils.SHOW_INTRO_WIFI_HOTSPOT_TYPE_KEY, true);
                editor.apply();

                isShowWifiHotspotIntro = true;
            }
        } catch (Exception ignored) {
            Crashlytics.logException(ignored);
        }

    }

    private void exitApp(){
        new SweetAlertDialog(MainAppActivity_v2.this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Exit.")
                .setContentText("Do you want to exit ?")
                .setConfirmText("Yes")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {

                        finish();

                    }
                })
                .setCancelText("No")
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {

                        sweetAlertDialog.dismissWithAnimation();

                    }
                })
                .show();
    }
}
