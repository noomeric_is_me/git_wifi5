package wimosalsafimainapp.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;

import com.jao.freewifi.wifimanager.R;
import com.codemybrainsout.ratingdialog.RatingDialog;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.thefinestartist.finestwebview.FinestWebView;

import wimosalsafifreewifi.application.AppController;
import wimosalsafifreewifi.main.MainAppActivity;
import mehdi.sakout.aboutpage.AboutPage;
import mehdi.sakout.aboutpage.Element;
import utils.AppUtils;

/**
 * Created by NTL on 6/11/2017 AD.
 */

public class AboutActivity extends AppCompatActivity {

    private int color_message;
    private String setContentUrl = "";
    private String setImageUrl = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        simulateDayNight(/* DAY */ 0);

        Intent intent = getIntent();
        color_message = intent.getIntExtra(MainAppActivity.COLOR_MESSAGE,0);

        Context context = AppController.getInstance().getAppContext();


        try{
            if(AppUtils.appConfig != null){

                setContentUrl = AppUtils.appConfig.getData().getSpeedTestSharing()
                        .get_share_contentUrl();
                setImageUrl = AppUtils.appConfig.getData().getSpeedTestSharing()
                        .get_share_setImageUrl();

            } else{
                setContentUrl = context.getString(R.string.share_contentUrl);
                setImageUrl = context.getString(R.string.share_setImageUrl);
            }

        }catch (Exception e){
            Crashlytics.logException(e);
            setContentUrl = context.getString(R.string.share_contentUrl);
            setImageUrl = context.getString(R.string.share_setImageUrl);
        }


        //Version
        String version = "1.0+";
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        //Send Feedback
        Element feedbackElement = new Element();
        feedbackElement.setTitle("Send Feedback");
        feedbackElement.setIconDrawable(R.drawable.about_icon_email);
        feedbackElement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Answers.getInstance().logCustom(new CustomEvent("About")
                        .putCustomAttribute("About-Feedback", "Click"));

                try{
                    final RatingDialog ratingDialog = new RatingDialog.Builder(AboutActivity.this)
                            .threshold(3)
                            .onRatingBarFormSumbit(new RatingDialog.Builder.RatingDialogFormListener() {
                                @Override
                                public void onFormSubmitted(String feedback) {

                                    Answers.getInstance().logCustom(new CustomEvent("About")
                                            .putCustomAttribute("About-Feedback", feedback));

                                    showDialogThankyou();
                                }
                            })
                            .build();
                    ratingDialog.show();
                    ratingDialog.openForm();

                }catch (Exception e){

                    Crashlytics.logException(e);
                    Toast.makeText(AboutActivity.this
                            , "something went wrong, we will fix this as soon as possible."
                            , Toast.LENGTH_SHORT).show();
                }
            }
        });

        //Privacy
        Element privacyElement = new Element();
        privacyElement.setTitle("Privacy Policy");
        privacyElement.setIconDrawable(R.drawable.about_icon_link);
        privacyElement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Answers.getInstance().logCustom(new CustomEvent("About")
                        .putCustomAttribute("About-Privacy", "Click"));

                try{
                    String url_privacy_policy = AboutActivity.this.getResources().getString(R.string.url_privacy_policy);
                    new FinestWebView.Builder(AboutActivity.this)
                            .toolbarColor(color_message)
                            .statusBarColor(color_message)
                            .statusBarColor(color_message)
                            .disableIconMenu(true)
                            .showUrl(false)
                            .show(url_privacy_policy);
                }catch (Exception e){
                    Crashlytics.logException(e);
                    Toast.makeText(AboutActivity.this
                            , "something went wrong, we will fix this as soon as possible."
                            , Toast.LENGTH_SHORT).show();
                }
            }
        });

        //Rate on playstore
        Element addPlayStore = new Element();
        addPlayStore.setTitle("Rate & Write Review on the Play Store");
        addPlayStore.setIconDrawable(R.drawable.about_icon_google_play);
        addPlayStore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Answers.getInstance().logCustom(new CustomEvent("About")
                        .putCustomAttribute("About-RateOnPlayStore", "Click"));

                AppUtils.getInstance().openPlaystore(AboutActivity.this);
            }
        });


        //Website
        Element addWebsite = new Element();
        addWebsite.setTitle(AppController.getInstance().getAppContext().getResources()
                            .getString(R.string.about_website));
        addWebsite.setIconDrawable(R.drawable.about_icon_link);
        addWebsite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Answers.getInstance().logCustom(new CustomEvent("About")
                        .putCustomAttribute("About-Website", "Click"));

                try{

                    new FinestWebView.Builder(AboutActivity.this)
                            .toolbarColor(color_message)
                            .statusBarColor(color_message)
                            .swipeRefreshColor(color_message)
                            .disableIconMenu(true)
                            .showUrl(false)
                            .show(setContentUrl);
                }catch (Exception e){
                    Crashlytics.logException(e);
                    Toast.makeText(AboutActivity.this
                            , "something went wrong, we will fix this as soon as possible."
                            , Toast.LENGTH_SHORT).show();
                }
            }
        });

        //Share App to facebook
        Element addShareAppToFacebook = new Element();
        addShareAppToFacebook.setTitle("Share");
        addShareAppToFacebook.setIconDrawable(R.drawable.about_icon_facebook);
        addShareAppToFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Answers.getInstance().logCustom(new CustomEvent("About")
                        .putCustomAttribute("About-ShareAppToFacebook", "Click"));

                try{
                    ShareDialog shareDialog = new ShareDialog(AboutActivity.this);

                    if (ShareDialog.canShow(ShareLinkContent.class)) {

                        ShareLinkContent content = new ShareLinkContent.Builder()
                                .setContentUrl(Uri.parse(setContentUrl))
                                .setImageUrl(Uri.parse(setImageUrl))
                                .setContentTitle(AboutActivity.this.getString(R.string.share_setTitle))
                                .setContentDescription(AboutActivity.this.getString(R.string.share_setContentDescription))
                                .build();
                        shareDialog.show(content);
                    }
                }catch (Exception e){
                    Crashlytics.logException(e);
                    Toast.makeText(AboutActivity.this
                            , "something went wrong, we will fix this as soon as possible."
                            , Toast.LENGTH_SHORT).show();
                }
            }
        });

        View aboutPage = new AboutPage(this)
                .setDescription(AboutActivity.this.getString(R.string.about_description))
                .isRTL(false)
                .setImage(R.drawable.dummy_image)
                .addItem(new Element().setTitle("Version: " + version))
//                .addPlayStore("com.hack.free.wifi.manager.hotspot","Rate us & Write Review on the Play Store")
                .addItem(addPlayStore)
                .addGroup("Connect with us")
                .addItem(addShareAppToFacebook)
                .addItem(feedbackElement)
//                .addEmail("mobiletooling@gmail.com")
//                .addWebsite(setContentUrl)
                .addItem(addWebsite)
                .addItem(privacyElement)
                .addItem(getCopyRightsElement())
                .create();

        setContentView(aboutPage);
    }


    Element getCopyRightsElement() {
        Element copyRightsElement = new Element();
//        final String copyrights = String.format(getString(R.string.copy_right), Calendar.getInstance().get(Calendar.YEAR));
        copyRightsElement.setTitle(getString(R.string.copy_right) + " 2017");
        copyRightsElement.setIconDrawable(R.drawable.about_icon_copy_right);
        copyRightsElement.setIconTint(mehdi.sakout.aboutpage.R.color.about_item_icon_color);
        copyRightsElement.setIconNightTint(android.R.color.white);
        copyRightsElement.setGravity(Gravity.CENTER);
//        copyRightsElement.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(AboutActivity.this, "copyrights", Toast.LENGTH_SHORT).show();
//            }
//        });
        return copyRightsElement;
    }

    void simulateDayNight(int currentSetting) {
        final int DAY = 0;
        final int NIGHT = 1;
        final int FOLLOW_SYSTEM = 3;

        int currentNightMode = getResources().getConfiguration().uiMode
                & Configuration.UI_MODE_NIGHT_MASK;
        if (currentSetting == DAY && currentNightMode != Configuration.UI_MODE_NIGHT_NO) {
            AppCompatDelegate.setDefaultNightMode(
                    AppCompatDelegate.MODE_NIGHT_NO);
        } else if (currentSetting == NIGHT && currentNightMode != Configuration.UI_MODE_NIGHT_YES) {
            AppCompatDelegate.setDefaultNightMode(
                    AppCompatDelegate.MODE_NIGHT_YES);
        } else if (currentSetting == FOLLOW_SYSTEM) {
            AppCompatDelegate.setDefaultNightMode(
                    AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM);
        }
    }

    private void showDialogThankyou(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(AboutActivity.this);
        alertDialogBuilder.setTitle("Thank you !");
        alertDialogBuilder.setMessage("Thank you for your support and give us a feedback we promise to improve as soon as possible.");
        alertDialogBuilder.setPositiveButton("Dismiss",  new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // on success

                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
//        alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                if (dialog != null) {
//                    dialog.dismiss();
//                }
//            }
//
//        });

        alertDialogBuilder.show();
    }
}
