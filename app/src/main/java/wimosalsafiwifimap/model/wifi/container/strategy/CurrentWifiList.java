package wimosalsafiwifimap.model.wifi.container.strategy;

import java.util.List;

import wimosalsafiwifimap.model.wifi.WifiElement;
import wimosalsafiwifimap.model.wifi.container.strategy.sortedlist.WifiList;

/**
 * Created by Federico
 */
public class CurrentWifiList extends WifiList implements WifiListPopulate {
    @Override
    public void populate(List<WifiElement> wifiElementList) {
        clear();
        addAll(wifiElementList);
    }
}
