package wimosalsafiwifimap.model.location;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.location.Location;
import android.os.IBinder;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;
import java.util.Random;

import javax.inject.Inject;

import wimosalsafiwifimap.model.db.sqlite.DataBaseManager;
import wimosalsafiwifimap.model.location.hashmap.LocationMap;
import wimosalsafiwifimap.model.service.FusedLocationService;
import wimosalsafiwifimap.model.wifi.WifiElement;

/**
 * Created by Federico
 */
public class LocationHandler {
    private final LocationMap locationMap = new LocationMap();

    private FusedLocationService fusedLocationService;

    @Inject
    public LocationHandler(Context context, DataBaseManager dataBaseManager) {
        ServiceConnection serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                fusedLocationService = ((FusedLocationService.LocalBinder) service).getService();
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {}
        };

        Intent intent = new Intent(context, FusedLocationService.class);
        context.bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
        locationMap.putAll(dataBaseManager.selectLocationElements());
    }

    public LatLng getCurrentLatLng() {
        if (fusedLocationService == null || fusedLocationService.isLocationUnavailable()) {
            return null;
        }

        final Location location = fusedLocationService.getLocation();
        return new LatLng(location.getLatitude(), location.getLongitude());
    }

    public void populate(List<WifiElement> wifiElementList) {
        //NB. fusedLocationService may be not yet available...
        if (fusedLocationService == null || fusedLocationService.isLocationUnavailable()) {
            return;
        }

        final Location location = fusedLocationService.getLocation();
        final LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
//        final LatLng latLng = new LatLng(location.getLatitude()+getlatLngRandom(), location.getLongitude()+getlatLngRandom());

        for (WifiElement wifiElement : wifiElementList) {
            locationMap.insert(wifiElement.getBSSID(), new LocationElement(latLng, wifiElement.calculateDistance()));
        }
    }

    public LocationKeeper get(String bssid) {
        return locationMap.get(bssid);
    }

    //public boolean contain(String bssid) {
//        return locationMap.containsKey(bssid);
//    }

    private final Random RANDOM = new Random();
    public double getlatLngRandom() {

        double latLng;
        switch (RANDOM.nextInt(10)) {
            default:
            case 0:
                return 0;
            case 1:
                latLng = 0.000015;
                return latLng;
            case 2:
                latLng = 0.000016;
                return latLng;
            case 3:
                latLng = 0.000017;
                return latLng;
            case 4:
                latLng = 0.000018;
                return latLng;
            case 5:
                latLng = 0.000019;
                return latLng;
            case 6:
                latLng = 0.000020;
                return latLng;
            case 7:
                latLng = 0.000021;
                return latLng;
            case 8:
                latLng = 0.000022;
                return latLng;
            case 9:
                return 0;
        }
    }
}
