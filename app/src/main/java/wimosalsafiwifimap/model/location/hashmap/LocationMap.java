package wimosalsafiwifimap.model.location.hashmap;

import com.google.android.gms.maps.model.LatLng;

import java.util.HashMap;

import wimosalsafiwifimap.model.location.LocationElement;
import wimosalsafiwifimap.model.location.LocationKeeper;

/**
 * Created by Federico
 */
public class LocationMap extends HashMap<String, LocationKeeper> {

    public void put(String bssid, double latitude, double longitude, double radius) {
        super.put(bssid, new LocationKeeper(new LocationElement(new LatLng(latitude, longitude), radius)));
    }

    public void insert(String bssid, LocationElement locationElement) {
        final LocationKeeper locationKeeper = get(bssid);

        if (locationKeeper == null)
            put(bssid, new LocationKeeper(locationElement));
        else
            locationKeeper.addNear(locationElement);
    }
}
