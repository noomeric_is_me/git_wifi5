package wimosalsafiwifimap.utils;

import com.jao.freewifi.wifimanager.R;

/**
 * Created by federico on 04/12/15.
 */
public enum DataBaseAction {
    IS_PRESENT {
        @Override
        public int getImage() {
            return R.drawable.signal_close;
        }

        @Override
        public int getMessage() {
            return R.string.ask_remove_favourite;
        }

        @Override
        public int getResultMessage() {
            return R.string.saved_wifi_element;
        }
    },
    NOT_PRESENT {
        @Override
        public int getImage() {
            return R.drawable.signal_wifi4;
        }

        @Override
        public int getMessage() {
            return R.string.ask_add_favourite;
        }

        @Override
        public int getResultMessage() {
            return R.string.removed_wifi_element;
        }
    };

    abstract public int getImage();
    public abstract int getMessage();
    public abstract int getResultMessage();
}
