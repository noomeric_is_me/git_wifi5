package wimosalsafiwifimap.utils;


import com.jao.freewifi.wifimanager.R;

import javax.inject.Inject;

import wimosalsafiwifimap.model.db.DataBaseHandler;
import wimosalsafiwifimap.model.wifi.WifiElement;
import wimosalsafiwifimap.model.wifi.WifiKeeper;

/**
 * Created by Federico
 */
public class ResourceProvider {
    private final WifiKeeper wifiKeeper;
    private final DataBaseHandler dataBaseHandler;

    @Inject
    public ResourceProvider(WifiKeeper wifiKeeper, DataBaseHandler dataBaseHandler) {
        this.wifiKeeper = wifiKeeper;
        this.dataBaseHandler = dataBaseHandler;
    }

    public int getWifiResource(WifiElement wifiElement) {
        if (wifiKeeper.contains(wifiElement.getBSSID()) && wifiElement.isLineOfSight()) {
            return wifiElement.isSecure()
                    ? WifiSecureImageEnum.values()[wifiElement.getSignalLevel()].getResource()
                    : WifiImageEnum.values()[wifiElement.getSignalLevel()].getResource();
        }

        return R.drawable.signal_wifi1;
    }

    public int getSavedResource() {
        return R.drawable.signal_close;
    }

    public int getSavedResource(WifiElement wifiElement) {
        return dataBaseHandler.contains(wifiElement)
                ? R.drawable.signal_close
                : R.drawable.signal_wifi4;
    }
}
