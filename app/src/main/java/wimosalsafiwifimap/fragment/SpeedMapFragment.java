package wimosalsafiwifimap.fragment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.doubleclick.PublisherAdRequest;
import com.google.android.gms.ads.doubleclick.PublisherAdView;
import com.jao.freewifi.wifimanager.R;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

import wimosalsafifreewifi.application.AppController;
import wimosalsafifreewifi.main.MapsMarkerActivity;
import wimosalsafifreewifi.services.GsonRequest;
import wimosalsafifreewifi.services.ServiceAPI;
import wimosalsafifreewifi.services.model.External_IP;
import wimosalsafifreewifi.services.model.NetworkInformation;
import wimosalsafimainapp.scrollable.fragment.FragmentPagerFragment;
import utils.AppUtils;
import wimosalsafispeedtest.fragment.SpeedDetailFragment;
import wimosalsafiwifimap.activity.MapActivity;
import yalantis.com.sidemenu.interfaces.ScreenShotable;

/**
 * Created by NTL on 8/12/2017 AD.
 */

public class SpeedMapFragment extends FragmentPagerFragment implements ScreenShotable {

    public static final String TAG = SpeedMapFragment.class.getSimpleName();

    private View mView;
    private Bitmap bitmap;
    private PublisherAdView mPublisherAdView;
    private NestedScrollView mScrollView;

    public static SpeedMapFragment newInstance()
    {
        return new SpeedMapFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if(mView == null){
            mView = inflater.inflate(R.layout.fragment_speed_map, container, false);

            mScrollView = mView.findViewById(R.id.scrollView);

//            TextView text_title_network_map = (TextView) mView.findViewById(R.id.text_title_network_map);
//            text_title_network_map.setCompoundDrawablesWithIntrinsicBounds(
//                    R.drawable.ic_zoom_out_map_black_24dp, 0, 0, 0);

            ImageView wifi_map_zoom = (ImageView) mView.findViewById(R.id.wifi_map_zoom);
            wifi_map_zoom.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Answers.getInstance().logContentView(new ContentViewEvent()
                            .putContentName("Network Map Location Zoom - Image Click"));

                    if(mLat == 0 || mLng ==0){
                        return;
                    }

                    openMapsMarker();
                }
            });

            LinearLayout linear_wifi_map = (LinearLayout) mView.findViewById(R.id.linear_wifi_map);
            linear_wifi_map.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Answers.getInstance().logContentView(new ContentViewEvent()
                            .putContentName("Network Map Location Zoom - Text Click"));

                    if(mLat == 0 || mLng ==0){
                        return;
                    }

                    openMapsMarker();
                }
            });

            TextView txt_wifi_map_explorer = (TextView) mView.findViewById(R.id.txt_wifi_map_explorer);
            txt_wifi_map_explorer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Answers.getInstance().logContentView(new ContentViewEvent()
                            .putContentName("Wi-Fi Map Explorer - Text Click"));

                    openMapsExplorer();

                }
            });

            initAds();

        }

        return mView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //Get Network information
        getNetworkInformation();
    }

    @Override
    public void onDestroy() {
        if(adViewfacebook != null){
            adViewfacebook.destroy();
        }
        super.onDestroy();
    }

    private com.facebook.ads.AdView adViewfacebook;
    //    private NativeAdsManager manager;
//    private NativeAdScrollView nativeAdScrollView;
    private void initAds(){

        if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
            //Banner
//            AdView mAdView = (AdView) mView.findViewById(R.id.admob_banner_view);
//            mAdView.setVisibility(View.VISIBLE);
//            AppUtils.getInstance().showAdsBanner(mAdView);

            mPublisherAdView = mView.findViewById(R.id.admob_banner_view);
            PublisherAdRequest adRequest = new PublisherAdRequest.Builder().build();
            mPublisherAdView.setVisibility(View.VISIBLE);
            mPublisherAdView.loadAd(adRequest);

//            mPublisherAdView = (PublisherAdView) mView.findViewById(R.id.fluid_view);
//            PublisherAdRequest publisherAdRequest = new PublisherAdRequest.Builder().build();
//            mPublisherAdView.loadAd(publisherAdRequest);

            mPublisherAdView = (PublisherAdView) mView.findViewById(R.id.fluid_view_1);
            PublisherAdRequest publisherAdRequest_1 = new PublisherAdRequest.Builder().build();
            mPublisherAdView.loadAd(publisherAdRequest_1);
            //load large ads native bottom
//            NativeExpressAdView adViewNative = (NativeExpressAdView) mView.findViewById(R.id.adViewNative);
//            adViewNative.setVisibility(View.VISIBLE);
//            AppUtils.getInstance().showNativeAdsBanner(adViewNative);

//            NativeExpressAdView adViewNative_1 = (NativeExpressAdView) mView.findViewById(R.id.adViewNative_1);
//            adViewNative_1.setVisibility(View.VISIBLE);
//            AppUtils.getInstance().showNativeAdsBanner(adViewNative_1);
        } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
            //Banner
            RelativeLayout adViewContainer = (RelativeLayout) mView.findViewById(R.id.facebook_banner_ad_container);
            adViewContainer.setVisibility(View.VISIBLE);
            adViewfacebook = AppUtils.getInstance().showFBAdsBanner(AppController.getInstance().getAppContext(),adViewContainer);

            //Native
            LinearLayout adNativeViewContainer = (LinearLayout) mView.findViewById(R.id.speed_map_native_ad_small_container);
            adNativeViewContainer.setVisibility(View.VISIBLE);
            AppUtils.getInstance().showFBAdsNativeSmallInit(AppController.getInstance().getAppContext(),adNativeViewContainer);

            LinearLayout adNativeViewContainer_1 = (LinearLayout) mView.findViewById(R.id.speed_map_native_ad_container);
            adNativeViewContainer_1.setVisibility(View.VISIBLE);
            AppUtils.getInstance().showFBAdsNativeInit(AppController.getInstance().getAppContext(),adNativeViewContainer_1);

        }
    }

    private void getNetworkInformation() {

        if(AppUtils.getInstance().getAppNetworkInfo() == null || AppUtils.getInstance().getAppNetworkInfo().getExternal_IP() == null) {
            getExternal_IP();
        }


        if(AppUtils.getInstance().getAppNetworkInfo() == null || AppUtils.getInstance().getAppNetworkInfo().getNetworkInformation() == null) {
            getNetworkInfo();
        }else {
            double lat = Double.parseDouble(AppUtils.getInstance().getAppNetworkInfo().getNetworkInformation().getLat());
            double lng = Double.parseDouble(AppUtils.getInstance().getAppNetworkInfo().getNetworkInformation().getLon());
            setGoogleMap(lat,lng);
        }
    }


    private void getNetworkInfo(){
        String get_networkinfo_url = "http://ip-api.com/json/";
        //Volley
        GsonRequest reqNetworkInfo = ServiceAPI.getNetworkInfo(get_networkinfo_url,new Response.Listener<NetworkInformation>() {
            @Override
            public void onResponse(NetworkInformation response) {

                if(response != null){
                    AppUtils.getInstance().getAppNetworkInfo().setNetworkInformation(response);

                    double lat = Double.parseDouble(response.getLat());
                    double lng = Double.parseDouble(response.getLon());
                    setGoogleMap(lat,lng);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        reqNetworkInfo.setShouldCache(false);
        AppController.getInstance().addToRequestQueue(reqNetworkInfo, TAG);
    }

    private void getExternal_IP(){

        String get_external_ip_url = "https://api.ipify.org?format=json";
        //Volley
        GsonRequest reqExternalIp = ServiceAPI.getExternalIP(get_external_ip_url,new Response.Listener<External_IP>() {
            @Override
            public void onResponse(External_IP response) {

                if(response != null){
                    AppUtils.getInstance().getAppNetworkInfo().setExternal_IP(response);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        reqExternalIp.setShouldCache(false);
        AppController.getInstance().addToRequestQueue(reqExternalIp, TAG);
    }

    public String getMacAddress() {
        try {
            List<NetworkInterface> networkInterfaceList = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface networkInterface : networkInterfaceList) {
                if (!networkInterface.getName().equalsIgnoreCase("wlan0")) {
                    continue;
                }

                byte[] macAddress = networkInterface.getHardwareAddress();
                if (macAddress == null) {
                    return "";
                }

                StringBuilder result = new StringBuilder();
                for (byte data : macAddress) {
                    result.append(Integer.toHexString(data & 0xFF)).append(":");
                }

                if (result.length() > 0) {
                    result.deleteCharAt(result.length() - 1);
                }
                return result.toString();
            }
        } catch (Exception ignored) {
        }
        return "02:00:00:00:00:00";
    }

    /** Get IP For mobile */
    public static String getMobileIP() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface
                    .getNetworkInterfaces(); en.hasMoreElements();) {
                NetworkInterface intf = (NetworkInterface) en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf
                        .getInetAddresses(); enumIpAddr.hasMoreElements();) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        String ipaddress = inetAddress .getHostAddress().toString();
                        return ipaddress;
                    }
                }
            }
        } catch (SocketException ex) {
            Crashlytics.logException(ex);
        }
        return null;
    }


    private double mLat, mLng;
    private void setGoogleMap(final double lat, final double lng){
        mLat = lat;
        mLng = lng;

        if(isAdded()){
//            try {
//                SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
//                mapFragment.getMapAsync(new OnMapReadyCallback() {
//                    @Override
//                    public void onMapReady(GoogleMap googleMap) {
//
//                        LatLng sydney = new LatLng(lat, lng);
//                        googleMap.addMarker(new MarkerOptions().position(sydney).title("Network Here!"));
//                        //        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
//                        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 16));
//                    }
//                });
//            } catch (Exception ignored) {
//                Crashlytics.logException(ignored);
//            }

            try {

                //https://maps.googleapis.com/maps/api/staticmap?center=13.7768,100.579&zoom=12&size=400x400&key=AIzaSyDBMlJ1ukpfqHfmVbYUd-lGtty7lRkY_3A
                ImageView imageView = (ImageView) mView.findViewById(R.id.wifi_map_static);
                String static_map = "https://maps.googleapis.com/maps/api/staticmap?center=&markers=size:mid%7Ccolor:red%7Clabel:N%7C"
                        + mLat
                        + ","
                        + mLng
                        + "&zoom=17&size=640x640&maptype=roadmap&key="
                        + AppController.getInstance().getAppContext().getString(R.string.google_static_map_api_key);
                Glide.with(this).load(static_map).into(imageView);

            } catch (Exception ignored) {
                Crashlytics.logException(ignored);
            }
        }
    }

    @Override
    public boolean canScrollVertically(int direction) {
        return mScrollView != null && mScrollView.canScrollVertically(direction);
    }

    @Override
    public void onFlingOver(int y, long duration) {
        if (mScrollView != null) {
            mScrollView.smoothScrollBy(0, y);
        }
    }

    private void openMapsExplorer(){
        try {

//            if (AppUtils.ads_interstitial_show_all) {
//
//                if (AppUtils.adsNetworkType != AppUtils.NONE_TYPE) {
//                    AppUtils.getInstance().showAdsFullBanner(new AppUtils.BaseAdListener() {
//                        @Override
//                        public void onAdClosed() {
//                            Intent intent = new Intent(AppController.getInstance().getAppContext(), MapActivity.class);
//                            startActivity(intent);
//                        }
//                    });
//                } else {
//                    Intent intent = new Intent(AppController.getInstance().getAppContext(), MapActivity.class);
//                    startActivity(intent);
//                }
//
//            } else {
//
//                if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
//                    AppUtils.getInstance().showAdmobAdsFullBanner(new AppUtils.BaseAdListener() {
//                        @Override
//                        public void onAdClosed() {
//                            Intent intent = new Intent(AppController.getInstance().getAppContext(), MapActivity.class);
//                            startActivity(intent);
//                        }
//                    });
//                } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
//                    AppUtils.getInstance().showFBAdsFullBanner(new AppUtils.BaseAdListener() {
//                        @Override
//                        public void onAdClosed() {
//                            Intent intent = new Intent(AppController.getInstance().getAppContext(), MapActivity.class);
//                            startActivity(intent);
//                        }
//                    });
//                } else {
//
//                    Intent intent = new Intent(AppController.getInstance().getAppContext(), MapActivity.class);
//                    startActivity(intent);
//                }
//            }

            Intent intent = new Intent(AppController.getInstance().getAppContext(), MapActivity.class);
            startActivity(intent);

        } catch (Exception ignored) {
            Crashlytics.logException(ignored);
        }
    }

    private void openMapsMarker(){
        try {

//            if (AppUtils.ads_interstitial_show_all) {
//
//                if (AppUtils.adsNetworkType != AppUtils.NONE_TYPE) {
//                    AppUtils.getInstance().showAdsFullBanner(new AppUtils.BaseAdListener() {
//                        @Override
//                        public void onAdClosed() {
//                            Intent intent = new Intent(AppController.getInstance().getAppContext(), MapsMarkerActivity.class);
//                            intent.putExtra(MapsMarkerActivity.LAT_MESSAGE, mLat);
//                            intent.putExtra(MapsMarkerActivity.LNG_MESSAGE, mLng);
//                            startActivity(intent);
//                        }
//                    });
//                } else {
//                    Intent intent = new Intent(AppController.getInstance().getAppContext(), MapsMarkerActivity.class);
//                    intent.putExtra(MapsMarkerActivity.LAT_MESSAGE, mLat);
//                    intent.putExtra(MapsMarkerActivity.LNG_MESSAGE, mLng);
//                    startActivity(intent);
//                }
//
//            } else {
//
//                if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
//                    AppUtils.getInstance().showAdmobAdsFullBanner(new AppUtils.BaseAdListener() {
//                        @Override
//                        public void onAdClosed() {
//                            Intent intent = new Intent(AppController.getInstance().getAppContext(), MapsMarkerActivity.class);
//                            intent.putExtra(MapsMarkerActivity.LAT_MESSAGE, mLat);
//                            intent.putExtra(MapsMarkerActivity.LNG_MESSAGE, mLng);
//                            startActivity(intent);
//                        }
//                    });
//                } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
//                    AppUtils.getInstance().showFBAdsFullBanner(new AppUtils.BaseAdListener() {
//                        @Override
//                        public void onAdClosed() {
//                            Intent intent = new Intent(AppController.getInstance().getAppContext(), MapsMarkerActivity.class);
//                            intent.putExtra(MapsMarkerActivity.LAT_MESSAGE, mLat);
//                            intent.putExtra(MapsMarkerActivity.LNG_MESSAGE, mLng);
//                            startActivity(intent);
//                        }
//                    });
//                } else {
//
//                    Intent intent = new Intent(AppController.getInstance().getAppContext(), MapsMarkerActivity.class);
//                    intent.putExtra(MapsMarkerActivity.LAT_MESSAGE, mLat);
//                    intent.putExtra(MapsMarkerActivity.LNG_MESSAGE, mLng);
//                    startActivity(intent);
//                }
//            }

            Intent intent = new Intent(AppController.getInstance().getAppContext(), MapsMarkerActivity.class);
            intent.putExtra(MapsMarkerActivity.LAT_MESSAGE, mLat);
            intent.putExtra(MapsMarkerActivity.LNG_MESSAGE, mLng);
            startActivity(intent);

        } catch (Exception ignored) {
            Crashlytics.logException(ignored);
        }
    }

    @Override
    public void takeScreenShot() {
        Thread thread = new Thread() {
            @Override
            public void run() {
                Bitmap bitmap = Bitmap.createBitmap(mView.getWidth(),
                        mView.getHeight(), Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(bitmap);
                mView.draw(canvas);
                SpeedMapFragment.this.bitmap = bitmap;
            }
        };

        thread.start();

    }
    @Override
    public Bitmap getBitmap() {
        return bitmap;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.mView = view.findViewById(R.id.container);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
