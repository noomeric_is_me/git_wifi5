package wimosalsafiwifimap.component;

import javax.inject.Singleton;

import dagger.Component;
import wimosalsafifreewifi.application.AppController;
import wimosalsafifreewifi.main.MainAppActivity_v2;
import wimosalsafiwifimap.activity.MapActivity;
import wimosalsafiwifimap.module.DaggerModule;

/**
 * Created by Federico
 */
@Singleton
@Component(modules = DaggerModule.class)
public interface WifiExplorerComponent {
    void inject(AppController application);
    void inject(MainAppActivity_v2 mainActivity);
    void inject(MapActivity mapActivity);
}
