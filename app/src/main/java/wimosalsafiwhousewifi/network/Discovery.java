package wimosalsafiwhousewifi.network;


import wimosalsafiwhousewifi.async.ScanHostsAsyncTask;
import wimosalsafiwhousewifi.response.MainAsyncResponse;

public class Discovery {
    public static void scanHosts(int i, int i2, int i3, MainAsyncResponse mainAsyncResponse) {
        new ScanHostsAsyncTask(mainAsyncResponse).execute(i, i2, i3);
    }
}
