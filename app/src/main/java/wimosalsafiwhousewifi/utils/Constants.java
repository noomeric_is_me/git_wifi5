package wimosalsafiwhousewifi.utils;

public class Constants {
    public static final int MAX_PORT_VALUE = 65535;
    public static final int MIN_PORT_VALUE = 1;
}
