package wimosalsafiwhousewifi.response;

public interface HostAsyncResponse extends LanHostAsyncResponse, WanHostAsyncResponse, ErrorAsyncResponse {
}
