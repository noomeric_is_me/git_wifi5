package wimosalsafiwhousewifi.response;

interface ErrorAsyncResponse {
    <T extends Throwable> void processFinish(T t);
}
